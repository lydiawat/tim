"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

from colorbar_and_scale import *
from abstract_floating_widget import FloatingChildWidgetBase

import settings
import logging

class MplCanvas(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""
    def __init__(self, parent=None, width=5, height=4, dpi=100, ax_aspect=None):
        fig = Figure(figsize=(width, height), dpi=dpi,
            facecolor='w', edgecolor='w')
        if ax_aspect:
            self.axes = fig.add_axes([0.05, 0.1, 0.5, 0.8], aspect=ax_aspect)
        else:
            self.axes = fig.add_axes([0.05, 0.1, 0.5, 0.8])
        # We want the axes cleared every time plot() is called
        self.axes.hold(False)

        self.compute_initial_figure()

        #
        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        # QSizePolicy.Preferred, QSizePolicy.Expanding
        FigureCanvas.setSizePolicy(self,
                                   QSizePolicy.Expanding,
                                   QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

    def compute_initial_figure(self):
        pass
        #raise NotImplementedError("Please Implement this method")


class ColorBarView(FloatingChildWidgetBase,MplCanvas):
    """ Use this as a normal QWidget  """
    def __init__(self, *args, **kwargs):
        """ overwrite, so that empty shows nothing at all """
        super(ColorBarView, self).__init__(*args, **kwargs)
        self.empty()
        self.loadSettings()

    def saveSettings(self):
        s = settings.appSettings()
        s.beginGroup("colour_bar_view")
        s.setValue("geometry", self.saveGeometry())
        s.endGroup()

    def loadSettings(self):
        s = settings.appSettings()
        s.beginGroup("colour_bar_view")
        if s.contains("geometry"):
            self.restoreGeometry(s.value("geometry").toByteArray())
        s.endGroup()

    def _setHighlight(self):
        self._orig_facecolor = self.axes.figure.get_facecolor()
        self.axes.figure.set_facecolor((0.9,0.9,0.9,0.9))
        self.draw()

    def _unsetHighlight(self):
        self.axes.figure.set_facecolor(self._orig_facecolor)
        self.draw()

    def empty(self):
        """ overwrite, so that empty shows nothing at all """
        self.axes.clear()
        self.axes.axis('off')
        self.draw()

    def update_figure(self,cb,label='Color Bar'):
        self.empty()
        from matplotlib import colors, colorbar, ticker

        # 1. indexed color mode (eg. rocktypes):
        #    discrete, uniform, cb_values only used as string labels,
        #    cb_colors indexed. n values (strings), n colors
        # 2. discrete color mode:
        #    each color is within a values range, n values, n-1 colors
        # 3. continuous (normal) mode:
        #    color smoothly varies between specified values, n values,
        #    n colors
        IDX_MODE, VAL_DISC_MODE, VAL_CONT_MODE = 0,1,2

        if cb.unit:
            label += ' (%s)' % cb.unit

        # if rocktype kind of plots
        if isinstance(cb.valueScale[0], str):
            mode = IDX_MODE
        else:
            if cb.discreteColorMode:
                mode = VAL_DISC_MODE
            else:
                mode = VAL_CONT_MODE

        # allow user to customise format of the tick labels
        tick_label_formatter = cb.label_formatter
        if callable(tick_label_formatter):
            def tick_format(value):
                return str(tick_label_formatter(value))
        elif isinstance(tick_label_formatter,str):
            def tick_format(value):
                return tick_label_formatter % value
        elif isinstance(tick_label_formatter,(list,tuple)):
            def tick_format(value):
                return tick_label_formatter[cb.valueScale.index(value)]
        else:
            def tick_format(value):
                return str(value)


        cb_values = cb.valueScale
        # QColor to generally accepted form, eg. '#eeff00'
        cb_colors = [str(c.name()) for c in cb.colorScale]

        if mode == IDX_MODE:
            # indexed colors (eg. rocktypes)
            norm = colors.NoNorm()
            cmap = colors.ListedColormap(cb_colors)

            cbar = colorbar.ColorbarBase(self.axes,
                cmap=cmap,
                norm=norm,
                drawedges = True,
                orientation='vertical',
                ticks=range(len(cb_values)),
                format=ticker.FixedFormatter([tick_format(v) for v in cb_values]),
                )
            #spacing='uniform',

            # Important! Trick is required with newer matplotlib (somewhere
            # between v1.0 and v1.4).  If format=None (by default),
            # ScalarFormatter will be used, and it can no longer handle single
            # value colors in the newer versions.  This is presumably caused by
            # changes made in ticker.py: ScalarFormatter._set_format()  Here we
            # avoid it by setting it to FixedFormatter to start with.
            # Originally it would be automatically set to FixedFormatter inside
            # cbar.set_ticklabels().

            # Necessary to set_ticks() before set_ticklabels().  Now both are in
            # init as as 'ticks' and 'format' (see above).  The plus side is
            # that this should be faster as two refreshes are avoided.

            # cbar.set_ticks(range(len(cb_values)))
            # cbar.set_ticklabels([tick_format(v) for v in cb_values])
            cbar.set_label(label)

            # trick to get the ticks gone
            cbar.ax.tick_params(bottom=False, top=False, left=False, right=False)

        elif mode == VAL_DISC_MODE:
            # If a ListedColormap is used, the length of the bounds array
            # (cb_values here) must be one greater than the length of the color
            # list.  The bounds must be monotonically increasing.   Also I make
            # value proportional here.
            cb_colors = cb_colors[:-1]

            # Possible matplotlib bug: it will crash when len(cb_values) < 3
            # (it doesn't even like len ==3 even though it does not crash)

            bounds = cb_values
            norm = colors.BoundaryNorm(bounds, len(cb_colors))
            cmap = colors.ListedColormap(cb_colors)

            cbar = colorbar.ColorbarBase(self.axes,
                cmap=cmap,
                norm=norm,
                spacing='proportional',
                drawedges = True,
                orientation='vertical')

            cbar.set_ticks(cb_values)
            cbar.set_label(label)
            if cb.logScaleMode:
                # log10 mode
                from math import pow
                cbar.set_ticklabels([tick_format(pow(10.0,v)) for v in cb_values])
            else:
                # normal value mode
                cbar.set_ticklabels([tick_format(v) for v in cb_values])

        elif mode == VAL_CONT_MODE:
            # continuous modes, n values, n colors, use LinearSegmentedColormap.from_list
            norm = colors.Normalize(vmin=cb_values[0], vmax=cb_values[-1])
            vals = norm(cb_values)
            # vcs is a list of (value, color) tuples can be given to divide the range unevenly
            vcs = zip(vals,cb_colors)
            cmap = colors.LinearSegmentedColormap.from_list('abc',vcs)

            cbar = colorbar.ColorbarBase(self.axes,
                cmap=cmap,
                norm=norm,
                orientation='vertical')

            cbar.set_ticks(cb_values)
            cbar.set_label(label)
            if cb.logScaleMode:
                # log10 mode
                from math import pow
                cbar.set_ticklabels([tick_format(pow(10.0,v)) for v in cb_values])
            else:
                # normal value mode
                cbar.set_ticklabels([tick_format(v) for v in cb_values])

        #for k in sorted(cbar.ax.yaxis.__dict__.keys()):
        #    print k
        self.axes.axis('on')
        self.draw()

class ColorDialogDelegate(QItemDelegate):
    """ Delegate to pop a color selector dialog for users to pick color easily """
    def __init__(self, parent=None):
        super(ColorDialogDelegate, self).__init__(parent)

    def createEditor(self, parent, option, index):
        editor = QColorDialog(parent)
        return editor

    def setEditorData(self, editor, index):
        editor.setCurrentColor(QColor(index.model().data(index, Qt.EditRole).toString()))

    def setModelData(self, editor, model, index):
        model.setData(index, str(editor.currentColor().name()))

class Ui_ColorBarEdit(QDialog):
    def __init__(self, parent=None):
        super(Ui_ColorBarEdit, self).__init__(parent)
        # should I connect a colorbar or a manager?
        self.cb_manager = None
        self.current_cb_name = None

        self.table = QTableWidget()
        self.table.setItemDelegateForColumn(1,ColorDialogDelegate())

        self.layout = QBoxLayout(QBoxLayout.TopToBottom)
        self.layout.addWidget(self.table)

        self.checkBoxes, self._op_partials = {}, {}
        self._addCheckBox('Discrete Color','discreteColorMode')
        self._addCheckBox('Log Scale','logScaleMode')
        self._addCheckBox('No Color When Under','noneColorUnder')
        self._addCheckBox('Use Regular Expression','regExpMode')
        self._addCheckBox('Text values','textMode')

        # colorbar limits
        groupbox_limits = QGroupBox("Limits")
        layout_limits = QGridLayout()
        self.edit_max, self.edit_min = QLineEdit(), QLineEdit()
        self.lbl_max, self.lbl_min = QLabel('Ma&x:'), QLabel('Mi&n:')
        self.lbl_max.setBuddy(self.edit_max); self.lbl_min.setBuddy(self.edit_min)
        self.edit_num_vals = QSpinBox()
        self.edit_num_vals.setMinimum(2)
        self.lbl_num_vals = QLabel("&Steps:")
        self.lbl_num_vals.setBuddy(self.edit_num_vals)
        self.apply_limits = QPushButton('Apply')
        layout_limits.addWidget(self.lbl_max, 0, 0)
        layout_limits.addWidget(self.lbl_min, 1, 0)
        layout_limits.addWidget(self.lbl_num_vals, 2, 0)
        layout_limits.addWidget(self.edit_max, 0, 1)
        layout_limits.addWidget(self.edit_min, 1, 1)
        layout_limits.addWidget(self.edit_num_vals, 2, 1)
        layout_limits.addWidget(self.apply_limits, 0, 2, 3, 1)
        groupbox_limits.setLayout(layout_limits)
        self.layout.addWidget(groupbox_limits)

        self.addRow = QPushButton('Add value/color')
        self.layout.addWidget(self.addRow)
        self.removeRow = QPushButton('Remove value/color')
        self.layout.addWidget(self.removeRow)
        self.resetColor = QPushButton('Reset color')
        self.layout.addWidget(self.resetColor)

        self.setLayout(self.layout)

        self.connect(self.addRow,SIGNAL("clicked ()"),self._addRow)
        self.connect(self.removeRow,SIGNAL("clicked ()"),self._removeRow)
        self.connect(self.resetColor,SIGNAL("clicked ()"),self._resetColor)
        QObject.connect(self.apply_limits, SIGNAL("clicked ()"), self._setLimits)
        QObject.connect(self.edit_min, SIGNAL("returnPressed ()"), self._setLimits)
        QObject.connect(self.edit_max, SIGNAL("returnPressed ()"), self._setLimits)
        self.loadSettings()

    def saveSettings(self):
        s = settings.appSettings()
        s.beginGroup("colour_bar_edit")
        s.setValue("geometry", self.saveGeometry())
        s.endGroup()

    def loadSettings(self):
        s = settings.appSettings()
        s.beginGroup("colour_bar_edit")
        if s.contains("geometry"):
            self.restoreGeometry(s.value("geometry").toByteArray())
        s.endGroup()

    def _addRow(self):
        if (self.cb_manager is None) or (self.current_cb_name is None): return
        cb = self.cb_manager[self.current_cb_name]
        i,j = self.table.currentRow(), self.table.currentColumn()
        cb.insertValueColor(i)
        # cb.valueScale = cb.valueScale[:i] + [cb.valueScale[i]] + cb.valueScale[i:]
        # cb.colorScale = cb.colorScale[:i] + [cb.colorScale[i]] + cb.colorScale[i:]
        self.cb_manager[self.current_cb_name] = cb
        self.table.setCurrentCell(i,j)
        self.table.setFocus()
        self.table.editItem(self.table.currentItem())

    def _removeRow(self):
        if (self.cb_manager is None) or (self.current_cb_name is None): return
        cb = self.cb_manager[self.current_cb_name]
        # These checking aren't ideal, the other half of the security is
        # provided by the colorbar object itself.  TODO: combine these
        # restriction and those colorbar buildin ones.
        if cb.textMode:
            if len(cb.valueScale) <= 1: return
        elif cb.discreteColorMode:
            if len(cb.valueScale) <= 3: return
        else:
            if len(cb.valueScale) <= 2: return
        i,j = self.table.currentRow(), self.table.currentColumn()
        if i is -1: i = len(cb.valueScale) - 1 # if not selected, make last one
        cb.removeValueColor(i)
        # cb.valueScale = cb.valueScale[:i] + cb.valueScale[i+1:]
        # cb.colorScale = cb.colorScale[:i] + cb.colorScale[i+1:]
        self.cb_manager[self.current_cb_name] = cb
        self.table.setCurrentCell(min(i,self.table.rowCount()-1),j)
        self.table.setFocus()

    def _resetColor(self):
        """ Reset the color scale (not values) to default. """
        if (self.cb_manager is None) or (self.current_cb_name is None): return
        cb = self.cb_manager[self.current_cb_name]
        cb.resetColor()
        self.cb_manager[self.current_cb_name] = cb

    def _setLimits(self):
        """ Reset the colorbar's limits using values specified. """
        cb = self.cb_manager[self.current_cb_name]

        if cb.textMode:
            logging.info("Setting limits is not allowed in text mode.")
            return
        try:
            _min = float(self.edit_min.text())
            _max = float(self.edit_max.text())
        except ValueError as err:
            logging.warning(str(err) + ", refreshing")
            self._refreshMinMax()
            return

        cb.valueScale = [_min] * self.edit_num_vals.value()
        cb.setBounds(_min, _max, useraw=False)
        cb.resetColor()
        self.cb_manager[self.current_cb_name] = cb

    def _setColorBarOption(self,checked,name):
        """ set a option of colorbar object to True or False """
        if (self.cb_manager is None) or (self.current_cb_name is None): return
        cb = self.cb_manager[self.current_cb_name]
        setattr(cb,name,bool(checked))
        self.cb_manager[self.current_cb_name] = cb
        # set cb will cause: self._ColorBarUpdated() -> self.loadColorBar() ->
        # self._refreshCheckBoxes(), so no need to refresh manually

    def _addCheckBox(self,text,name):
        """ temporarily designed for colorbar objects, text is displayed on
        checkboxes, name is attribute name for cb object """
        chkbox = QCheckBox(text)
        self.checkBoxes[name] = chkbox
        self.layout.addWidget(chkbox)

        from functools import partial
        self._op_partials[name] = partial(self._setColorBarOption,name=name)
        self.connect(chkbox,SIGNAL("toggled (bool)"),self._op_partials[name])

    def _refreshCheckBoxes(self):
        """ refresh the current colorbar's True/False options, mainly used when
        colorbar modified, and may affect other options, logic inside colorbar itself """
        cb = self.cb_manager[self.current_cb_name]
        for op_name in self._op_partials.keys():
            self.checkBoxes[op_name].blockSignals(True)
            self.checkBoxes[op_name].setChecked(getattr(cb,op_name))
            self.checkBoxes[op_name].blockSignals(False)

    def _refreshMinMax(self):
        """ Refresh edit boxes to colorbar's value. """
        if self.cb_manager is None: return
        cb = self.cb_manager[self.current_cb_name]
        self.edit_min.setText(str(cb.valueScale[0]))
        self.edit_max.setText(str(cb.valueScale[-1]))
        self.edit_num_vals.setValue(len(cb.valueScale))

    def loadColorBar(self,cb_name):
        cb_name = str(cb_name)
        if self.cb_manager is None: return
        if cb_name not in self.cb_manager:
            self.table.clear()
            self.setWindowTitle("ColorBar")
            return
        cb = self.cb_manager[cb_name]
        self.current_cb_name = cb_name
        self.setWindowTitle("ColorBar: %s" % cb_name)

        # +++++ sync check boxes
        self._refreshCheckBoxes()
        self._refreshMinMax()

        # +++++ sync table
        # disconnect before filling in the table, otherwise repeated  calls
        self.disconnect(self.table,SIGNAL("itemChanged (QTableWidgetItem *)"),
            self._itemChanged)

        self.table.clear()
        self.table.setRowCount(len(cb.valueScale))
        self.table.setColumnCount(2)
        self.table.setHorizontalHeaderLabels(["Value","Color"])

        for row,(v,c) in enumerate(zip(cb.valueScale,cb.colorScale)):
            vitem = QTableWidgetItem(str(v))
            citem = QTableWidgetItem(c.name())
            citem.setData(Qt.DecorationRole,c)
            citem.setData(Qt.ToolTipRole,"Tip: Also accepts color name (eg. 'blue').")
            self.table.setItem(row,0,vitem)
            self.table.setItem(row,1,citem)

        self.connect(self.table,SIGNAL("itemChanged (QTableWidgetItem *)"),
            self._itemChanged)

    def _ColorBarUpdated(self,cb_name):
        if self.cb_manager is None: return
        if cb_name == self.current_cb_name:
            self.loadColorBar(cb_name)

    def _itemChanged(self,item):
        cb = self.cb_manager[self.current_cb_name]

        i = item.row()
        t = str(item.text())
        if item.column() == 0:
            vs = cb.valueScale
            if cb.textMode:
                newv = str(t)
            else:
                newv = float(t)
            cb.valueScale = cb.valueScale[:i] + [newv] + cb.valueScale[i+1:]
            #item.setData(Qt.DisplayRole,newv)
        else:
            cs = cb.colorScale
            newc = str(t)
            cb.colorScale = cb.colorScale[:i] + [newc] + cb.colorScale[i+1:]
            #item.setData(Qt.DecorationRole,newc)

        self.cb_manager[self.current_cb_name] = cb

    def connectColorBarManager(self,cb_man):
        """ table will be refreshed if cb changed elsewhere """
        if self.cb_manager is not None:
            # disconnect
            self.disconnect(self.cb_manager,SIGNAL("color_bar_updated"),self._ColorBarUpdated)
        self.cb_manager = cb_man
        if self.cb_manager is not None:
            # connect: this listen changes from colorbar manager
            self.connect(self.cb_manager,SIGNAL("color_bar_updated"),self._ColorBarUpdated)


class Test(QWidget):
    def __init__(self, use_manager=False, parent=None):
        super(Test, self).__init__(parent)

        self.cbs = []

        if use_manager:
            cbm = ColorBarManager()
            cbm._setupColorBars()

            self.cbs.append(cbm['K 1'])

            self.cbs.append(cbm['Temperature'])
            self.cbs[-1].discreteColorMode = True

        cb = myColorBar()
        cb.valueScale = ['1/6A', '2/6B', '3/6C', '4/6D', '5/6E', '6/6F']
        cb.colorScale = [QColor(c) for c in [
            'blue','skyblue','lightgreen','yellow','orange','red']]
        cb.discreteColorMode = True
        cb.label_formatter = '%s'
        cb.logScaleMode = False
        self.cbs.append(cb)

        n = 1
        cb = myColorBar()
        cb.valueScale = ['1/6A', '2/6B', '3/6C', '4/6D', '5/6E', '6/6F'][:n]
        cb.colorScale = [QColor(c) for c in [
            'blue','skyblue','lightgreen','yellow','orange','red'][:n]]
        cb.discreteColorMode = True
        cb.label_formatter = '%s'
        cb.logScaleMode = False
        self.cbs.append(cb)

        n = 2
        cb = myColorBar()
        cb.valueScale = [1.0,1.5,3.0,5.0][:n]
        cb.colorScale = [QColor(c) for c in [
            'blue','skyblue','lightgreen','yellow','orange','red'][:n]]
        cb.discreteColorMode = True
        cb.label_formatter = '%f'
        cb.logScaleMode = False
        self.cbs.append(cb)

        self.cbp = ColorBarView(self, width=0.5, height=5, dpi=100, ax_aspect=15)
        self.cbp.update_figure(self.cbs[0])

        layout = QVBoxLayout()
        layout.addWidget(self.cbp)
        self.setLayout(layout)

        self.icall = 0

        self.setToolTip('Double Click to cycle to next...')
        QObject.connect(self.cbp, SIGNAL("double_clicked"), self.nextFigure)

    def nextFigure(self):
        print 'showing # ', self.icall
        self.cbp.update_figure(self.cbs[self.icall])
        self.icall += 1
        if self.icall < 0 or self.icall >= len(self.cbs):
            self.icall = 0

    def mouseDoubleCLickEvent(self,e):
        if e.buttons() == Qt.LeftButton:
            self.nextFigure()

if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)

    win = Test()

    # cbm = ColorBarManager()
    # cbm._setupColorBars()
    # win = Ui_ColorBarEdit()
    # win.connectColorBarManager(cbm)
    # win.loadColorBar('K 1')

    win.show()
    win.raise_()
    win.activateWindow()


    app.exec_()


