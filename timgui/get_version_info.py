""" This script uses git describe to get a version description that contains
last known tag, number of commit ahead, SHA-1, dirty status and branch name.

An example: rc0.7.0-1-gf44c155-dirty (release-0.7.0)

This can be used as an independent scriptm called from within a git repo
directory.  Which will reaturn a descriptive version string.  It can also be
called programmically and pass a repo_path into function.  Note it's useful to
call getGitDesc() with the directory of the calling script, eg.
os.path.dirname(os.path.realpath(__file__)).
"""

from subprocess import *
import os
import sys

def getGitDesc(repo_path=None):
    desc, branch = '', ''
    if repo_path is None:
        # use sys.path[0] to know where the script is, see note below
        repo_path = sys.path[0]
    if os.path.exists(repo_path):
        original_cwd = os.getcwd()
        os.chdir(repo_path)

        # these has to be run within the repo directory, where the script is.
        # desc = Popen('git describe --tags --always --dirty', stdout=PIPE, shell=True).stdout.read().strip()
        # branch = Popen('git rev-parse --abbrev-ref HEAD', stdout=PIPE, shell=True).stdout.read().strip()
        with open(os.devnull, "w") as fnull:
            try:
                desc = check_output('git describe --tags --always --dirty',
                                    stderr=fnull).strip()
                branch = check_output('git rev-parse --abbrev-ref HEAD',
                                      stderr=fnull).strip()
            except:
                # CalledProcessError and WindowsError are what I found on
                # windows machine, but I should probably just trap all
                # exceptions, so it's more graceful. Git desc is not critical.
                pass

        # change directory back to original cwd
        os.chdir(original_cwd)
    if branch:
        desc = '%s (%s)' % (desc, branch)
    return desc

if __name__ == '__main__':
    print getGitDesc(os.getcwd())

# How to get script directory using sys.path[0]:
#
# As initialized upon program startup, the first item of this list, path[0], is
# the directory containing the script that was used to invoke the Python
# interpreter. If the script directory is not available (e.g. if the interpreter
# is invoked interactively or if the script is read from standard input),
# path[0] is the empty string, which directs Python to search modules in the
# current directory first.
