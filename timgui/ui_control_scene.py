"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from graphics_scenes import *
from choices_widgets import getChoiceFromDialog
from units import Unit, shortUnit, unitChoices
import logging

import ui_resources

class Ui_SceneControl(QWidget):
    """ each scene controller can/should be connected to a particular
    SceneManager and view (or views) """
    def __init__(self, parent=None):
        super(Ui_SceneControl, self).__init__(parent)
        self._s_manager = None
        self._view = None

        # (may need to move) scene controller controls arrowscale and colorbar
        self._arrowscaleview = None # not used yet
        self._current_arrow_name = None
        self._colorbarview = None
        self._current_colorview_name = None
        self._colorbar_control = None

        # (may need to move) scene controller controls arrowscale and colorbar
        self.arrowscale_manager = None
        self.colorbar_manager = None

        self._current_scene = ''
        self._layer_setting = None

        self.setWindowTitle("Scene")

        #----- indivisual controls and associated text label
        self.LayerSliceSelector = QListWidget()
        labelLayerSliceSelector = QLabel("Goto &Scene (Slice or Layer):")
        labelLayerSliceSelector.setBuddy(self.LayerSliceSelector)
        #labelLayerSliceSelector.setAlignment(Qt.AlignRight)

        self.ColorSelector = QComboBox()
        labelColorSelector = QLabel("View Variable as &Color:")
        labelColorSelector.setBuddy(self.ColorSelector)
        #labelColorSelector.setAlignment(Qt.AlignRight)

        self.color_unit_button = QPushButton('')
        self.color_unit_button.setEnabled(False)
        self.text_unit_button = QPushButton('')
        self.text_unit_button.setEnabled(False)
        self.arrow_unit_button = QPushButton('')
        self.arrow_unit_button.setEnabled(False)

        self.TextSelector = QComboBox()
        labelTextSelector = QLabel("View Variable as &Text:")
        labelTextSelector.setBuddy(self.TextSelector)
        #labelTextSelector.setAlignment(Qt.AlignRight)

        self.ArrowSelector = QComboBox()
        labelArrowSelector = QLabel("View Variable as &Arrow:")
        labelArrowSelector.setBuddy(self.ArrowSelector)
        #labelArrowSelector.setAlignment(Qt.AlignRight)

        self.ArrowScale = QDoubleSpinBox()
        self.ArrowScale.setMaximum(1.0E20)
        self.ArrowScale.setSingleStep(1)
        self.ArrowScale.setDecimals(4)
        labelArrowScale = QLabel("A&rrow Scale")
        labelArrowScale.setBuddy(self.ArrowScale)
        #labelArrowScale.setAlignment(Qt.AlignRight)

        self.ShowColorbarView = QCheckBox("Colorbar")
        self.ShowColorbarView.setChecked(True)
        labelColorBarLimit = QLabel("Color Bar Limit:")
        self.SetColorLimitScene = QPushButton(QIcon(":/colorbar.png"),'Scene Limit')
        self.SetColorLimitModel = QPushButton(QIcon(":/colorbar.png"),'Model Limit')
        self.CustomiseColorBar = QPushButton(QIcon(":/colorbar.png"),'Customise')
        labelColorBarLimit.setBuddy(self.SetColorLimitScene)

        self.ShowWells = QCheckBox("Show &Wells")
        self.ShowWells.setChecked(False)

        self.find_box = QLineEdit()
        self.find_box.setToolTip("Enter block name, or well name with 'e:' prefix")
        self.goto_button = QPushButton('Goto')
        self.goto_button.setToolTip("Centre scene on item")
        label_find_box = QLabel('Find:')
        label_find_box.setToolTip("Search and goto item in current scene")
        label_find_box.setBuddy(self.find_box)

        #----- layout
        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.addWidget(labelLayerSliceSelector)
        layout.addWidget(self.LayerSliceSelector)

        layout_h2 = QHBoxLayout()
        layout_h2.addWidget(labelColorSelector)
        layout_h2.addWidget(self.color_unit_button)
        layout.addLayout(layout_h2)
        layout.addWidget(self.ColorSelector)

        layout_h3 = QHBoxLayout()
        layout_h3.addWidget(labelTextSelector)
        layout_h3.addWidget(self.text_unit_button)
        layout.addLayout(layout_h3)
        layout.addWidget(self.TextSelector)

        layout_h4 = QHBoxLayout()
        layout_h4.addWidget(labelArrowSelector)
        layout_h4.addWidget(self.arrow_unit_button)
        layout.addLayout(layout_h4)
        layout.addWidget(self.ArrowSelector)

        layout.addWidget(self.ShowColorbarView)
        layout_h1 = QHBoxLayout()
        layout_h1.addWidget(self.SetColorLimitScene)
        layout_h1.addWidget(self.SetColorLimitModel)
        layout_h1.addWidget(self.CustomiseColorBar)
        layout.addLayout(layout_h1)

        layout.addWidget(labelArrowScale)
        layout.addWidget(self.ArrowScale)
        layout.addWidget(self.ShowWells)

        layout_hfind = QHBoxLayout()
        layout_hfind.addWidget(label_find_box)
        layout_hfind.addWidget(self.find_box)
        layout_hfind.addWidget(self.goto_button)
        layout.addLayout(layout_hfind)

        #layout.addStretch()
        self.setLayout(layout)

        # function/callables for updating control
        from functools import partial
        self._updateColorList = partial(self._updateComboBoxChoices,
            combo_box=self.ColorSelector,separator_aft=[])
        self._updateLabelList = partial(self._updateComboBoxChoices,
            combo_box=self.TextSelector,separator_aft=[])
        self._updateArrowList = partial(self._updateComboBoxChoices,
            combo_box=self.ArrowSelector,separator_aft=[])

        # basic inter-control communications, I don't like how these
        # are separate from the other connections that directly connects
        # to scene manager
        self.connect(self.LayerSliceSelector,
            SIGNAL("currentTextChanged (const QString&)"),
            self._setSceneToView)
        self.connect(self.ShowColorbarView,
            SIGNAL("stateChanged (int)"),
            self._setColorbarVisible)
        # units
        self.connect(self.ColorSelector,
            SIGNAL("currentIndexChanged (const QString&)"),
            self._updateColorUnitButton)
        self.connect(self.TextSelector,
            SIGNAL("currentIndexChanged (const QString&)"),
            self._updateTextUnitButton)
        self.connect(self.ArrowSelector,
            SIGNAL("currentIndexChanged (const QString&)"),
            self._updateArrowUnitButton)
        self.connect(self.color_unit_button,
            SIGNAL("clicked ()"),
            self._promptChangeColorUnit)
        self.connect(self.text_unit_button,
            SIGNAL("clicked ()"),
            self._promptChangeTextUnit)
        self.connect(self.arrow_unit_button,
            SIGNAL("clicked ()"),
            self._promptChangeArrowUnit)
        self.connect(self.ColorSelector,
            SIGNAL("currentIndexChanged (const QString&)"),
            self._forceColorbarShowHide)
        self.connect(self.ArrowSelector,
            SIGNAL("currentIndexChanged (const QString&)"),
            self._updateArrowScaleView)
        self.connect(self.ArrowScale,
            SIGNAL("valueChanged (double)"),
            self._editArrowScale)
        self.connect(self.ShowWells,
            SIGNAL("stateChanged (int)"),
            self._setWellVisible)
        self.connect(self.SetColorLimitScene,
            SIGNAL("clicked ()"),
            self.setColorLimitScene)
        self.connect(self.SetColorLimitModel,
            SIGNAL("clicked ()"),
            self.setColorLimitModel)
        self.connect(self.CustomiseColorBar,
            SIGNAL("clicked ()"),
            self.showColorbarControl)

        QObject.connect(self.goto_button,
            SIGNAL('clicked ()'),
            self.centerOnItem)
        QObject.connect(self.find_box,
            SIGNAL('returnPressed ()'),
            self.centerOnItem)

        # make find box select text when got focused
        class FocusInEventFilter(QObject):
            def __init__(self, parent, callback):
                QObject.__init__(self, parent)
                self.callback = callback
            def eventFilter(self, obj, event):
                if event.type() == QEvent.FocusIn:
                    self.callback()
                return QObject.eventFilter(self, obj, event)
        self.find_box.installEventFilter(FocusInEventFilter(self.find_box, self.find_box.selectAll))


    def setVertical(self):
        self.layout().setDirection(QBoxLayout.TopToBottom)
    def setHorizontal(self):
        self.layout().setDirection(QBoxLayout.LeftToRight)

    def _setComboBoxCurrentText(self,combo_box,text):
        i = combo_box.findText(text)
        if i == -1:
            print '%s is not a valid option.' % text
            return
        combo_box.setCurrentIndex(i)

    def setCurrentColor(self,name):
        self._setComboBoxCurrentText(self.ColorSelector,name)
    def currentColor(self):
        return str(self.ColorSelector.currentText())

    def setCurrentText(self,name):
        self._setComboBoxCurrentText(self.TextSelector,name)
    def currentText(self):
        return str(self.TextSelector.currentText())

    def setCurrentArrow(self,name):
        self._setComboBoxCurrentText(self.ArrowSelector,name)
    def currentArrow(self):
        return str(self.ArrowSelector.currentText())

    def centerOnItem(self):
        s = self._current_scene
        pos = s.getItemPos(str(self.find_box.text()))
        if pos:
            self._view.centerOn(pos)
            self._view.activateWindow()
            self._view.setFocus(Qt.OtherFocusReason)

    def _updateSceneList(self,text_list):
        """ clears and update the list widget with updated scene list
        This isn't very good, clears everytime """
        # remembers the current selection
        cur_item = self.LayerSliceSelector.currentItem()
        if cur_item:
            cur_name = cur_item.text()
        # clear and refill the list
        self.LayerSliceSelector.clear()
        for t in text_list:
            i = QListWidgetItem(t,self.LayerSliceSelector)
            if cur_item:
                if str(cur_name) == str(t):
                    self.LayerSliceSelector.setCurrentItem(i)
        # if still no selected, make an default one
        if not self.LayerSliceSelector.currentItem():
            if self.LayerSliceSelector.count() <> 0:
                self.LayerSliceSelector.setCurrentRow(0,QItemSelectionModel.Select)

    def _updateComboBoxChoices(self,text_list,combo_box,separator_aft=[]):
        """ clears and update the combo_box to the text_list, with
        separator added after indices in separator_aft """
        cur_name = str(combo_box.currentText())
        if cur_name in text_list:
            # no update/refresh when text not going to change
            combo_box.blockSignals(True)

        # clear and refill the list
        cur_i = None
        combo_box.clear()
        for i,t in enumerate(text_list):
            combo_box.addItem(QString(t))
            if str(cur_name) == str(t):
                cur_i = i
            if i in separator_aft:
                combo_box.insertSeparator(i+1)

        if cur_i is not None:
            combo_box.setCurrentIndex(cur_i)
        else:
            # if still no selected, make an default one
            if combo_box.count() <> 0:
                combo_box.setCurrentIndex(0)

        # restore signals no matter what
        combo_box.blockSignals(False)

    def _updateColorUnitButton(self, valName=None):
        if valName is None:
            valName = self.currentColor()
        u = self._s_manager.getColorTextUnit(str(valName)) #gets model's unit - not tim session defaults
        if u in [None, '']:
            self.color_unit_button.setText('')
            self.color_unit_button.setEnabled(False)
            return
        if len(unitChoices(u)) <= 1:
            self.color_unit_button.setText('(%s)' % shortUnit(u))
            self.color_unit_button.setEnabled(False)
            return
        self.color_unit_button.setText('(%s)' % shortUnit(u))
        self.color_unit_button.setEnabled(True)

    def _updateTextUnitButton(self, valName=None):
        if valName is None:
            valName = self.currentText()
        u = self._s_manager.getColorTextUnit(str(valName))
        if u in [None, '']:
            self.text_unit_button.setText('')
            self.text_unit_button.setEnabled(False)
            return
        if len(unitChoices(u)) <= 1:
            self.color_unit_button.setText('(%s)' % shortUnit(u))
            self.color_unit_button.setEnabled(False)
            return
        self.text_unit_button.setText('(%s)' % shortUnit(u))
        self.text_unit_button.setEnabled(True)

    def _updateArrowUnitButton(self, valName=None):
        if valName is None:
            valName = self.currentArrow()
        u = self._s_manager.getFlowUnit(str(valName))
        if u in [None, '']:
            self.arrow_unit_button.setText('')
            self.arrow_unit_button.setEnabled(False)
            return
        if len(unitChoices(u)) <= 1:
            self.color_unit_button.setText('(%s)' % shortUnit(u))
            self.color_unit_button.setEnabled(False)
            return
        self.arrow_unit_button.setText('(%s)' % shortUnit(u))
        self.arrow_unit_button.setEnabled(True)

    def _promptChangeColorUnit(self):
        v = self.currentColor()
        u = self._s_manager.getColorTextUnit(v)
        if u not in [None, '']:
            choices = [str(Unit(uu).units) for uu in unitChoices(u)]
            idx = choices.index(str(Unit(u).units))
            new_unit = getChoiceFromDialog(v, choices, idx)
            if new_unit is not None:
                if new_unit != u:
                    self._setColorTextUnit(v, new_unit)
                    self._updateColorUnitButton()
                    self._updateTextUnitButton()

    def _promptChangeTextUnit(self):
        v = self.currentText()
        u = self._s_manager.getColorTextUnit(v)
        if u not in [None, '']:
            u = str(Unit(u).units)
            choices = [str(Unit(uu).units) for uu in unitChoices(u)]
            idx = choices.index(str(Unit(u).units))
            new_unit = getChoiceFromDialog(v, choices, idx)
            if new_unit is not None:
                if new_unit != u:
                    self._setColorTextUnit(v, new_unit)
                    self._updateColorUnitButton()
                    self._updateTextUnitButton()

    def _promptChangeArrowUnit(self):
        v = self.currentArrow()
        u = self._s_manager.getFlowUnit(v)
        if u not in [None, '']:
            u = str(Unit(u).units)
            choices = [str(Unit(uu).units) for uu in unitChoices(u)]
            idx = choices.index(str(Unit(u).units))
            new_unit = getChoiceFromDialog(v, choices, idx)
            if new_unit is not None:
                if new_unit != u:
                    self._setArrowUnit(v, new_unit)
                    self._updateArrowUnitButton()

    def _setSceneToView(self,scene_name,view_idx=0):
        """ if there is more than one views connected to scene_control,
        it is possible to set scene[scene_name] to view[view_idx], otherwise
        simply set self._view to the specified scene """
        if self._view is None or self._s_manager is None or not scene_name: return

        # scene_name either a key to dictionary, or an int as index
        if isinstance(scene_name,int):
            to_scene = self._s_manager.scenelist[scene_name]
        else:
            to_scene = self._s_manager.scene[str(scene_name)]

        # ----- set QCompleter, if scene not have it, create one -----
        self.find_box.setCompleter(to_scene.completer())

        # ----- zoom/scroll saving and loading -----
        to_scroll_values = None
        if self._view.scene():
            # save zoon/scroll setting
            self._view.scene().previousScrollValues = self._view.zoomScrollSetting()
            if type(self._view.scene()) in [LayerScene,TopSurfaceScene]:
                self._layer_setting = self._view.zoomScrollSetting()
        if hasattr(to_scene,'previousScrollValues'):
            # if available, load setting
            to_scroll_values = to_scene.previousScrollValues
        if type(to_scene) in [LayerScene,TopSurfaceScene]:
            if self._layer_setting is not None:
                # overwrite if layer (all layers use the same)
                to_scroll_values = self._layer_setting

        # ----- set scene to view -----
        # ensure scene is ready, see setupGraphicsPostponed()
        to_scene.setupIfNotYet()
        if type(self._view) == type([]):
            self._view[view_idx].setScene(to_scene)
        else:
            self._view.setScene(to_scene)

        # ----- set scene zoom/scroll -----
        if to_scroll_values:
            self._view.setZoomScrollSetting(to_scroll_values)
        else:
            self._view.resetScale()

        # update scene, to reflect selector
        self._s_manager.showSceneColor(str(self.ColorSelector.currentText()))
        self._s_manager.showSceneText(str(self.TextSelector.currentText()))
        self._s_manager.showSceneArrow(str(self.ArrowSelector.currentText()))
        #self._s_manager.setWellVisible(self.ShowWells.isChecked())

        self._current_scene = to_scene

    def _forceColorbarShowHide(self, name):
        """ Switching color variable always force colorbar view to show or hide.
        """
        if name == 'No Fill':
            self.ShowColorbarView.setChecked(False)
            self._setColorbarVisible()
        else:
            self.ShowColorbarView.setChecked(True)
            self._setColorbarVisible()

    def _setColorbarVisible(self):
        self.emit(SIGNAL("show_colorbar_view"), self.ShowColorbarView.isChecked())

    def _setWellVisible(self):
        if self._s_manager:
            self._s_manager.setWellVisible(self.ShowWells.isChecked())

    def _editArrowScale(self,val):
        """ reflect user edited scale value in scale object """
        if self.arrowscale_manager:
            sc = self.arrowscale_manager[str(self.ArrowSelector.currentText())]
            sc.val = val
            self.arrowscale_manager[str(self.ArrowSelector.currentText())] = sc

    def _updateArrowScale(self, valName=None):
        """ reflect the actual values of scale in the spinbox """
        if valName is None:
            valName = self.ArrowSelector.currentText()
        valName = str(valName)
        if self.arrowscale_manager:
            self.ArrowScale.setValue(self.arrowscale_manager[valName].val)

    def _setColorTextUnit(self, vname, newunit):
        """ Change color/text variable's unit, this will make scene manager to
        change all scenes' unit (of the same variable) and update cb object, so
        colorbar display it properly.
        """
        self._s_manager.setColorTextUnit(vname, newunit)
        cb = self.colorbar_manager[vname]
        cb.unit = newunit
        self.colorbar_manager[vname] = cb

    def _setArrowUnit(self, vname, newunit):
        """ Change arrow variable's unit, this will make scene manager to
        change all scenes' unit (of the same variable).
        TODO: will add update scale factor etc.
        """
        self._s_manager.setFlowUnit(vname, newunit)
        sc = self.arrowscale_manager[vname]
        sc.unit = newunit
        self.arrowscale_manager[vname] = sc

    def _setColorBarStrings(self, raw_values, cb_name):
        """ handles string type variable, setting up color by name groups """
        def group_values(values, maxn, n):
            return list(set([d[:n]+'.*'*min(1,(maxn-n)) for d in values if d]))
        vlength = max([len(x) for x in raw_values])
        # finding optimal grouping
        optimal = 20 # this is the target number of groups
        nn, ngrps = [], {}
        for i in range(vlength+1):
            grps = group_values(raw_values, vlength, i)
            nn.append(abs(optimal - len(grps)))
            ngrps[i] = grps
        # find minimum from the back, so use as many characters as it can
        default_n = vlength - nn[::-1].index(min(nn))
        # asks/confirms with user
        n, ok = QInputDialog.getInt(None,
            "Group Colors By Name",
            "Number of characters used to group strings (0-%i):" % vlength,
            default_n, 0, vlength)
        new_grp = ngrps[n]
        # setting cb
        cb = self.colorbar_manager[cb_name]
        cb.valueScale = sorted(new_grp, reverse=True)
        cb.setRegExpMode(True)
        cb.resetColor()
        self.colorbar_manager[cb_name] = cb

    def _setColorBarBounds(self, vmin, vmax, cb_name):
        """ handles float type variable, simply set min and max """
        cb = self.colorbar_manager[cb_name]
        cb.setBounds(vmin,vmax,useraw=True)
        self.colorbar_manager[cb_name] = cb
        msg = "Set colorbar '%s' limit (%s,%s)" % (cb_name, str(vmin), str(vmax))
        logging.info(msg)

    def _setColorBar(self, values, cb_name):
        """ checks variable types, and set colorbar differently """
        dtyp = values.dtype.name
        if dtyp.startswith('string'):
            self._setColorBarStrings(list(values), cb_name)
        elif dtyp.startswith('int'):
            raise Exception('Not implemented yet')
        elif dtyp.startswith('float'):
            vmin, vmax = min(values), max(values)
            try:
                vmin, vmax = vmin.magnitude, vmax.magnitude
            except AttributeError:
                pass
            self._setColorBarBounds(vmin, vmax, cb_name)

    def setColorLimitScene(self):
        if self._current_scene and self.colorbar_manager:
            cb_name = str(self.ColorSelector.currentText())
            if cb_name == 'No Fill':
                return
            values = self._current_scene.getBlockValues(cb_name)
            self._setColorBar(values, cb_name)

    def setColorLimitModel(self):
        # scene control does not have model, should I get it from the model in
        # scene or scene manager?
        model = self._current_scene._model
        if model is None:
            return
        if self._current_scene and self.colorbar_manager:
            cb_name = str(self.ColorSelector.currentText())
            if cb_name == 'No Fill':
                return
            values = model.getData(Model.BLOCK, cb_name)
            self._setColorBar(values, cb_name)

    def setCurrentScene(self,name):
        """ show the specified scene by name, return the original shown scene
        name """
        lw = self.LayerSliceSelector
        orignal_name = str(lw.model().data(lw.model().index(lw.currentRow(),0)).toString())
        for i in range(lw.model().rowCount()):
            if name == str(lw.model().data(lw.model().index(i,0)).toString()):
                lw.setCurrentRow(i)
                break
        return orignal_name

    def showColorbarControl(self):
        if self._colorbar_control is not None:
            self._colorbar_control.show()
            self._colorbar_control.raise_()
            self._colorbar_control.activateWindow()

    def _updateColorBarView(self,cb_name=''):
        """ Show the specified colorbar in colorbar view, by name/key in cb
        manager.
        """
        cb_name = str(cb_name)
        if self._current_scene and self.colorbar_manager:
            if cb_name:
                # to ensure colorbar's unit matches the variables from scene
                cb = self.colorbar_manager[cb_name]
                cb.unit = self._current_scene.getBlockUnit(cb_name)
                self._colorbarview.update_figure(cb,cb_name)
                self._current_colorview_name = cb_name
            else:
                self._colorbarview.empty()

    def _syncColorBarChanges(self,cb_name):
        """ If the specifed colorbar (by name) is currently displayed, then
        force update (to reflect changes in cb object).  Otherwise do nothing.
        """
        if self._current_colorview_name == cb_name:
            self._updateColorBarView(cb_name)

    def _updateArrowScaleView(self, valName):
        """ There is no arrow scale view yet, but arrow scale should have
        similar updating mechanism like color/text """
        valName = str(valName)
        if self._current_scene and self.arrowscale_manager:
            if valName:
                # to ensure arrowscale's unit matches the variables from scene
                sc = self.arrowscale_manager[valName]
                sc.unit = self._current_scene.getFlowUnit(valName)
                # self.arrowscale_manager[valName] = sc
                self._updateArrowScale()
                self._current_arrow_name = valName

    def _syncArrowScaleChanges(self, valName):
        """ There is no arrow scale view yet, but arrow scale should have
        similar updating mechanism like color/text """
        if self._current_arrow_name == valName:
            self._updateArrowScaleView(valName)

    def connectView(self,view,clear_other=False):
        """ set view or list of views to show scenes managed by self._s_manager
        and reflect this control """
        if clear_other:
            self._view = None
            self._view = view
        else:
            if self._view is None:
                self._view = view
            else:
                if view is not list: view = [view]
                if self._view is not list: self._view = [self._view]
                self._view += view
        # set new view(s) to currently displayed scene
        if self._s_manager is not None:
            if len(self._s_manager.scenelist) > 0:
                self._setSceneToView(self._current_scene)


    def connectSceneManager(self,s_manager):
        """ connects to a scene manager """
        if self._s_manager is not None:
            # disconnect with prev scene manager
            self.disconnect(self._s_manager,0,self,0)
            self.disconnect(self,0,self._s_manager,0)

        self._s_manager = s_manager

        # connect manager signals to ui updates
        self.connect(s_manager,SIGNAL("scene_list_updated"),self._updateSceneList)
        self.connect(s_manager,SIGNAL("color_vars_updated"),self._updateColorList)
        self.connect(s_manager,SIGNAL("label_vars_updated"),self._updateLabelList)
        self.connect(s_manager,SIGNAL("arrow_vars_updated"),self._updateArrowList)

        # intentionally triger it to ensure the combo boxes is filled
        s_manager.emitVariableLists()

        # intentionally sync the well visibility
        self._setWellVisible()

        # connect ui controls to scene manager's function
        # maybe this should be in scene_manager? but I don't want
        # scene manager to know the actual "controls"
        self.connect(self.ColorSelector,
            SIGNAL("currentIndexChanged (const QString&)"),
            self._s_manager.showSceneColor)
        self.connect(self.TextSelector,
            SIGNAL("currentIndexChanged (const QString&)"),
            self._s_manager.showSceneText)
        self.connect(self.ArrowSelector,
            SIGNAL("currentIndexChanged (const QString&)"),
            self._s_manager.showSceneArrow)

    def connectColorBarControl(self,cb_control):
        """ simply imform colorbar control which cb_name is being used """
        if self._colorbar_control is not None:
            self.disconnect(self.ColorSelector,
                SIGNAL("currentIndexChanged (const QString&)"),
                self._colorbar_control.loadColorBar)
        self._colorbar_control = cb_control
        if self._colorbar_control is not None:
            self.connect(self.ColorSelector,
                SIGNAL("currentIndexChanged (const QString&)"),
                self._colorbar_control.loadColorBar)

    def connectColorBarView(self,cb_view):
        """ When the user change variable in the color selector, update the
        colorbar view with appropriate colorbar object.

        TODO: this is not very neat, maybe needs to go somewhere else.
        """
        if self._colorbarview is not None:
            # disconnect
            self.disconnect(self.ColorSelector,
                SIGNAL("currentIndexChanged (const QString&)"),
                self._updateColorBarView)
        self._colorbarview = cb_view
        if self._colorbarview is not None:
            # connect: colorbar view listens to combobox
            self.connect(self.ColorSelector,
                SIGNAL("currentIndexChanged (const QString&)"),
                self._updateColorBarView)

    def connectColorBarManager(self,cb_manager):
        """ Colorbar manager informs/triggers the update/redraw of colorbar view
        if a particular colorbar object is updated.

        Note: cb manager knows this when a cb obj set to the manager, eg.
        cb_manager[xxx] = cb

        The _syncColorBarChanges() will only trigger actual redraw if the
        particular colorbar is currently being displayed.

        TODO: this is not very neat, maybe needs to go somewhere else.
        """
        if self.colorbar_manager is not None:
            # disconnect
            self.disconnect(self.colorbar_manager,
                SIGNAL("color_bar_updated"),
                self._syncColorBarChanges)
        self.colorbar_manager = cb_manager
        if self.colorbar_manager is not None:
            # connect: this listen changes from colorbar manager
            self.connect(self.colorbar_manager,
                SIGNAL("color_bar_updated"),
                self._syncColorBarChanges)

    def connectArrowScaleManager(self,arrow_arrowscale_manager=None):
        if self.arrowscale_manager:
            self.disconnect(self.arrowscale_manager,SIGNAL("arrow_scale_updated"),
                self._syncArrowScaleChanges)
        self.arrowscale_manager = arrow_arrowscale_manager
        if arrow_arrowscale_manager:
            # react to manager's notification about value changes
            # set manager's value via self control
            self.connect(self.arrowscale_manager,SIGNAL("arrow_scale_updated"),
                self._syncArrowScaleChanges)
            # # update to where it should be now
            # self._syncArrowScaleChanges()

