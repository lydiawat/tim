"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import logging

# PyTOUGH model centre
from t2model import Model

# scene/view and their control
from graphics_scenes import *
from scene_manager import SceneManager
from graphics_view import TimGraphicsView

from ui_control_scene import Ui_SceneControl
from ui_control_mode import Ui_SceneModeControl, getModeManager
from ui_control_time import Ui_TimeControl

# additional utility "clipboards"
from ui_control_points import Ui_PointsBoard
from ui_control_info import *
#from tim_board_selection import *
from ui_control_edit_rock import Ui_EditRocktypeBoard
from ui_table_gener import Ui_GenerTable
from ui_table_rocks import Ui_Rocktypes

from ui_graph_plot_board import makeDownholePlotBoard, makeHistoryPlotBoard
from ui_graph_plot_board import makeAlongPlotBoard

from colorbar_and_scale import *
from ui_control_colorbar import ColorBarView, Ui_ColorBarEdit

from ui_about import Ui_AboutDialog

from ui_control_units import Ui_UnitsControl
#from unit_manager import UnitManager

from interactive import *

import settings
import ui_resources

class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        """ Lots steps here:
            1. create/initialise non-UI components
            2. create/initialise UI components
            3. connect these components
            4. put some of them into dock windows
            5. load commands and initialise undo stack
            6. connect some component to commands
            7. create menus
            8. create toolbars
            9. initialise interactive console
        """
        super(MainWindow, self).__init__(parent)

        #-----------------------------------------------------------
        # setup ui components etc.
        self.setWindowTitle("Tim isn't Mulgraph")

        #-----------------------------------------------------------
        # abstract objects: data/model/scene etc
        self.model = Model()

        self.scene_manager = SceneManager()
        self.mode_manager = getModeManager()
        self.arrow_manager = ArrowScaleManager()
        self.colorbar_manager = ColorBarManager()
        #self.unit_manager = UnitManager()

        #-----------------------------------------------------------
        # ui/visible components
        self.main_view = TimGraphicsView()
        self.colorbar_view = ColorBarView(width=1.5, height=5, dpi=100, ax_aspect=15)

        self.scene_control = Ui_SceneControl()
        self.points_board = Ui_PointsBoard()
        self.time_control = Ui_TimeControl()
        self.mode_control = Ui_SceneModeControl()

        self.info_board = Ui_InfoBoard()
        self.selection_board = Ui_EditRocktypeBoard()

        self.unit_control = Ui_UnitsControl() #Unit control dialog

        # modeless dialogs:

        # unfortunately at the moment model object is needed before the loading
        # of the plot board
        self.plot_board_depth = makeDownholePlotBoard(self.model, self.scene_manager, self)
        self.plot_board_history = makeHistoryPlotBoard(self.model, self.scene_manager, self)
        self.plot_board_along = makeAlongPlotBoard(self.model, self.scene_manager, self)

        self.gener_table = Ui_GenerTable(self)
        self.rock_table = Ui_Rocktypes(self.model, self)
        self.colorbar_control = Ui_ColorBarEdit(self)

        self.about = Ui_AboutDialog(self)

        #-----------------------------------------------------------
        #possibly unit control should be connected here? rather than the other way TODO: look into this
        # connections
        self.scene_manager.connectModel(self.model)
        self.scene_manager.connectModeManager(self.mode_manager)
        self.scene_manager.connectColorBars(self.colorbar_manager)
        self.scene_manager.connectArrowScaleManager(self.arrow_manager)
        self.scene_manager.connectInfoBoard(self.info_board)
        self.scene_manager.connectSelectionBoard(self.selection_board)
        self.scene_manager.connectGenerTable(self.gener_table)
        self.scene_manager.connectPointsBoard(self.points_board)

        self.time_control.connectModel(self.model)
        self.info_board.connectModel(self.model)
        self.selection_board.connectModel(self.model)
        self.selection_board.connectRockTable(self.rock_table)
        self.gener_table.connectModel(self.model)
        self.rock_table.connectModel(self.model)
        self.unit_control.connectModel(self.model)


        self.colorbar_control.connectColorBarManager(self.colorbar_manager)

        self.scene_control.connectSceneManager(self.scene_manager)
        self.scene_control.connectView(self.main_view)
        self.scene_control.connectArrowScaleManager(self.arrow_manager)
        self.scene_control.connectColorBarManager(self.colorbar_manager)
        self.scene_control.connectColorBarView(self.colorbar_view)
        self.scene_control.connectColorBarControl(self.colorbar_control)

        #-----------------------------------------------------------
        # put them on screen, main window's central widget can't be layouts
        self.setCentralWidget(self.main_view)
        self.colorbar_view.setParent(self.main_view)

        # add controls into dock windows
        # NOTE: dockwidgets can be hidden by either closing or stacked when tabified
        #       to show a hidden tabified dockwidget self.dock_windows['xx'].raise_()
        #       to show a closed dockwidget self.dock_windows['xx'].show()
        # protected widgets will have close feature disabled
        self.dock_windows = {}
        self.createAndAddDockWindow("Scene",
            self.scene_control,
            Qt.LeftDockWidgetArea|Qt.RightDockWidgetArea,
            Qt.LeftDockWidgetArea,
            tabify_on='',
            protected=True)
        self.createAndAddDockWindow("Edit Rocktypes",
            self.selection_board,
            Qt.LeftDockWidgetArea|Qt.RightDockWidgetArea,
            Qt.LeftDockWidgetArea)
        self.createAndAddDockWindow("Line",
            self.points_board,
            Qt.LeftDockWidgetArea|Qt.RightDockWidgetArea,
            Qt.LeftDockWidgetArea,
            'Edit Rocktypes')
        self.createAndAddDockWindow("Info",
            self.info_board,
            Qt.LeftDockWidgetArea|Qt.RightDockWidgetArea,
            Qt.LeftDockWidgetArea,
            'Line')
        self.createAndAddDockWindow("Time",
            self.time_control,
            Qt.TopDockWidgetArea|Qt.BottomDockWidgetArea,
            Qt.BottomDockWidgetArea)
        # if main window needs to be shrinked (for low res screen), try tabify
        # other tall widgets behand the tallest
        self._dock_main_widget = 'Scene'
        self._dock_tabify_order = ["Edit Rocktypes", "Line", "Info",]

        #-----------------------------------------------------------
        # non-essential UI relationships (for better look etc)
        self.connect(self.dock_windows['Scene'],
            SIGNAL("dockLocationChanged (Qt::DockWidgetArea)"),
            self._scenecontrolDirection)
        self._scenecontrolDirection(Qt.LeftDockWidgetArea)

        #-----------------------------------------------------------
        # These must come before adding menus etc, (all that deals with QActions)
        # TODO: should self.shortcuts and self.icons be command classes' attribute?
        self.shortcuts = self.loadKeymap()
        self.icons = {
            'create_geo'              : ':/new_geo.png',
            'create_dat'              : ':/new_dat.png',
            'open_geo'              : ':/open_geo.png',
            'open_dat'              : ':/open_dat.png',
            'open_lst'              : ':/open_lst.png',
            'save_dat'              : ':/save_dat.png',
            'console_start'         : ':/interactive.png',
            'draw_line'             : ':/create_line.png',
            'pick_rock_source_mode' : ':/single_select.png',
            'pick_blocks_mode'      : ':/multi_select.png',
            'apply_rock'            : ':/down.png',
            'reset_scale'           : ':/reset_view.png',
            'zoom_in'               : ':/zoom_in.png',
            'zoom_out'              : ':/zoom_out.png',
            'show_plot_history'     : ':/plot_history.png',
            'show_plot_depth'       : ':/plot_depth.png',
            'show_gener_table'      : ':/gener_table.png',
            'show_rocks_table'      : ':/rocks_table.png',
            'block_text_larger'     : ':/font_larger.png',
            'block_text_smaller'    : ':/font_smaller.png',
            'run_model'             : ':/run_model.png',
        }
        self.init_disabled = {
            'show_unit_controls'    : ["model_lst_reloaded", 'model', 'self.model.lst()'],
        }

        ##actions which should initially be disabled are specified in the format
        # 'command name' : ['signal', 'attribute to examine for the signal', 'executable condition']
        #when the signal is emmitted if the executable condition then the action will be enabled
        #condition may be optionally left off and will be assumed to be true whenever the signal is emitted

        #eg adding: 'open_dat'              : ["model_geo_reloaded", 'model', 'self.model.geo()'],
        #would prevent the open_dat action in menu/toolbar from being enabled until a geometry file was loaded

        # commands and undo stack
        self.loadCommands()
        self._undo_stack = []

        # actions
        self._current_path = ''

        # connect some widget signal to application level actions
        self.scene_manager.connectSignalFromScene("browse_mode_double_clicked",self.commandCallable("show_info_board"))
        #self.scene_manager.connectSignalToScene(obj,signal_name,scene_method_name)
        QObject.connect(self.points_board,SIGNAL("create_slice"),self.commandCallable("create_slice"))
        QObject.connect(self.selection_board, SIGNAL("show_rock_table"), self.commandCallable("show_rocks_table"))
        QObject.connect(self.scene_control, SIGNAL("show_colorbar_view"), self.commandCallable("show_colorbar"))
        QObject.connect(self.colorbar_view, SIGNAL("double_clicked"), self.commandCallable("show_colorbar_control"))

        #feels messy to have so many connections - look into a better way of doing this - this is what ui_control_edit_rock does
        QObject.connect(self.time_control, SIGNAL("show_units_control"), self.commandCallable("show_unit_controls"))
        QObject.connect(self.plot_board_depth, SIGNAL("show_units_control"), self.commandCallable("show_unit_controls"))
        QObject.connect(self.plot_board_history, SIGNAL("show_units_control"), self.commandCallable("show_unit_controls"))
        QObject.connect(self.plot_board_along, SIGNAL("show_units_control"), self.commandCallable("show_unit_controls"))

        #I need to figure out my connections a bit better pls - also this definitely shouldn't go here
        QObject.connect(self.unit_control,SIGNAL('updatedTimeOffset'),self.time_control.updateLabel)
        QObject.connect(self.unit_control,SIGNAL('updatedTimeOffset'),self.plot_board_history.refreshUnits)

        self.setupDialogActions()

        #-----------------------------------------------------------
        # menus
        file_menu = self.menuBar().addMenu("&File")
        self.addMenuItems(file_menu, [
            'open_geo',
            'open_dat',
            'open_lst',
            'save_dat',
            'run_model',
            None,
            'create_geo',
            'create_dat',
            None,
            'console_start'
            ])

        edit_menu = self.menuBar().addMenu("&Edit")
        mode_submenu = QMenu("&Mode",edit_menu)
        rock_submenu = QMenu("Edit &Rocktypes",edit_menu)
        for a in self.mode_control.actionlist():
            mode_submenu.addAction(a)

        self.addMenuItems(edit_menu, [
            mode_submenu,
            None,
            rock_submenu,
            None,
            'show_gener_table',
            'show_rocks_table',
            None,
            'show_unit_controls',
            None,
            'select_inside_polygon',
            'select_intersect_polyline',
            # 'reset_rocktype_color',
            # 'group_rocktype_by_name',
            ])

        edit_rock_actions = [
            'pick_rock_source_mode',
            'pick_blocks_mode',
            'apply_rock'
            ]
        for a in edit_rock_actions:
            rock_submenu.addAction(self.getAction(a))


        # special dynamic menu
        scene_menu = self.menuBar().addMenu("&Scene")
        self.connect(scene_menu, SIGNAL("aboutToShow()"), self.updateSceneMenu)
        self.scene_list_submenu = QMenu("Goto &Slice/Layer",scene_menu)
        self.scene_color_submenu = QMenu("Show Variable as &Color",scene_menu)
        self.scene_text_submenu = QMenu("Show Variable as &Text",scene_menu)
        self.scene_arrow_submenu = QMenu("Show Flow as &Arrows",scene_menu)
        self.addMenuItems(scene_menu, [
            'draw_line',
            'create_slice',
            self.scene_list_submenu,
            None,
            self.scene_color_submenu,
            self.scene_text_submenu,
            self.scene_arrow_submenu,
            None,
            'show_info_board',
            ])

        graph_menu = self.menuBar().addMenu("&Graph")
        self.addMenuItems(graph_menu, [
            'show_plot_history',
            'show_plot_depth',
            'show_plot_along',
            'batch_plot',
            ])

        view_menu = self.menuBar().addMenu("&View")
        well_label_submenu = QMenu("&Well Label",view_menu)
        well_label_actions = [
            'well_text_show',
            'well_text_hide',
            'well_text_larger',
            'well_text_smaller',
            'make_wells_blue']
        for a in well_label_actions:
            well_label_submenu.addAction(self.getAction(a))
        self.addMenuItems(view_menu, [
            'reset_scale',
            'zoom_in',
            'zoom_out',
            None,
            'block_text_larger',
            'block_text_smaller',
            None,
            'show_block_name',
            well_label_submenu
            ])

        help_menu = self.menuBar().addMenu("&Help")
        self.addMenuItems(help_menu, [
            'online_help',
            'online_getting_started',
            'online_howtos',
            'report_issue',
            None,
            'open_settings_dir',
            'open_log_file',
            None,
            'about',
            'about_qt',
            ])

        #-----------------------------------------------------------
        # tool bars
        fileToolbar = self.addToolBar("File")
        fileToolbar.setObjectName("FileToolBar")
        self.addMenuItems(fileToolbar, [
            'open_geo',
            'open_dat',
            'open_lst',
            None,
            'save_dat',
            'run_model',
            None,
            'create_geo',
            'create_dat',
            ])

        viewToolbar = self.addToolBar("View")
        viewToolbar.setObjectName("ViewToolBar")
        self.addMenuItems(viewToolbar, [
            'reset_scale',
            'zoom_in',
            'zoom_out',
            'block_text_larger',
            'block_text_smaller'
            ])

        modeToolbar = self.addToolBar("Mode")
        modeToolbar.setObjectName("ModeToolBar")
        for b in self.mode_control.buttonlist():
            modeToolbar.addWidget(b)

        miscToolbar = self.addToolBar("Plots and Tables")
        miscToolbar.setObjectName("MiscToolBar")
        self.addMenuItems(miscToolbar, [
            'show_plot_history',
            'show_plot_depth',
            'show_gener_table',
            'show_rocks_table'
            ])

        self.statusBar().showMessage("To start with, open a geometry file.")

        # interactive console, setup scope and create console
        def getPyQtScope():
            import PyQt4.QtCore as a
            import PyQt4.QtGui as b
            import interactive as c
            return dict(a.__dict__.items()+b.__dict__.items()+c.__dict__.items())
        # TODO: sort out what to expose in console tidily, with proper help
        scope = dict(getPyQtScope().items()+[(k,getattr(self,k)) for k in dir(self)])
        scope = dict([(k,getattr(self,k)) for k in dir(self)])
        # scope = {'run_command': self.run_command}
        self.console = Console(scope,self)
        # I think delete later stops the console from re-start after stopped by ^Z
        #self.connect(self.console, SIGNAL("finished()"),self.console, SLOT("deleteLater()"))

        # must be at the end, so can deal with dock widgets etc.
        self.loadSettings()

        #------------------ End of __init__() --------------------------------

    def list_commands(self):
        """ Call this from Interactive Console to  get a list available commands """
        cmds = sorted(self.available_commands.keys())
        return cmds

    def run_command(self, cmd_name, *args, **kwargs):
        """ Call this to execute main window level commands from interactive
        console. """
        # detecting which thread this method is called from
        is_gui_thread = QThread.currentThread() is QApplication.instance().thread()
        if not is_gui_thread:
            logging.debug('Command %s executed from another thread.' % cmd_name)
            a = QObject()
            a.moveToThread(QApplication.instance().thread())
            QObject.connect(a, SIGNAL('run_cmd'), self.runCommand)
            a.emit(SIGNAL('run_cmd'), cmd_name, *args, **kwargs)
            del a
        else:
            self.runCommand(cmd_name, *args, **kwargs)

    def loadCommands(self):
        """ collect all available commands from module(s) """
        import main_commands
        import inspect
        all_classes = dict(inspect.getmembers(main_commands,inspect.isclass))

        import glob, imp, os, ntpath
        path = settings.settingsDir()

        if os.path.exists(path):
            logging.info('Loading user commands at: ' + path)
            # Change the current directory to that of the module. It's not safe to
            # just add the modules directory to sys.path, as that won't accept
            # unicode paths on Windows
            oldpath = os.getcwdu()
            os.chdir(path)

            for fname in glob.glob(path + '/*.py'):
                try:
                    modulename, ext = os.path.splitext(os.path.basename(fname))
                    m_info = imp.find_module(modulename, [settings.settingsDir()])
                    m = imp.load_module(modulename, *m_info)
                    all_classes.update(dict(inspect.getmembers(m,inspect.isclass)))
                    # Windows paths can use either backslash or forward slash as path
                    # separator. Therefore, the ntpath module (which is equivalent to
                    # os.path when running on windows) will work for all paths on all
                    # platforms. (Lauritz V. Thaulow, Stack Overflow)
                    logging.info('  '+ ntpath.basename(fname))
                    if m_info[0]:
                        m_info[0].close()
                except Exception as e:
                    logging.error("Error loading user code %s" % fname)
                    logging.error("  " + str(e))
            # Restore the current directory
            os.chdir(oldpath)
        else:
            logging.info('user settings not found, skip loading user commands.')

        from timgui.base_commands import WindowCommand
        self.available_commands = {}
        for k,c in all_classes.items():
            if c.__bases__:
                if issubclass(c, WindowCommand) and (c is not WindowCommand):
                    c_name = c.name()
                    self.available_commands[c_name] = c
                    self.addAction(self.getAction(c_name))

            # if k.endswith('_command'):
                # self.available_commands[k[:-len('_command')]] = all_classes[k]
        return

    def runCommand(self, cmd_name, *args, **kwargs):
        """ This is the actual method of running command, a lot of api only work
        when called from the same thread as main gui.  Hence this should only be
        called from the main GUI thread. """

        # logging.debug("cmd_name='%s'" % cmd_name)
        # logging.debug("args='%s'" % str(args))
        # logging.debug("kwargs='%s'" % str(kwargs))

        # create command object, the dict controls the accessing of command
        # TODO: decide how much to expose, and how components are accessed.
        try:
            cmd = self.available_commands[cmd_name]({
                "main_window":self,
                })
        except KeyError:
            logging.error("Command '%s' does not exist.  Use 'list_commands()' to see all commands." % cmd_name)
            return
        # run command with arguments
        cmd.run(*args, **kwargs)

        def canUndo(cmd):
            """ check if a command supports undo """
            from base_commands import WindowCommand
            # if a command class defined .undo(), then it supports undo
            if 'undo' in cmd.__class__.__dict__:
                return True
            return False

        # TODO: figure out how to deal with issues with certain operations will
        # 'block' undo, and should clear undo stack in that situations?
        if canUndo(cmd):
            logging.debug("Command '%s' added to undo stack." % cmd_name)
            self._undo_stack.append(cmd)

    def undoCommand(self):
        """ undo the last command saved in stack.  Note: this should not be
        called from console, use .run_command('undo') instead. """
        if len(self._undo_stack) > 0:
            cmd = self._undo_stack.pop()
            logging.info("Undo command '%s'" % cmd.name)
            cmd.undo()
        else:
            logging.info('Nothing to undo.')

    def commandCallable(self, cmd_name, *args, **kwargs):
        """ Creates and returns a callable of command with argument, usually
        used as slot for connecting to signals. The returned callable is
        actually a partial object calling .runCommand().

        If a slot of connection is a lambda function or a partial function then
        its reference count is automatically incremented to prevent it from
        being immediately garbage collected.  Source:
        http://pyqt.sourceforge.net/Docs/PyQt4/old_style_signals_slots.html
        """
        if cmd_name not in self.available_commands:
            logging.error("Command '%s' is not found." % cmd_name)
            return

        from functools import partial
        p = partial(self.runCommand,cmd_name, *args, **kwargs)
        return p

    def getAction(self, name):
        """ Get a QAction object with specified name.  An internal dict
        self._actions will be searched first.  If it does not already exist, a
        new action will be created from (command) name.  The new action will be
        kept for reuse. """
        # QMainWindow.actions() is an Qt function returns a list of actions
        # which has main window as parent.
        if not hasattr(self,'_actions'):
            self._actions = {}
        if name not in self._actions:
            self._actions[name] = self.createAction(name)
        return self._actions[name]

    def createAction(self, cmd_name, *args, **kwargs):
        """ Create a QAction from command name and arguments.  The action is
        usually used in menus and toolbars.   The created action will
        automaticlly have the MainWindow as parent.
        """
        if cmd_name not in self.available_commands:
            logging.error("Command '%s' is not found." % cmd_name)
            return

        # dummy cmd used to get text and tip etc...
        cmd = self.available_commands[cmd_name]({})

        action = QAction(cmd.caption, self)
        action.setToolTip(cmd.tip)
        action.setStatusTip(cmd.tip)

        from functools import partial

        if cmd_name in self.shortcuts['main']:
            action.setShortcuts(self.shortcuts['main'][cmd_name])
            action.setShortcutContext(Qt.ApplicationShortcut)
        if cmd_name in self.icons:
            action.setIcon(QIcon(self.icons[cmd_name]))
        #check if action should be only conditionally enabled and if so attach a connection to control that
        if cmd_name in self.init_disabled:
            action.setEnabled(False)
            args = self.init_disabled[cmd_name]
            sig = args[0]
            obj = getattr(self,args[1])
            if args[2]:
                condition = args[2]
            else:
                condition = 'True'
            action.updateEnabled = partial(self.enableAction,action,condition)
            QObject.connect(obj,SIGNAL(sig),action.updateEnabled)


        # the partial is stored as a property of action object, so it can be
        # triggered as slot later when action is 'triggered()'
        action.run_cmd_ = partial(self.runCommand,cmd_name, *args, **kwargs)
        QObject.connect(action, SIGNAL("triggered()"), action.run_cmd_)



        return action

    def enableAction(self,action,strCondition):
        """ enables action (usually in menu bar or toolbar) if
        strCondition is met. StrCondition is given as a string
        and evaluated within the function"""
        condition = eval(strCondition)
       # action = self.getAction('cmd_name')
        if condition:
            action.setEnabled(True)
        else:
            action.setEnabled(False)

    def addMenuItems(self, target, actions):
        """ target could be menu or toolbar etc, actions can be a
        tuple/list of actions.  If an action is None, a separator will
        be added. """
        for action in actions:
            if action is None:
                target.addSeparator()
            elif type(action) is QMenu:
                target.addMenu(action)
            elif type(action) is str:
                target.addAction(self.getAction(action))
            elif type(action) is QAction:
                target.addAction(action)

    def setupDialogActions(self):
        """ add actions into dialogs, both dialog's own and app level ones """
        def addPrivateActions(widget, method_name, name, ks_list):
            """ create and add QActions that directly linked to a widget's own
            method, used for local shortcut keys """
            a = QAction(name, widget)
            a.setShortcuts(ks_list)
            QObject.connect(a, SIGNAL("triggered()"), getattr(widget, method_name))
            widget.addAction(a)

        widgets = {
            'plot_board': [self.plot_board_depth, self.plot_board_history]
        }
        for wn, wlist in widgets.iteritems():
            for w in wlist:
                for cn, ks in self.shortcuts[wn].iteritems():
                    for k in ks:
                        addPrivateActions(w, cn, cn, k)

    def updateSceneMenu(self):
        """ Updates several dynamic sub-menus in Scene menu.  Actions with
        partial are created to be children of the sub menus, and they will be
        destroyed when clear(). """
        from functools import partial
        def comboBoxToSubMenu(cb,sub):
            """ fill the submenu (QMenu) with items in a QComboBox """
            for i in range(cb.count()):
                p = partial(cb.setCurrentIndex,i)
                t = cb.itemText(i)
                action = sub.addAction(t)
                QObject.connect(action, SIGNAL("triggered()"), p)

        def listWidgetToSubMenu(lw,sub):
            """ fill the submenu (QMenu) with items in a QListWidget """
            for i in range(lw.model().rowCount()):
                p = partial(lw.setCurrentRow,i)
                t = lw.model().data(lw.model().index(i,0)).toString()
                action = sub.addAction(t)
                QObject.connect(action, SIGNAL("triggered()"), p)

        # Note that when a slot is a Python callable its reference count is not
        # increased. This means that a class instance can be deleted without
        # having to explicitly disconnect any signals connected to its methods.
        # However, if a slot is a lambda function or a partial function then its
        # reference count is automatically incremented to prevent it from being
        # immediately garbage collected.  Source:
        # http://pyqt.sourceforge.net/Docs/PyQt4/old_style_signals_slots.html

        self.scene_list_submenu.clear()
        listWidgetToSubMenu(self.scene_control.LayerSliceSelector,self.scene_list_submenu)

        self.scene_color_submenu.clear()
        self.scene_text_submenu.clear()
        self.scene_arrow_submenu.clear()
        comboBoxToSubMenu(self.scene_control.ColorSelector,self.scene_color_submenu)
        comboBoxToSubMenu(self.scene_control.TextSelector,self.scene_text_submenu)
        comboBoxToSubMenu(self.scene_control.ArrowSelector,self.scene_arrow_submenu)

    def _scenecontrolDirection(self,dock_widget_area):
        if dock_widget_area in [Qt.LeftDockWidgetArea, Qt.RightDockWidgetArea]:
            self.scene_control.layout().setDirection(QBoxLayout.TopToBottom)
        else:
            self.scene_control.layout().setDirection(QBoxLayout.LeftToRight)

    def createAndAddDockWindow(self,title,widget,allow_areas,initial_area,
        tabify_on='', protected=False):
        """ create and add a QDockWidget, it will be inserted and
        initialised in the main window (self) and kept in the dictionary
        self.dock_window.  Nothing will be done if the title already been
        used, if tabify_on is not empty, new dockwidget will be placed
        (tabified) on top of the named dockwidget """
        if title not in self.dock_windows:
            dock = QDockWidget(title)
            # unique names must be given for save/restoreState() to work
            dock.setObjectName(title)
            dock.setWidget(widget)
            dock.setAllowedAreas(allow_areas)
            if protected:
                dock.setFeatures(QDockWidget.DockWidgetMovable | QDockWidget.DockWidgetFloatable)
            self.addDockWidget(initial_area,dock)
            self.dock_windows[title] = dock
        if tabify_on and tabify_on in self.dock_windows:
            self.tabifyDockWidget(self.dock_windows[tabify_on],dock)

    def shrinkMainWindow(self):
        """ shrink required window height by moving other widgets behind the
        specified main dock window.  This will be continued until
        minimumSizeHint is shorter than the current height, or until runs out of
        widget to move. """
        def moveDockWidgetBehind(ww1, ww2):
            """ Move the first dock widget to be behind the second one. """
            self.splitDockWidget(ww2, ww1, Qt.Vertical)
            self.tabifyDockWidget(ww1, ww2)

        ww_main = self.dock_windows[self._dock_main_widget]
        for i, w in enumerate(self._dock_tabify_order):
            if self.minimumSizeHint().height() <= self.height():
                break
            if w in self.dock_windows:
                ww = self.dock_windows[w]
                if ww not in self.tabifiedDockWidgets(ww_main):
                    logging.info('Move dock widget %s' % w)
                    moveDockWidgetBehind(ww, ww_main)


    def loadKeymap(self):
        """ load keymap.json from code directory first, then from settings
        directory (if exists).  They are joined together as long list, with
        later entries able to overwrite earlier entries, if smae shorcut keys
        used more than once.  This returns a dict keyed by either 'main' or
        other widget names.  Each of these values is also a dict with command
        names as keys. """
        import json

        # load default from code directory first
        fname = settings.getCodePath('keymap.json')
        f = open(fname,'r')
        jlist = json.load(f)
        f.close()
        # load from user settings dir if file exists
        fname = settings.getSettingsPath('keymap.json', fallback=False)
        try:
            f = open(fname,'r')
            jlist += json.load(f)
            f.close()
        except IOError:
            pass

        all_key_strings = {}
        for obj in jlist:
            k = str(QKeySequence(obj['key']).toString())
            if k:
                c = str(obj['command'])
                # if widget not specified, assume it's for the main app
                if 'widget' in obj:
                    w = str(obj['widget'])
                else:
                    w = 'main'
                if w not in all_key_strings:
                    all_key_strings[w] = {}
                # overwrites if k exists
                all_key_strings[w][k] = c
            else:
                # lead to empty keysequence
                logging.error('Keymap "%s" not recognised.' % obj['key'])

        def convert_keymap(kstr_cmd):
            """ convert dict {key string: command} into {command: [QKeySequence,]} """
            cmd_klist = {}
            for k, c in kstr_cmd.iteritems():
                if c not in cmd_klist:
                    cmd_klist[c] = []
                cmd_klist[c].append(QKeySequence(k))
            return cmd_klist

        converted = {}
        for k, i in all_key_strings.iteritems():
            converted[k] = convert_keymap(i)

        return converted

    def saveSettings(self):
        s = settings.appSettings()

        s.beginGroup("main_window")
        # TODO: pos() and size() is human readable and ByteArray is not
        # s.setValue("size", self.size())
        # s.setValue("pos", self.pos())
        s.setValue("geometry", self.saveGeometry())
        s.setValue("state", self.saveState())
        s.endGroup()

        # always save current directory
        s.setValue("current_directory", self._current_path)

        # NOTE if save settings back, all comments will disapear
        # s = settings.userSettings()
        # s.beginGroup("test")
        # s.setValue("int", 9)
        # s.setValue("str", 'abs')
        # s.setValue("load_plots_on_start", True)
        # s.setValue("path", settings.appSettingsFile())
        # s.endGroup()

    def loadSettings(self):
        # if fail to load settings, just use original
        try:
            s = settings.appSettings()
            s.beginGroup("main_window")
            # self.resize(s.value("size", QSize(800,800)).toSize())
            # self.move(s.value("pos", QPoint(100,100)).toPoint())
            self.restoreGeometry(s.value("geometry").toByteArray())
            self.restoreState(s.value("state").toByteArray())
            s.endGroup()

            # default group is 'General'
            wd = str(s.value("current_directory", "").toString())

            s = settings.userSettings()

            if s.contains("initial_directory"):
                # if "initial_directory" then use it, overwrite previous directory
                wd = str(s.value("initial_directory", "").toString())
                import os.path
                wd = os.path.normpath(wd)
            self._current_path = wd

            # s.beginGroup("test")
            # if s.contains("path"):
            #     p = str(s.value("path").toString())
            #     from os.path import getsize
            #     print p, getsize(p)
            # s.endGroup()
        except Exception as e:
            self._current_path = '.'
            logging.error('Failed to load application settings!')
            logging.error('Setting file: %s' % settings.appSettingsFile())
            logging.error(str(e))

    def resizeEvent(self, e):
        min_h = self.minimumSizeHint().height()
        cur_h = self.height()
        if min_h > cur_h:
            logging.info('Layout may be changed to fit window height, req: %i, cur: %i' % (min_h, cur_h))
            self.shrinkMainWindow()

    def closeEvent(self, event):
        # close the interactive console as well, otherwise it will be
        # there if user close the window
        #con.push('exit()')
        self.saveSettings()

        # save settings for children widgets
        to_save = [
            self.plot_board_history,
            self.plot_board_depth,
            self.plot_board_along,
            self.colorbar_view,
            self.colorbar_control,
            self.gener_table,
            self.rock_table,

            self.arrow_manager,
            self.colorbar_manager,
            ]
        for ts in to_save:
            ts.saveSettings()

        event.accept()


    #-------------------------------------------------------------------
    # should I separate out these dialog pop-up actions?
