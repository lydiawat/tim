"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

from t2tables import t2_Rocktypes,ww_Rocktypes
import settings

class Ui_Rocktypes(QDialog):
    def __init__(self, model, parent=None):
        super(Ui_Rocktypes, self).__init__(parent)
        self._model = model

        self.rocktypes = None
        # keeps a copy of .rocktypes (QAbstractTableModel) once initialised,
        # avoid doing it unnecessarily if keep switching between simulators
        self.t2_rock_model = None
        self.ww_rock_model = None

        self.table = QTableView()

        self.setWindowIcon(QIcon(":/rocks_table.png"))
        self.sel_button = QPushButton('&Select Rock Type')
        self.connect(self.sel_button,SIGNAL("clicked ()"),self.selRock)
        self.sel_button.setEnabled(False)

        self.add_button = QPushButton('&Add Rock Type')
        self.connect(self.add_button,SIGNAL("clicked ()"),self.addRock)
        self.add_button.setEnabled(False)

        self.del_button = QPushButton('&Remove Rock Type')
        self.connect(self.del_button,SIGNAL("clicked ()"),self.delRock)
        self.del_button.setEnabled(False)

        layout = QBoxLayout(QBoxLayout.TopToBottom)
        layout.addWidget(self.table)
        layout_1 = QBoxLayout(QBoxLayout.LeftToRight)
        layout_1.addWidget(self.sel_button)
        layout_1.addStretch(1)
        layout_1.addWidget(self.add_button)
        layout_1.addWidget(self.del_button)
        layout.addLayout(layout_1)
        self.setLayout(layout)
        self.setWindowTitle('Rocktype Table')

        self.setUpRockModel()
        self.loadSettings()

    def setUpRockModel(self):
        """ Responsible for setting the rock model (table type) (t2 or waiwera)
        for the view to display. Called on initialisation, and whenever the
        "model_dat_reloaded" signal is received Should be called after
        self.model has been updated, so that it is checking the new dat object.
        """
        if self._model.dat():
            # get current model's dat type (dat_type is either 'ww', 't2' or
            # None) and enable buttons
            dat_type = {
                'waiwera': 'ww',
                'autough2': 't2',
                'tough2': 't2',
            }[self._model.dat_simulator]

            self.sel_button.setEnabled(True)
            self.add_button.setEnabled(True)
            self.del_button.setEnabled(True)
        else:
            dat_type = None

            self.sel_button.setEnabled(False)
            self.add_button.setEnabled(False)
            self.del_button.setEnabled(False)

        # find current rockmodel type (curr_rock_mod is either 'ww','t2' or None)
        if self.rocktypes:
            curr_rock_mod = type(self.rocktypes).__name__.split('_')[0]
        else:
            curr_rock_mod = None

        # if current model matches required one, no need to change rock model
        if dat_type == curr_rock_mod: return

        # set self.rocktypes to be an instance of the required rockmodel type
        if dat_type == 'ww':
            if not self.ww_rock_model:
                # save the initialised model in a variable to prevent
                # unnecessary initialisation if keep switching between t2 and ww
                self.ww_rock_model = ww_Rocktypes(self._model)
            self.rocktypes = self.ww_rock_model
        elif dat_type == 't2':
            if not self.t2_rock_model:
                self.t2_rock_model = t2_Rocktypes(self._model)
            self.rocktypes = self.t2_rock_model
        elif dat_type is None:
            self.rocktypes = None

        if curr_rock_mod:
            # if a previous rockmodel already loaded, delete the selectionModel
            # and replace the rockmodel
            m = self.table.selectionModel()
            self.table.setModel(self.rocktypes)
            m.deleteLater()
        else:
            # otherwise just replace the rockmodel
            self.table.setModel(self.rocktypes)

        if self.rocktypes:
            # when insert new rocktype, scroll to it
            QObject.connect(
                self.rocktypes, SIGNAL('rowsInserted (const QModelIndex&,int,int)'),
                self.table.scrollToBottom)

    def saveSettings(self):
        s = settings.appSettings()
        s.beginGroup("rocks_table")
        s.setValue("geometry", self.saveGeometry())
        s.endGroup()

    def loadSettings(self):
        s = settings.appSettings()
        s.beginGroup("rocks_table")
        if s.contains("geometry"):
            self.restoreGeometry(s.value("geometry").toByteArray())
        s.endGroup()

    def addRock(self):
        r = self.rocktypes.rowCount()
        self.rocktypes.insertRows(r)
        r = self.rocktypes.rowCount()
        index = self.rocktypes.index(r,1) #move to rock
        self.table.setFocus()
        self.table.setCurrentIndex(index)
        #self.table.edit(index)

    def delRock(self):
        r = self.table.currentIndex().row()
        self.rocktypes.removeRows(r)

    def selRock(self):
        r = self.table.currentIndex().row()
        if r < 0: return
        if self._model.dat_simulator in ['autough2', 'tough2']:
            name = self._model.dat().grid.rocktypelist[r].name
        elif self._model.dat_simulator in ['waiwera']:
            name = r
        self.emit(SIGNAL("rocktype_selected"),name)
        print name, 'selected'

    def _modelReloaded(self):
        """ this will be called indirectly upon initialistion, whenever a model
        is connected, or when dat is reloaded
        """
        self.setUpRockModel()

    def connectModel(self,model):
        """ only connects to a model at any given time, all entries re-created
        with new model
        """
        if self._model:
            self.disconnect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
            if self.t2_rock_model: self.t2_rock_model.connectModel(self._model)
            if self.ww_rock_model: self.ww_rock_model.connectModel(self._model)

            self._modelReloaded()

def test_Ui_Rocktypes():
    import sys
    app = QApplication(sys.argv)
    from t2model import Model

    #model1 = Model()
    #model1.loadFileMulgrid('D:\\waiwera_files\\waiwera_example\\gOH35446.dat')
    #model1.loadFileDataInput('D:\\waiwera_files\\waiwera_example\\OH35446_ns_MJO001.dat')

    #model2 = Model()
    #model2.loadFileMulgrid('D:\\waiwera_files\\waiwera_example\\gwai41457_18.dat')
    #model2.loadFileDataInput('D:\\waiwera_files\\waiwera_example\\wai41457pr_7566.dat')

    model = Model()
    model.loadFileMulgrid('D:\\waiwera_files\\waiwera_example\\g2medium.dat')
    model.loadFileDataInput('D:\\waiwera_files\\waiwera_example\\2DM002dodge.json')
    #model1.loadFileMulgrid('D:\\waiwera_files\\waiwera_example\\gOH35446.dat')
    #model1.loadFileDataInput('D:\\waiwera_files\\waiwera_example\\OH35446_ns_MJO001.dat')

    test = Ui_Rocktypes(model)
    test.show()
    app.exec_()
 #   return model
