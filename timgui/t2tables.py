"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from t2model import Model
from misc_funcs import find_unused_name, valid_t2_name
import logging

from t2grids import rocktype
from waiwera_input import rock_cells, WAIWERA_ROCK_DFALT
from t2data import t2generator

class t2_Rocktypes(QAbstractTableModel):
    """ A class that keeps track of rocktypes in dat.grid (for t2 data). To be used
    with QTableView. See Qt's Model/View programming. """
    NAME, DROK, POR, PER1, PER2, PER3, CWET, SPHT,FRQ = 0,1,2,3,4,5,6,7,8
    def __init__(self,model=None):
        super(t2_Rocktypes, self).__init__()
        self._model = model
        self.connectModel(model)

    def rowCount(self, index=QModelIndex()):
        try:
            return self._model.dat().grid.num_rocktypes
        except:
            return 0

    def columnCount(self, index=QModelIndex()):
        return 9 # just first line of ROCKS in data file

    def flags(self, index):
        if not index.isValid(): return Qt.ItemIsEnabled
        if index.column() == self.FRQ: return Qt.ItemIsEnabled
        return Qt.ItemFlags(QAbstractTableModel.flags(self,index)|Qt.ItemIsEditable)

    def headerData(self, section, orientation, role = Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                if section == self.NAME:
                    return QVariant('Name')
                elif section == self.DROK:
                    return QVariant('DROK')
                elif section == self.POR:
                    return QVariant('POR')
                elif section == self.PER1:
                    return QVariant('PER(1)')
                elif section == self.PER2:
                    return QVariant('PER(2)')
                elif section == self.PER3:
                    return QVariant('PER(3)')
                elif section == self.CWET:
                    return QVariant('CWET')
                elif section == self.SPHT:
                    return QVariant('SPHT')
                elif section == self.FRQ:
                    return QVariant('FREQUENCY')
            elif orientation == Qt.Vertical:
                return QVariant(int(section+1))
        return QVariant()

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid() or not (0 <= index.row() < self.rowCount()):
            return QVariant()
        r = self._model.dat().grid.rocktypelist[index.row()]
        column = index.column()
        if role == Qt.DisplayRole:
            if column == self.NAME:
                return QVariant(r.name)
            elif column == self.DROK:
                return QVariant(r.density)
            elif column == self.POR:
                return QVariant(r.porosity)
            elif column == self.PER1:
                return QVariant('%e' % r.permeability[0])
            elif column == self.PER2:
                return QVariant('%e' % r.permeability[1])
            elif column == self.PER3:
                return QVariant('%e' % r.permeability[2])
            elif column == self.CWET:
                return QVariant(r.conductivity)
            elif column == self.SPHT:
                return QVariant(r.specific_heat)
            elif column == self.FRQ:
                return QVariant(self._model.dat().grid.rocktype_frequency(r.name))
        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if index.isValid() and 0 <= index.row() < self._model.dat().grid.num_rocktypes:
            r = self._model.dat().grid.rocktypelist[index.row()]
            column = index.column()
            v, ok = 0.0, True
            if column == self.NAME: #name is not editable so why is this here?
                new_name = str(value.toString())
                if len(new_name) > 5 or len(new_name.strip()) == 0:
                    logging.warning("Rocktype name of '%s' is invalid." % new_name)
                    return False
                try:
                    self._model.dat().grid.rename_rocktype(r.name, new_name)
                except Exception as e:
                    logging.warning(str(e))
                    return False
            elif column == self.DROK:
                v,ok = value.toFloat()
                if ok: r.density = v
            elif column == self.POR:
                v,ok = value.toFloat()
                if ok: r.porosity = v
            elif column == self.PER1:
                v,ok = value.toFloat()
                if ok: r.permeability[0] = v
            elif column == self.PER2:
                v,ok = value.toFloat()
                if ok: r.permeability[1] = v
            elif column == self.PER3:
                v,ok = value.toFloat()
                if ok: r.permeability[2] = v
            elif column == self.CWET:
                v,ok = value.toFloat()
                if ok: r.conductivity = v
            elif column == self.SPHT:
                v,ok = value.toFloat()
                if ok: r.specific_heat = v
            if not ok:
                return False
            print 'Rock Type %s edited.' % r.name
            self._model.setDatEdited()
            self.emit(SIGNAL("dataChanged (const QModelIndex&,const QModelIndex&)"),index,index)
            return True
        return False

    def insertRows(self, row, count=1, parent = QModelIndex()):
        if not self.ok():
            return False
        self.beginInsertRows(parent,row,row+count-1)
        r = rocktype()
        used_names = self._model.dat().grid.rocktype.keys()
        r.name = find_unused_name(used_names, r.name)
        self._model.dat().grid.add_rocktype(r)
        self.endInsertRows()
        self._model.setDatEdited()
        return True

    def removeRows(self, row, count=1, parent = QModelIndex()):
        if not self.ok():
            return False
        name = self._model.dat().grid.rocktypelist[row].name
        if self._model.dat().grid.rocktype_frequency(name) > 0:
            logging.warning("Rock Type '%s' is used, cannot be removed." % name)
            return False
        self.beginRemoveRows(parent,row,row+count-1)
        self._model.dat().grid.delete_rocktype(name)
        self.endRemoveRows()
        self._model.setDatEdited()
        return True

    def ok(self):
        """return True if there is a model, and there is dat().grid"""
        try:
            rlist = self._model.dat().grid.rocktypelist
            return True
        except AttributeError:
            return False
        except:
            return False

    def _modelReloaded(self):
        self.beginResetModel()
        self.endResetModel()

    def connectModel(self,model):
        """ only connects to a model at any given time, all entries re-created with new model """
        if self._model:
            self.disconnect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
            self._modelReloaded()

class ww_Rocktypes(QAbstractTableModel):
    """ A class that keeps track of rocktypes in dat (for waiwera data). To be used
    with QTableView. See Qt's Model/View programming. """
    HEADER = ['Rock Index', 'Name', 'Density', 'Porosity', 'Perm (1)', 'Perm (2)', 'Perm (3)', 'Wet Conductivity', 'Dry Conductivity', 'Specfic Heat','Frequency']
    WAIWERA_VALS = ['-', 'name', 'density','porosity',0,1,2,'wet_conductivity','dry_conductivity','specific_heat','-']
    def __init__(self,model=None):
        super(ww_Rocktypes, self).__init__()
        self._model = model
        self.connectModel(model)

    def rowCount(self, index=QModelIndex()):
        try:
            return len(self._model.dat()['rock']['types'])
        except:
            return 0

    def columnCount(self, index=QModelIndex()):
        return len(self.HEADER) #10 if using all three permeability values, 9 if a horizontal 2d model

    #def flags(self, index): #unsure about this
    #    print('do something')

    def flags(self, index):
        if not index.isValid(): return Qt.ItemIsEnabled
        if self.HEADER[index.column()] in ['Rock Index','Frequency']: return Qt.ItemIsEnabled
        return Qt.ItemFlags(QAbstractTableModel.flags(self,index)|Qt.ItemIsEditable)

    def headerData(self, section, orientation, role = Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return QVariant(self.HEADER[section])
            elif orientation == Qt.Vertical:
                return QVariant(int(section+1))
        return QVariant()

    def data(self, index, role=Qt.DisplayRole):
        #would be nice to colour box different colour if value is only default or something?
        #is it okay to rely on default_model_datatypes having what I want?
        if not index.isValid() or not (0 <= index.row() < self.rowCount()):
            return QVariant()
        #index.row() == 0 based index describing rock type  - we don't order the tables? - yes
        r = self._model.dat()['rock']['types'][index.row()]
        column = index.column()
        if role == Qt.DisplayRole:
            if self.HEADER[column] == 'Rock Index':
                return QVariant(index.row()) #rock index has special calculate 'method'
            elif self.HEADER[column] == 'Frequency':
                cells = rock_cells(r,self._model.dat()) #returns list of all cells with that rocktype
                return QVariant(len(cells))
            elif self.HEADER[column].startswith('Perm'): #deal with permeability separately
                idx = self.WAIWERA_VALS[column]
                try:
                    return QVariant('%.4e' % r['permeability'][idx])
                except KeyError: #no permeability field for that rocktype
                    default = WAIWERA_ROCK_DFALT.get('permeability')
                    if default:
                        return QVariant('%.4e' % default)
                    else: return QVariant()
                except IndexError: #that permeability value does not exist, return empty
                    return QVariant()
                except TypeError: #permeability is a single value for all K1,K2(,K3)
                    return QVariant('%.4e' % r['permeability'] )

            else:
                val = self.WAIWERA_VALS[column]
                try:
                    return QVariant(r[val])
                except KeyError:
                    if val == 'dry_conductivity':
                        #default for dry_conductivity is wet_conductivity value
                        #try to use wet_conductivity value, or use wet_conductivity default
                        default = r.get('wet_conductivity',WAIWERA_ROCK_DFALT.get('wet_conductivity'))
                    else:
                        default = WAIWERA_ROCK_DFALT.get(val)
                    return QVariant(default)
        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if index.isValid() and 0 <= index.row() < self.rowCount():
            r = self._model.dat()['rock']['types'][index.row()]
            column = index.column()
            v, ok = 0.0, True
            if self.WAIWERA_VALS[column] in ['name']:
                new_name = str(value.toString())
                if len(new_name.strip()) == 0:
                    logging.warning("Rocktype name of '%s' is invalid." %new_name)
                    return False
                r['name'] = new_name #set name
                #don't change ok as it is true
            else:
                v,ok = value.toFloat()
                if ok:
                    if self.HEADER[column].startswith('Perm'):
                        try:
                            perm = r['permeability']
                        except KeyError:
                            r['permeability'] = WAIWERA_ROCK_DFALT.get('permeability',10e-13)
                            perm = r['permeability']
                        if isinstance(perm,float):
                            if 'Perm (3)' in self.HEADER:
                                perm = [perm] * 3
                            else:
                                perm = [perm] * 2
                            r['permeability'] = perm

                        r['permeability'][self.WAIWERA_VALS[column]] = v
                    else:
                        v = round(v,5) #TODO: do I like this?
                        if self.WAIWERA_VALS[column] == 'wet_conductivity' and 'dry_conductivity' not in r:
                            #dry conductivity default depends on wet_conductivity, so if editing wet_conductivity, dry conductivity should be set to prevent it from changing
                            r['dry_conductivity'] =  r.get('wet_conductivity',WAIWERA_ROCK_DFALT.get('wet_conductivity'))
                        r[self.WAIWERA_VALS[column]] = v
            if not ok:
                return False
            try:
                msg = 'Rock Type %d, %s edited.' % (index.row() , r['name'])
            except KeyError:
                msg = 'Rock Type %d edited.' % index.row()
            print msg
            self._model.setDatEdited()
            self.emit(SIGNAL("dataChanged (const QModelIndex&,const QModelIndex&)"),index,index)
            return True
        return False

    def insertRows(self, row, count=1, parent = QModelIndex()):
        if not self.ok():
            return False
        self.beginInsertRows(QModelIndex(),row,row+count-1)
        used_names = [x.get('name', '') for x in self._model.dat()['rock']['types']]
        # no need deepcopy here because all those in WAIWERA_ROCK_DFALT dict are
        # simple builtin types
        r = WAIWERA_ROCK_DFALT.copy()
        r['name'] = find_unused_name(used_names, 'NEWROCK0')
        self._model.dat()['rock']['types'].append(r)
        self.endInsertRows()
        self._model.setDatEdited()
        return True

    def removeRows(self, row, count=1, parent = QModelIndex()):
        if not self.ok():
            return False
        idx = row
        rock = self._model.dat()['rock']['types'][idx]

        #if rock type is in use get confirmation from user before continuing
        #TODO: review as this doesn't really belong in a QAbstractTableModel - should maybe be using a delegate?
        frq = len(rock_cells(rock,self._model.dat()))
        if frq > 0:
            name = rock.get('name','')
            if name: ident = '%s, index %i,' % (name,idx)
            else: ident =  str(idx)

            msg = " ".join(['Rock type %s is in use!' % ident, 'Removing it will clear the rock parameters of %i cells.' %frq, 'This action cannot be undone.','Do you wish to continue and delete?'])

            reply = QMessageBox.warning(None,"Trying to delete rock type in use",msg, (QMessageBox.Yes|QMessageBox.Cancel),QMessageBox.Cancel )
            if reply == QMessageBox.Cancel:
                return False

        self.beginRemoveRows(QModelIndex(),row,row+count-1)
        del self._model.dat()['rock']['types'][idx]
        self.endRemoveRows()
        self._model.setDatEdited()
        return True

    def ok(self):
        """return True if the model has rock types specified - waiwera type"""
        #TODO this rock model is not connected to the view unless there is a waiwera dat object - so possibly this function is unneeded
        try:
            rlist = self._model.dat()['rock']['types']
            return True
        except:
            return False

    def _modelReloaded(self):
        self.beginResetModel()
        #hide/show the permeability three column depending on whether geo is 2d or 3d
        if self._model.geo().num_layers <= 2 and 'Perm (3)' in self.HEADER:
            del self.HEADER[6]
            del self.WAIWERA_VALS[6]

        if self._model.geo().num_layers > 2 and 'Perm (3)' not in self.HEADER:
            self.HEADER.insert(6,'Perm (3)')
            self.WAIWERA_VALS.insert(6,2)
        self.endResetModel()

    def connectModel(self,model):
        """ only connects to a model at any given time, all entries re-created with new model """
        if self._model:
            self.disconnect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
            self._modelReloaded()

class Generators(QAbstractTableModel):
    """ A class that keeps track of generators in dat. To be used
    with QTableView. See Qt's Model/View programming. """
    HEADER = ['BLOCK','NAME','LTAB','TYPE','ITAB','GX','EX','HG','FG']
    PYTOUGH_NAMES = ['block','name','ltab','type','itab','gx','ex','hg','fg']
    # 'NSEQ','NADD','NADS', ('nseq','nadd','nads',) not shown
    def __init__(self,model=None):
        super(Generators, self).__init__()
        self._model = model
        self.connectModel(model)

    def rowCount(self, index=QModelIndex()):
        try:
            return self._model.dat().num_generators
        except:
            return 0

    def columnCount(self, index=QModelIndex()):
        # just first line of GENER in data file, total 12 fields
        # take out NSEQ,NADD,NADS
        return 9

    def flags(self, index):
        if not index.isValid(): return Qt.ItemIsEnabled
        return Qt.ItemFlags(QAbstractTableModel.flags(self,index)|Qt.ItemIsEditable)

    def headerData(self, section, orientation, role = Qt.DisplayRole):
        if role == Qt.DisplayRole:
            if orientation == Qt.Horizontal:
                return QVariant(self.HEADER[section])
            elif orientation == Qt.Vertical:
                return QVariant(int(section+1))
        return QVariant()

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid() or not (0 <= index.row() < self.rowCount()):
            return QVariant()
        r = self._model.dat().generatorlist[index.row()]
        column = index.column()
        if role == Qt.DisplayRole:
            return QVariant(getattr(r,self.PYTOUGH_NAMES[column]))
        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        if index.isValid() and 0 <= index.row() < self._model.dat().num_generators:
            r = self._model.dat().generatorlist[index.row()]
            column = index.column()
            v, ok = 0.0, True
            if self.PYTOUGH_NAMES[column] in ['block']:
                v = str(value.toString())
                ok = v in self._model.geo().block_name_list
            elif self.PYTOUGH_NAMES[column] in ['name']:
                v = str(value.toString())
                ok = valid_t2_name(v)
            elif self.PYTOUGH_NAMES[column] in ['type']:
                v = str(value.toString())
            elif self.PYTOUGH_NAMES[column] in ['nseq','nadd','nads','ltab','itab']:
                v,ok = value.toInt()
                v = int(v)
            else:
                v,ok = value.toFloat()
                v = float(v)
            if ok:
                setattr(r,self.PYTOUGH_NAMES[column],v)
            else:
                return False
            print 'Gener %s edited.' % r.name
            self._model.setDatEdited()
            self.emit(SIGNAL("dataChanged (const QModelIndex&,const QModelIndex&)"),index,index)
            return True
        return False

    def insertRows(self, row, count=1, parent = QModelIndex()):
        if not self.ok():
            return False
        self.beginInsertRows(parent,row,row+count-1)

        try:
            default_name = self._model.geo().block_name_list[0]
        except:
            default_name = '     '
        r = t2generator(default_name,default_name)
        self._model.dat().add_generator(r)
        self.endInsertRows()
        self._model.setDatEdited()
        return True

    def removeRows(self, row, count=1, parent = QModelIndex()):
        if not self.ok():
            return False
        if self._model.dat().num_generators == 0:
            return False
        self.beginRemoveRows(parent,row,row+count-1)
        # PyTOUGH delete_generator() would cause issues when two or more
        # generators shares the same name, hence managing list and dict
        # manually, to avoid issues.
        del self._model.dat().generatorlist[row]
        self._model.dat().generator = {(g.block,g.name): g for g in self._model.dat().generatorlist}
        self.endRemoveRows()
        self._model.setDatEdited()
        return True

    def ok(self):
        """return True if there is a model, and there is dat().grid"""
        try:
            glist = self._model.dat().generatorlist
            return True
        except AttributeError:
            return False
        except:
            return False

    def _modelReloaded(self):
        self.beginResetModel()
        self.endResetModel()

    def connectModel(self,model):
        """ only connects to a model at any given time, all entries re-created with new model """
        if self._model:
            self.disconnect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_dat_reloaded"),self._modelReloaded)
            self._modelReloaded()

