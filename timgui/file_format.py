from PyQt4.QtCore import *
from PyQt4.QtGui import *

import unittest

# BLN file is Golden Software's blanking file format, it contains the XY
# coordinates to describe points,polylines and/or polygons. see:
# https://support.goldensoftware.com/hc/en-us/articles/226661208-How-
# can-I-create-a-BLN-file-in-Surfer-

class bln_line(object):
    """ If you are using the file for blanking purposes, a 1 means to blank
    inside the boundary and a 0 means to blank outside the boundary. If you are
    not using the BLN file for blanking, then it doesn't matter if you have a 1
    or a 0.
    """
    def __init__(self, filename=None):
        super(bln_line, self).__init__()
        self.num_points = 0
        self.blanking = 0
        self.line = []

class bln_file(object):
    """ BLN file is Golden Software's blanking file format

    A BLN file contains the XY coordinates to describe points,polylines
    and/or polygons.

    A BLN file consists of a 1-line header followed by a list of XY
    coordinates. The header contains the number of points and then a 1 or a
    0 (the blanking flag number).

    If you are describing a polygon, make sure the first and last set of
    coordinates are exactly the same.Otherwise, it will be a polyline.
    """
    def __init__(self, filename=None):
        super(bln_file, self).__init__()
        self.num_lines = 0
        self.lines = []
        self.filename = None # only keep filename if successfully read
        if filename:
            self.read(filename)

    def read(self, filename):
        import numpy as np
        try:
            with open(filename, 'r') as f:
                header = f.readline()
                while header.strip():
                    n,b = header.split(',')
                    bln = bln_line()
                    bln.num_points = int(n)
                    bln.blanking = int(b)
                    for i in range(bln.num_points):
                        bln.line.append(np.array(eval(f.readline())))
                    self.lines.append(bln)
                    self.num_lines += 1
                    header = f.readline()
                self.filename = filename
        except Exception as e:
            raise e


# Simple interface to GeoJSON-like objects
# TODO: make it more flexible and robust

def load_geojson(filename):
    """ TODO: deal with extra properties etc (with flexibility """
    import json
    from shapely.geometry import shape
    with open(filename, 'r') as f:
        data = json.load(f)
        if data['type'] == 'Feature':
            g = shape(data['geometry'])
            return g
        else:
            print 'GeoJSON: %s not supported yet.' % data['type']

def save_polygon_as_geojson(polygon, filename):
    """ simple PyTOUGH style polygon """
    import json
    from shapely.geometry import Polygon
    from shapely.geometry import mapping
    poly = Polygon(polygon) # might as well used as a check
    with open(filename, 'w') as f:
        json.dump({
                  "type": "Feature",
                  "geometry": mapping(poly),
                  }, f, indent=2, sort_keys=True)

def save_line_as_geojson(polyline, filename):
    """ simple PyTOUGH style polyline """
    import json
    from shapely.geometry import LineString
    from shapely.geometry import mapping
    line = LineString(polyline) # might as well used as a check
    with open(filename, 'w') as f:
        json.dump({
                  "type": "Feature",
                  "geometry": mapping(line),
                  }, f, indent=2, sort_keys=True)

def shape_to_qgraphics(s):
    """ return a list of qgraphicsitems that can be directly added into
    qgraphicsscent """
    if s.geom_type == 'Polygon':
        draw_as_lines = False
        if draw_as_lines:
            items = []
            poly = QPolygonF()
            for p in s.exterior.coords:
                poly.append(QPointF(p[0], p[1]))
            items.append(QGraphicsPolygonItem(poly))
            for g in s.interiors:
                poly = QPolygonF()
                for p in g.coords:
                    poly.append(QPointF(p[0], p[1]))
                items.append(QGraphicsPolygonItem(poly))
            return items
        else:
            poly = QPolygonF()
            for p in s.exterior.coords:
                poly.append(QPointF(p[0], p[1]))
            path_ext = QPainterPath()
            path_ext.addPolygon(poly)
            # cut out interior
            for g in s.interiors:
                poly = QPolygonF()
                for p in g.coords:
                    poly.append(QPointF(p[0], p[1]))
                sub = QPainterPath()
                sub.addPolygon(poly)
                path_ext = path_ext - sub
            it = QGraphicsPathItem(path_ext)
            # semi-transparent blue
            it.setBrush(QBrush(QColor(0, 0, 255, 80), Qt.SolidPattern))
            return [it]
    elif s.geom_type == 'LineString':
        path = QPainterPath(QPointF(s.coords[0][0],s.coords[0][1]))
        for i in range(1,len(s.coords)):
            path.lineTo(QPointF(s.coords[i][0],s.coords[i][1]))
        return [QGraphicsPathItem(path)]
    else:
        print 'not supported yet:', s.geom_type
        return []

class TestGeoJson(unittest.TestCase):
    """ tes simple geojson related routines """
    def setUp(self):
        import numpy as np
        self.poly = [
            np.array([2775147.745446,6284125.370169]),
            np.array([2774486.970810,6283041.699767]),
            np.array([2774301.953913,6281799.443452]),
            np.array([2774592.694752,6280372.170239]),
            np.array([2775412.055300,6278389.846332]),
            np.array([2775993.536980,6277808.364653]),
            np.array([2776865.759499,6277729.071697]),
            np.array([2777685.120047,6277544.054799]),
            np.array([2777975.860886,6276460.384396]),
            np.array([2778002.291872,6273262.235160]),
            np.array([2777922.998915,6272019.978845]),
            np.array([2777685.120047,6271094.894356]),
            np.array([2779720.305924,6270275.533808]),
            np.array([2780645.390414,6270381.257749]),
            np.array([2781914.077714,6270698.429574]),
            np.array([2783156.334029,6270857.015487]),
            np.array([2783922.832606,6270804.153516]),
            np.array([2785693.708630,6271676.376035]),
            np.array([2786486.638192,6272786.477423]),
            np.array([2786539.500163,6274927.387242]),
            np.array([2787913.911405,6277121.159032]),
            np.array([2787543.877609,6278680.587172]),
            np.array([2785376.536805,6278574.863230]),
            np.array([2783473.505854,6278522.001259]),
            np.array([2782072.663627,6278944.897026]),
            np.array([2781121.148152,6280160.722356]),
            np.array([2780988.993224,6281244.392758]),
            np.array([2780856.838297,6281905.167394]),
            np.array([2779641.012968,6283041.699767]),
            np.array([2778980.238332,6283358.871592]),
            np.array([2777711.551032,6284204.663125]),
            np.array([2777262.224280,6284733.282834]),
            np.array([2775967.105994,6284812.575790]),
            np.array([2775359.193329,6284389.680023]),
            ]

    def tearDown(self):
        pass

    def test_simple_polygon(self):
        import os
        save_polygon_as_geojson(self.poly, 'test_poly.json')
        s = load_geojson('test_poly.json')
        print type(s)
        os.remove('test_poly.json')

    def test_simple_line(self):
        import os
        save_line_as_geojson(self.poly[:10], 'test_line.json')
        s = load_geojson('test_line.json')
        print type(s)
        os.remove('test_line.json')

    def test_types(self):
        import json
        from shapely.geometry import mapping
        from shapely.geometry import Point
        from shapely.geometry import LineString
        from shapely.geometry import Polygon

        obj = Point((1.0, 2.0))
        with open('test_1.json', 'w') as f:
            json.dump({
                  "type": "Feature",
                  "geometry": mapping(obj),
                  }, f)
        shape_to_qgraphics(obj)

        obj = LineString([(0, 0), (1, 1)])
        with open('test_2.json', 'w') as f:
            json.dump({
                  "type": "Feature",
                  "geometry": mapping(obj),
                  }, f)
        shape_to_qgraphics(obj)

        obj = Polygon([(0, 0), (1, 1), (1, 0)])
        with open('test_3.json', 'w') as f:
            json.dump({
                  "type": "Feature",
                  "geometry": mapping(obj),
                  }, f)
        shape_to_qgraphics(obj)

        exterior = [(0, 0), (0, 2), (2, 2), (2, 0), (0, 0)]
        interior = [(1, 0), (0.5, 0.5), (1, 1), (1.5, 0.5), (1, 0)][::-1]
        obj = Polygon(exterior, [interior])
        with open('test_4.json', 'w') as f:
            json.dump({
                  "type": "Feature",
                  "geometry": mapping(obj),
                  }, f)
        shape_to_qgraphics(obj)



if __name__ == '__main__':
    unittest.main()
