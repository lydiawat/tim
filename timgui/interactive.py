"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

def getMethods(obj):
    import inspect
    methods = ['  '+i[0]+'()' for i in inspect.getmembers(obj,inspect.ismethod) if i[0][0] <> '_']
    helptxt = '\n'.join(methods)
    print helptxt

def getObjects(start_with=''):
    """ finds names of all local scope objects, including callables """
    import inspect
    scope = inspect.currentframe().f_back.f_locals
    helptxt = '\n'.join(sorted([k for k in scope.keys() if k[:len(start_with)]==start_with]))
    #print helptxt
    for k in sorted(scope.keys()):
        if k[:len(start_with)] == start_with:
            if inspect.ismethod(scope[k]):
                print '  %s()' % k
            elif inspect.isfunction(scope[k]):
                print '  %s()' % k
            elif inspect.isfunction(scope[k]):
                print '  %s()' % k
            elif inspect.isclass(scope[k]):
                print '  Class %s' % k
            elif inspect.ismodule(scope[k]):
                print '  Module %s' % k
            elif inspect.isbuiltin(scope[k]):
                print '  <built-in function> %s()' % k
                # seems Qt's function is in this group
            else:
                # normal object
                print '  %s' % k

class Console(QThread):
    def __init__(self, scope, parent=None):
        super(Console, self).__init__(parent)
        from code import InteractiveConsole
        self.con = InteractiveConsole(scope)

    def run(self):
        help_text = '\n'.join([
            "",
            "  Run command by using:",
            "    >>> run_command('about')",
            "",
            "  List commands by using:",
            "    >>> list_commands()",
            "",
            "  Top level objects that are useful: ",
            "    model, scene_manager, points_board, ... etc.",
            "",
            "  PyTOUGH objects can be accessed by .geo(), .dat(), .lst(), e.g.:",
            "    >>> print model.geo().columnlist[0].area",
            "    >>> print model.dat().grid.rocktypelist[-1].name",
            "    >>> print model.lst().element.column_name",
            ])
        self.con.interact(help_text)

    def __del__(self):
        self.quit()
        super(Console, self).__del__()

