"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This file contains the implementation of two QWidgets in a horizontal layout.
The purpose of this layout is for the QWidget pairs to be contained in a single
grid of a parent QGridLayout.

The implementations so far:
 -QBtnPair: left and right pair of QPushButtons
 -QEditPair: QLineEdit paired with a QPushButton("edit")

TODO:

 -Reimplement the various sizing policies (required .setMinimumSize() and
  setSizePolicy)  to split the sizing policy between both QWidgets.

 -OPTIONAL implement a general pair_layout(QWidget,QWidget) that takes two
  QWidgets and  gives them the pair layout.

Author: Stephen Lee Orenia (sorenia@bitbucket.org)
"""

import sys

from PyQt4.QtCore import *
from PyQt4.QtGui import *

class QBtnPair(QWidget):
    """
    The QBtnpair implements a pair of QPushButtons in a horizontal layout.

    Initialised by supplying strings for the text of the left and right buttons.
	    btns = QBtnPair(left = "Save", right = "Cancel") OR
        btns = QBtnPair("Save","Cancel")
     There is also functions for disabling the buttons, clickable(who) and
     not_clickable(who) The SIGNALs returned are "left" or "right" depending on
     which button was clicked
	    connect(btns, "left", updateLeft)
	    connect(btns, "right", updateRight)
    """
    def __init__(self, left = "left", right = "right", parent=None):
        super(QBtnPair, self).__init__(parent)

        # Create left and right buttons - text as input
        self.lftButt = QPushButton(left)
        self.rgtButt = QPushButton(right)

        # The setDefault is required to prevent the button to trigger when
        # return/enter is pressed
        self.lftButt.setAutoDefault(False)
        self.lftButt.setDefault(False)
        self.rgtButt.setAutoDefault(False)
        self.rgtButt.setDefault(False)

        # Adding buttons to horizontal layout
        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setSpacing(0)
        self.layout.addWidget(self.lftButt)
        self.layout.addWidget(self.rgtButt)
        self.setLayout(self.layout)

        # Creating signal for parent to slot from
        self.lftButtonClicked = lambda who="left": self.anyButton(who)
        self.rgtButtonClicked = lambda who="right": self.anyButton(who)
        self.connect(self.lftButt, SIGNAL("clicked()"), self.lftButtonClicked)
        self.connect(self.rgtButt, SIGNAL("clicked()"), self.rgtButtonClicked)

    def anyButton(self, who):
        # lambda signal that signifies which button has been clicked
        self.emit(SIGNAL(who))

    def not_clickable(self, who):
        # Function to disable specific button
        if who == "left": self.lftButt.setEnabled(False)
        elif who == "right": self.rgtButt.setEnabled(False)

    def clickable(self, who):
        # Function to enable specific button
        if who == "left": self.lftButt.setEnabled(True)
        elif who == "right": self.rgtButt.setEnabled(True)


class QEditPair(QWidget):
    """
    QEditPair implements a QLineEdit with QPushButton('Edit'). The QEditPair is
    a unique implementation that immediately disables the QLineEdit and only
    enables editing once the button is clicked.

	    edits = QEditPair()

    There are two public functions :

        .setText(text) implementation of QLineEdit.setText()
        .getText() returns the text in the QLineEdit, i.e. self.editBox.text()

    The SIGNALs emitted from this QWidget are 'EDITED' and 'RETURNED' that replicate the
    'editingFinished()' and 'returnPressed()'' signals of the QLineEdit.
    """
    def __init__(self, parent=None):
        super(QEditPair, self).__init__(parent)
        self.editBox = QLineEdit()
        self.editButt = QPushButton("Edit")

        #The setDefault is required to prevent the button to trigger when return/enter is pressed
        self.editButt.setAutoDefault(False)
        self.editButt.setDefault(False)

        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(0,0,0,0)
        self.layout.setSpacing(0)
        self.layout.addWidget(self.editBox)
        self.layout.addWidget(self.editButt)
        self.setLayout(self.layout)

        self.connect(self.editButt, SIGNAL("clicked()"), self.editself)
        # Will send double signal, returnPressed and editingFinished
        self.connect(self.editBox, SIGNAL("returnPressed()"), self.updateReturn)
        # pyQt Bug: Signal sends twice
        self.connect(self.editBox, SIGNAL("editingFinished()"), self.updateEdit)
        self.disableEdit()

    def setText(self, text):
    	self.editBox.setText(text)

    def getText(self):
        return self.editBox.text()

    def updateEdit(self):
        self.disableEdit()
        self.emit(SIGNAL("EDITED"))

    def updateReturn(self):
        self.emit(SIGNAL("RETURNED"))

    def disableEdit(self):
    	self.editBox.setEnabled(False)
    	self.editBox.setFrame(False)

    def editself(self):
    	self.editBox.setEnabled(True)
    	self.editBox.setFrame(True)
    	self.editBox.selectAll()
    	self.editBox.setFocus()


class test_buttons(QDialog):
    def __init__(self,parent = None):
        super(test_buttons, self).__init__(parent)

        self.buttons = QBtnPair()
        layout = QVBoxLayout()
        layout.addWidget(self.buttons)

        self.who_click = QLabel()
        layout.addWidget(self.who_click)
        self.setLayout(layout)
        self.setWindowTitle("Testing the left and right buttons")

        self.edits = QEditPair()
        layout.addWidget(self.edits)

        self.updateL = lambda who="left": self.update(who)
        self.updateR = lambda who="right": self.update(who)
        self.connect(self.buttons, SIGNAL("left"), self.updateL)
        self.connect(self.buttons, SIGNAL("right"), self.updateR)

    def update(self,who):
        self.who_click.setText(who)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    buttonWindow = test_buttons()
    buttonWindow.show()
    app.exec_()
