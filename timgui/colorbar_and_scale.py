"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *
from units import Unit
import numpy as np
import settings
import logging
import json

DEFAULT_COLORS = {
    'mulgraph': [
        '#0000ff',
        '#0047ff',
        '#0058ff',
        '#0090ff',
        '#009fff',
        '#00e7ff',
        '#1fffdf',
        '#50ffb0',
        '#7fff7f',
        '#b0ff50',
        '#dfff1f',
        '#fff800',
        '#ffdf00',
        '#ffc700',
        '#ff9000',
        '#ff4700',
        '#ff0000',
    ]
}['mulgraph']

class myColorBar(QWidget):
    """
        once contructed, can call .color(v) to get QColor obj for any QBrush

        it should have n values and n colors.  In the special case of
        numeric values with discrete mode, the last color will not be used.

        Note, this is actually very similar to class matplotlib.cm.ScalarMappable
        (cm (colormap)), maybe one day i will change this back to just wrap around
        matplotlib's cm.

    NOTE: ??? why does this class inherit QWidget?
    """
    def __init__(self, *args, **kwargs):
        super(myColorBar, self).__init__(*args, **kwargs)
        # allow public access
        self._discreteColorMode = False
        self._logScaleMode = False
        self.noneColorUnder = False
        self._regExpMode = False
        self._textMode = False
        # TODO: additional properties, not sure if shud keep or not.
        self.label_formatter = None
        self._unit = ''

        # # access by set/get
        # self._colorScale = [
        #     QColor('#0000ff'),
        #     QColor('#87ceeb'),
        #     QColor('#90ee90'),
        #     QColor('#ffff00'),
        #     QColor('#ffa500'),
        #     QColor('#ff4500'),
        #     QColor('#ff0000'),
        #     ]
        # self._valueScale = [
        #     0.0,
        #     1./6.,
        #     2./6.,
        #     3./6.,
        #     4./6.,
        #     5./6.,
        #     1.0,
        #     ]
        # # defaults

        # access by set/get
        self._colorScale = [QColor(c) for c in DEFAULT_COLORS]
        import numpy
        self._valueScale = list(numpy.linspace(0.0, 1.0, len(self._colorScale)))
        # defaults

        # strictly private
        # !!! should be reset to None everytime _valueScale or _colorScale changes
        self._rgbColorValues = None # for composing QColor, worked out on demand
        self._regExpObjs = None

    def __repr__(self):
        s = []
        for v,c in zip(self._valueScale, self._colorScale):
            if isinstance(v,str):
                s.append(' (str)   %s: %s' % (v, c.name()))
            else:
                s.append(' (float) %f: %s' % (v, c.name()))
        return '\n'.join(s)

    def getUnit(self):
        return self._unit
    def setUnit(self, newu):
        if not self.textMode:
            try:
                v = np.array(self.valueScale) * Unit(self._unit)
                v.ito(newu)
                self.valueScale = list(v.magnitude)
            except Exception as e:
                # TODO: too vague, need more detailed handling, currently to
                # allow initialisation of unit.
                logging.warning('ColorBar.setUnit() failed:' + str(e))
            self._unit = newu
    unit = property(getUnit, setUnit)

    def getLogScaleMode(self):
        return self._logScaleMode
    def setLogScaleMode(self,value):
        self._logScaleMode = value
        if self.textMode and value:
            self.textMode = False
    logScaleMode = property(getLogScaleMode, setLogScaleMode)

    def getRegExpMode(self):
        return self._regExpMode
    def setRegExpMode(self,value):
        self._regExpMode = value
        if value:
            self.textMode = True
    regExpMode = property(getRegExpMode, setRegExpMode)

    def getDiscreteColorMode(self):
        return self._discreteColorMode
    def setDiscreteColorMode(self,value):
        if len(self.valueScale) < 3:
            # force none discrete so matplotlib will not die
            self._discreteColorMode = False
            return
        self._discreteColorMode = value
    discreteColorMode = property(getDiscreteColorMode, setDiscreteColorMode)

    def getTextMode(self):
        return self._textMode
    def setTextMode(self,value):
        self._textMode = value
        if self._textMode:
            self._valueScale = [str(v) for v in self._valueScale]
            self.logScaleMode = False
            self.discreteColorMode = True
            if self.label_formatter is None:
                self.label_formatter = '%s'
        else:
            self._valueScale = [float(v) for v in self._valueScale]
            self.regExpMode = False
    textMode = property(getTextMode, setTextMode)

    def getValueScale(self):
        return self._valueScale
    def setValueScale(self,values):
        """ a few precautionary measure to ensure smooth operation if
        _valueScale is of strings (most defaults won't work well) """
        self._valueScale = values
        import numpy as np
        for v in self._valueScale:
            if isinstance(v,(str,np.string_,QString)):
                self.textMode = True
                # reset reg exp objs
                self._regExpObjs = None
        if len(self._valueScale) <> len(self._colorScale):
            self.resetColor(self._colorScale)
    valueScale = property(getValueScale, setValueScale)

    def getColorScale(self):
        return self._colorScale
    def setColorScale(self,colors):
        self._colorScale = [QColor(c) for c in colors]
        # reset rgb values
        self._rgbColorValues = None
        if len(self._valueScale) <> len(self._colorScale):
            self.resetColor(self._colorScale)
    colorScale = property(getColorScale, setColorScale)

    def removeValueColor(self, i):
        if 0 <= i < len(self._valueScale):
            self._valueScale = self._valueScale[:i] + self._valueScale[i+1:]
            self._colorScale = self._colorScale[:i] + self._colorScale[i+1:]
            # reset rgb values
            self._rgbColorValues = None
            self._regExpObjs = None

    def insertValueColor(self, i, cv=None):
        import numpy as np
        def avg_color(c1, c2):
            old_rgb_values = zip(*[list(QColor(c).getRgb()) for c in [c1, c2]])
            return QColor(*[sum(c)/len(c) for c in old_rgb_values])
        n = len(self._valueScale)
        if 0 <= i < n:
            if cv is None:
                if i == 0:
                    c, v = self._colorScale[i], self._valueScale[i]
                else:
                    if isinstance(self._valueScale[i-1], (str,np.string_,QString)):
                        v = self._valueScale[i]
                        self._regExpObjs = None
                    else:
                        v = (self._valueScale[i-1] + self._valueScale[i]) / 2
                    c = avg_color(self._colorScale[i-1], self._colorScale[i])
            else:
                c, v = cv
            self._valueScale = self._valueScale[:i] + [v] + self._valueScale[i:]
            self._colorScale = self._colorScale[:i] + [c] + self._colorScale[i:]
            # reset rgb values
            self._rgbColorValues = None

    def resetColor(self,colors=DEFAULT_COLORS):
        """ Use this to create color scale to match the size of value scale, by
        using a specified (or default) color scheme.  This routine does not like
        length of colors/values to be 1. """
        if len(colors) == 0:
            colors = DEFAULT_COLORS
        elif len(colors) == 1:
            colors += DEFAULT_COLORS[:1]

        if len(self._valueScale) == 1:
            self._colorScale = [QColor(c) for c in colors[:1]]
        else:
            # do it normally
            old_rgb_values = zip(*[list(QColor(c).getRgb()) for c in colors])
            old_color_value_scale = [0.0+(1.0/float(len(colors)-1))*i for i in range(len(colors))]
            new_value_scale = [0.0+(1.0/float(len(self._valueScale)-1))*i for i in range(len(self._valueScale))]

            from numpy import interp
            new_colors = []
            for val in new_value_scale:
                cv = [int(interp(val,old_color_value_scale,v)) for v in old_rgb_values]
                new_colors.append(QColor(cv[0],cv[1],cv[2],cv[3]))
            self.colorScale = new_colors

    def _getIndex(self, val):
        # for discrete color mode assume self._valueScale is in increasing order
        if val <= self._valueScale[0]:
            if self.noneColorUnder:
                return None
            else:
                return 0
        for i in range(len(self._valueScale)-1):
            if self._valueScale[i] < val <= self._valueScale[i+1]: return i
        if val > self._valueScale[-1]: return len(self._valueScale)-2

    def _getIndexRE(self, val):
        """ will return the index of the first match, if not found, return None """
        import re
        if self._regExpObjs is None:
            self._regExpObjs = [re.compile(s) for s in self._valueScale]
        for i,r in enumerate(self._regExpObjs):
            if r.match(val) is not None:
                return i
        return None

    def setBounds(self, vmin, vmax, useraw=False):
        """ automatically creates self._valueScale by specifying only two ends

        NOTE when .logScaleMode is True, useraw contorls how vmin/vmax is
        specified here. eg. permeability bounds at 1mD,1000mD, use:

        (from model) setBounds(1.0e-15, 1.0e-12, useraw=True)
        (from user)  setBounds(-15., -12.)

        (if set valueScale manually, should be something like [-15., -14., -13., -12.])
        """
        import numpy as np
        from math import log10
        for v in (vmin,vmax):
            if isinstance(v, (str, np.string_, QString)):
                print "ColorBar cannot setBound() with strings and alike."
                return
        if useraw and self.logScaleMode:
            vmin, vmax = log10(vmin), log10(vmax)
        if vmin >= vmax:
            # avoid bad vmin/vmax, which causes a bunch of errors later on
            vmax = vmin + max(abs(vmin) * 0.01,0.01)
        # note if discrete color mode, the last color will not be used
        n = len(self._colorScale)
        self.valueScale = list(np.linspace(vmin, vmax, num=n))

    def color(self, value):
        """ this function returns a QColor object to be used in eg. QBrush() """
        # transparent if value is not a number
        import numpy as np
        if value is None:
            return QColor('transparent')
        if value.dtype.name.startswith('float') and np.isnan(value):
            return QColor('transparent')
        # normal values
        from math import log10
        if self.logScaleMode:
            val = log10(value)
        else:
            val = value
        if self.discreteColorMode or self.textMode:
            if self.regExpMode:
                i = self._getIndexRE(val)
            else:
                i = self._getIndex(val)
            if i is None:
                return QColor('transparent')
            else:
                return self._colorScale[i]
        else:
            if self._rgbColorValues is None:
                self._rgbColorValues = zip(*[list(c.getRgb()) for c in self._colorScale])
            from numpy import interp
            if self.noneColorUnder and val <= self._valueScale[0]:
                return QColor('transparent')
            else:
                cv = [int(interp(val,self._valueScale,v)) for v in self._rgbColorValues]
                return QColor(cv[0],cv[1],cv[2],cv[3])

class ColorBarManager(QObject):
    """ keeps a dictionary of color bars available for use, will be
    responsible of reloading user preferences upon application launch """
    def __init__(self):
        super(ColorBarManager, self).__init__()
        self._color_bars = {}

        self.loadSettings()

    def __getitem__(self, value_name):
        """ like a dictionary, value_name is the key, will return a myColorBar()
        object if not found """
        if value_name not in self._color_bars:
            self._color_bars[value_name] = myColorBar()
        return self._color_bars[value_name]

    def __setitem__(self, value_name, cb):
        """ if cb is not a myColorBar() object, exception will be raised """
        if type(cb) is not type(myColorBar()):
            raise Exception('%s is not a myColorBar() object' % str(cb))
        self._color_bars[value_name] = cb
        self.emit(SIGNAL("color_bar_updated"),value_name)

    def __contains__(self, value_name):
        """ used for in or not in """
        return value_name in self._color_bars

    def saveSettings(self):

        def serialiseColorBar(cb):
            scb = {}
            scb['values'] = cb.valueScale
            scb['colors'] = [str(c.name()) for c in cb.colorScale]
            scb['discrete'] = cb.discreteColorMode
            scb['regular_expression'] = cb.regExpMode
            scb['log'] = cb.logScaleMode
            if isinstance(cb.label_formatter, str):
                scb['label_format_string'] = cb.label_formatter
            elif isinstance(cb.label_formatter,(list,tuple)):
                scb['label_list'] = cb.label_formatter
            elif callable(cb.label_formatter):
                scb['label_formatter'] = cb.label_formatter.__name__
            else:
                pass
            return scb

        # always save to active setting path, hence no fallback
        fname = settings.getSettingsPath('colorbars.json', fallback=False)
        import json
        f = open(fname, 'w')
        json.dump(self._color_bars, f, indent=4, default=serialiseColorBar)
        f.close()

    def loadSettings(self):
        """ this will load the colorbar definitions from an external file """

        def objectHookColorBar(single_dict):

            # TODO: additional formatting things might not work so well with
            # saving/reloading, need to make this external.  But I will probably
            # have to have a full unit system anyway.
            def format_perm(v):
                return '{0!s}'.format(v)
            def format_pres(v):
                return '{0!s}'.format(v)
            def format_temp(v):
                return '{0!s}'.format(v)

            if 'colors' in single_dict:
                # this is upper level object, return as normal dict, instead of obj
                return single_dict
            # otherwise it is one of the color, decode
            allcbs = {}
            for k, d in single_dict.iteritems():
                cb = myColorBar()
                # somehow matplotlib does not like unicode here in a
                if isinstance(d['values'][0],unicode):
                    cb.valueScale = [str(x) for x in d['values']]
                else:
                    cb.valueScale = d['values']
                cb.colorScale = [QColor(c) for c in d['colors']]
                if 'discrete' in d:
                    cb.discreteColorMode = d['discrete']
                if 'regular_expression' in d:
                    cb.regExpMode = d['regular_expression']
                if 'log' in d:
                    cb.logScaleMode = d['log']
                if 'label_format_string' in d:
                    cb.label_formatter = d['label_format_string']
                if 'label_list' in d:
                    cb.label_formatter = d['label_list']
                if 'label_formatter' in d:
                    # TODO: improve this, see above
                    cb.label_formatter = locals()[ d['label_formatter'] ]
                allcbs[k] = cb
            return allcbs

        self._color_bars = settings.safe_loading_json_file('colorbars.json', {},
                                                  object_hook=objectHookColorBar)

    def _setupColorBars(color_bars):
        # TODO: this is not used anymore, now loads from file. to be removed with some tests!
        raise Exception("----- YOU SHOULDN'T HAVE SEEN THIS!!! -----")

class Scale(object):
    """ This is used by ArrowScaleManager
    """
    def __init__(self,val):
        self.val = val
        self._unit = ''

    def getUnit(self):
        return self._unit
    def setUnit(self, newu):
        try:
            factor = Unit(self._unit).to(newu).magnitude
            self.val = self.val / factor
        except Exception as e:
            # TODO: too vague, need more detailed handling, currently to
            # allow initialisation of unit.
            logging.warning('Scale .setUnit() failed:' + str(e))
        self._unit = newu
    unit = property(getUnit, setUnit)

    def __repr__(self):
        return '<Scale: %f (%s)>' % (self.val, self.unit)

class ArrowScaleManager(QObject):
    """ keeps a dictionary of arrow scales available for use, will be
    responsible of reloading user preferences upon application launch """
    def __init__(self):
        super(ArrowScaleManager, self).__init__()
        self._scales = {}

        self.loadSettings()

    def __getitem__(self,value_name):
        """ like a dictionary, value_name is the key, will return a Scale()
        object if not found, will return the default Scale(1.0) and keeps the
        value in dict """
        if value_name not in self._scales:
            self._scales[value_name] = Scale(1.0)
        return self._scales[value_name]

    def __setitem__(self,value_name,scale):
        """ if scale is a Scale object, it will be used, otherwise assumed to be
        a simple value """
        if type(scale) is not type(Scale(0.0)): scale = Scale(scale)
        if value_name in self._scales:
            #if self._scales[value_name] <> scale:
            self._scales[value_name] = scale
            self.emit(SIGNAL("arrow_scale_updated"),value_name)
        else:
            self._scales[value_name] = scale
            self.emit(SIGNAL("arrow_scale_updated"),value_name)
    """
    def get(self,value_name):
        return self.__getitem__(value_name).val
    def set(self,value_name,val):
        return self.__setitem__(value_name,val)
    """

    def saveSettings(self):

        all_scales = {}
        for k,v in self._scales.iteritems():
            all_scales[k] = self._scales[k].val

        # always save to active setting path, hence no fallback
        fname = settings.getSettingsPath('arrow_scales.json', fallback=False)
        import json
        f = open(fname, 'w')
        json.dump(all_scales, f, indent=4)
        f.close()

    def loadSettings(self):
        """ this will load the arrow scale definitions from an external file """

        all_scales = settings.safe_loading_json_file('arrow_scales.json', {})

        self._scales = {}
        for k,v in all_scales.iteritems():
            self._scales[k] = Scale(v)

    def _setupArrowScales(self):
        # TODO: clean this up
        raise Exception("Should remove this! see TODOs")


# these are not here to stay:



