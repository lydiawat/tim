"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

import logging
from units import guessUnits

import numpy as np

class BaseDataSeries(QObject):
    """BaseDataSeries keeps information of a data series, which is a single line
    in a plot.  The inherting class must define class variable KEYS, a list of
    property names, and a method getXYData() for obtaining x and y values. """
    KEYS = []
    def __init__(self, **kwargs):
        """ All keywords and their respective values were set as properties of
        the object.  The values of .xs and .ys are also initialised. """
        super(BaseDataSeries, self).__init__()
        # initialise cached x, y values
        self.xs, self.ys = [], []
        self.xunit, self.yunit = '', ''
        self.xlabel, self.ylabel = '', ''  # series can "suggest" labels to plot
        # may need model to obtain x, y or for validation
        self._model = None
        from copy import deepcopy
        not_specified = deepcopy(self.KEYS)
        for key, value in kwargs.items():
            if key in self.KEYS:
                setattr(self, key, value)
                not_specified.remove(key)
            elif key in ['xunit', 'yunit']:
                # additional settable properties
                setattr(self, key, value)
            else:
                logging.error("Type %s does not accept property '%s' in __init__(), continue by skipping kwargs."
                    % (str(type(self)),key))
        for key in not_specified:
            setattr(self, key, None)

    def __repr__(self):
        """it is not compulsory, but it is recommended to reimplement this
        method, for more meaningful repr string."""
        keynames = self.KEYS
        values = [str(getattr(self,k)) for k in self.KEYS]
        return str(type(self)) + ' : ' + ("(%s) = (%s)"
            % (','.join(keynames), ','.join(values)))

    def reset(self):
        """clear xy data and emit signal"""
        self.xs, self.ys = [], []
        self.emit(SIGNAL("updated"))

    def getXYData(self):
        """ This method should be implemented by subclasses to return x and y
        lists/vectors.  The cached self.xs and self.ys are refreshed at the same
        time. """
        raise NotImplementedError("Please Implement this method")

    def createEditor(self):
        """Returns a QWidget that allows user to edit the contents of the series
        itself.  Note that it is important to realise that the series object
        (self) does not (cannot) take control of the widget, it's caller's
        responsibility to destroy the widget after use."""
        raise NotImplementedError("Please Implement this method")

    def connectEditor(self,editor):
        """connects the series to an editor widget (created by using
        createEditor(), if editor is None, then the series will be disconnected
        to the editor."""
        raise NotImplementedError("Please Implement this method")

    def disconnectEditor(self,editor):
        """disconnects the series to an editor widget (created by using
        createEditor(), if editor is None, then the series will be disconnected
        to the editor."""
        raise NotImplementedError("Please Implement this method")

    def connectModel(self,model):
        if self._model:
            self.disconnect(self._model,SIGNAL("model_raw_data_changed"),self._modelUpdated)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_raw_data_changed"),self._modelUpdated)

    def _modelUpdated(self,(mdtype,valName,change)):
        """Clear cached x y data if model objects modified.  If series x,y is
        cleared, a singal of "series_updated" will be emitted."""
        from t2model import Model
        if mdtype == Model.BLOCK and valName == self.variable:
            self.reset()

class AlongAxisSeries(BaseDataSeries):
    """
    """
    KEYS = ['axis','position','variable']
    def getXYData(self):
        if (len(self.xs) + len(self.ys)) == 0 and self._model:
            if all([self.axis, self.variable]):
                self.xlabel, self.ylabel = 'Along', self.variable
                try:
                    geo = self._model.geo()
                    gmin = np.append(geo.bounds[0], [geo.layerlist[-1].bottom])
                    gmax = np.append(geo.bounds[1], [geo.layerlist[0].top])
                    print gmin, gmax, self.axis
                    for i,a in enumerate(['x', 'y', 'z']):
                        if not self.axis == a:
                            gmin[i] = (gmin[i] + gmax[i]) / 2.0
                            gmax[i] = gmin[i]
                    print gmin, gmax
                    if not hasattr(geo, 'quadtree'):
                        geo.quadtree = geo.column_quadtree()
                    # done using PyTOUGH's mulgrid.well_values() method
                    from t2model import Model
                    self.xs, self.ys = geo.line_values(
                        gmin,
                        gmax,
                        self._model.getData(Model.BLOCK,self.variable),
                        divisions=10000,
                        coordinate=False,
                        qtree=geo.quadtree)
                    self.xunit = 'meter'
                    self.yunit = self._model.getDataUnit(Model.BLOCK,self.variable)

                except Exception as ex:
                    logging.warning('Failed to get %s data along %s-axis' % (self.variable, self.axis))
                    logging.warning('  Exception: %s' % str(ex))
        # these can be empty,
        return self.xs, self.ys

    def _setAxis(self,a):
        a = str(a)
        if a <> self.axis:
            self.axis = str(a)
            self.reset()

    def _setVariable(self,v):
        v = str(v)
        if v <> self.variable:
            self.variable = v
            self.reset()

    def createEditor(self):
        select = QComboBox()
        select.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        label = QLabel('Select an Axis:')
        label.setBuddy(select)
        variable_combobox = QComboBox()
        variable_combobox.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        label_variable = QLabel('Select Variable:')
        label_variable.setBuddy(variable_combobox)

        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(label)
        layout.addWidget(select)
        layout.addStretch()
        layout.addWidget(label_variable)
        layout.addWidget(variable_combobox)

        widget = QWidget()
        widget.setLayout(layout)
        # put these widgets as properties of the object, so easier to access
        widget.select = select
        widget.variable_combobox = variable_combobox
        return widget

    def updateEditor(self,editor):
        # well name
        editor.select.clear()
        if self._model:
            geo = self._model.geo()
            if geo:
                cur_i = -1
                for i,a in enumerate(['x','y','z']):
                    editor.select.addItem(a)
                    if a == self.axis:
                        cur_i = i
                editor.select.setCurrentIndex(cur_i)

        # variable name
        editor.variable_combobox.clear()
        if self._model:
            cur_i = -1
            for i,v in enumerate(self._model.variableList(self._model.BLOCK)):
                editor.variable_combobox.addItem(v)
                if v == self.variable:
                    cur_i = i
            editor.variable_combobox.setCurrentIndex(cur_i)

    def connectEditor(self,editor):
        QObject.connect(editor.select,
            SIGNAL('currentIndexChanged (const QString&)'), self._setAxis)
        QObject.connect(editor.variable_combobox,
            SIGNAL('currentIndexChanged (const QString&)'), self._setVariable)

    def disconnectEditor(self,editor):
        QObject.disconnect(editor.select,
            SIGNAL('currentIndexChanged (const QString&)'), self._setAxis)
        QObject.disconnect(editor.variable_combobox,
            SIGNAL('currentIndexChanged (const QString&)'), self._setVariable)

    def __repr__(self):
        return '<Axis: %s, %s>' % (str(self.axis),str(self.variable))

class RadialSeries(BaseDataSeries):
    """
    """
    KEYS = ['layer','variable']
    def getXYData(self):
        if (len(self.xs) + len(self.ys)) == 0 and self._model:
            if all([self.layer, self.variable]):
                self.xlabel, self.ylabel = 'Radial Distance', self.variable
                try:
                    geo = self._model.geo()
                    bs = [geo.block_name(self.layer, c.name) for c in geo.columnlist]
                    indices = [geo.block_name_index[b] for b in bs]
                    self.xs = [float(c.centre[0]) for c in geo.columnlist]
                    from t2model import Model
                    self.ys = self._model.getData(Model.BLOCK, self.variable, indices)
                    self.xunit = 'meter'
                    self.yunit = self._model.getDataUnit(Model.BLOCK,self.variable)
                except Exception as ex:
                    logging.warning('Failed to get %s data along %s-layer' % (self.variable, self.layer))
                    logging.debug('  Exception: %s' % str(ex))
        # these can be empty,
        return self.xs, self.ys

    def _setLayer(self,a):
        a = str(a)
        if a <> self.layer:
            self.layer = str(a)
            self.reset()

    def _setVariable(self,v):
        v = str(v)
        if v <> self.variable:
            self.variable = v
            self.reset()

    def createEditor(self):
        select = QComboBox()
        select.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        label = QLabel('Select an layer:')
        label.setBuddy(select)
        variable_combobox = QComboBox()
        variable_combobox.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        label_variable = QLabel('Select Variable:')
        label_variable.setBuddy(variable_combobox)

        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(label)
        layout.addWidget(select)
        layout.addStretch()
        layout.addWidget(label_variable)
        layout.addWidget(variable_combobox)

        widget = QWidget()
        widget.setLayout(layout)
        # put these widgets as properties of the object, so easier to access
        widget.select = select
        widget.variable_combobox = variable_combobox
        return widget

    def updateEditor(self,editor):
        editor.select.clear()
        if self._model:
            geo = self._model.geo()
            if geo:
                cur_i = -1
                for i,a in enumerate(lay.name for lay in geo.layerlist[1:]):
                    editor.select.addItem(a)
                    if a == self.layer:
                        cur_i = i
                editor.select.setCurrentIndex(cur_i)

        # variable name
        editor.variable_combobox.clear()
        if self._model:
            cur_i = -1
            for i,v in enumerate(self._model.variableList(self._model.BLOCK)):
                editor.variable_combobox.addItem(v)
                if v == self.variable:
                    cur_i = i
            editor.variable_combobox.setCurrentIndex(cur_i)

    def connectEditor(self,editor):
        QObject.connect(editor.select,
            SIGNAL('currentIndexChanged (const QString&)'), self._setLayer)
        QObject.connect(editor.variable_combobox,
            SIGNAL('currentIndexChanged (const QString&)'), self._setVariable)

    def disconnectEditor(self,editor):
        QObject.disconnect(editor.select,
            SIGNAL('currentIndexChanged (const QString&)'), self._setLayer)
        QObject.disconnect(editor.variable_combobox,
            SIGNAL('currentIndexChanged (const QString&)'), self._setVariable)

    def __repr__(self):
        return '<Radial: %s, %s>' % (str(self.layer),str(self.variable))

class DownholeWellSeries(BaseDataSeries):
    """Data series for downhole plot, use a well track from geometry file.

    It is responsible for keeping properties of a data series for plotting.  It
    also needs to know how to get the data out of model or external field data
    files.
    """
    KEYS = ['well','time','variable']
    def getXYData(self):
        """extract data from the model object, use getData() method, with
        additional information from .geo() .dat() .lst() objects."""
        # only extract when no cached and model available
        if (len(self.xs) + len(self.ys)) == 0 and self._model:
            if all([self.well, self.variable]):
                self.xlabel, self.ylabel = self.variable, 'Elevation'
                try:
                    geo = self._model.geo()
                    # done using PyTOUGH's mulgrid.well_values() method
                    from t2model import Model
                    self.ys, self.xs = geo.well_values(self.well,
                        self._model.getData(Model.BLOCK,self.variable), elevation=True, extend=True)
                    self.xunit = self._model.getDataUnit(Model.BLOCK,self.variable)
                    self.yunit = 'meter'

                    # # customised well_values may allow well block indices to be cached
                    # blocks = geo.well_blocks(self.well, elevation=True)
                    # indices = []
                    # self.ys = []
                    # for b in blocks:
                    #     indices.append(geo.block_name_index[b])
                    #     lay,col = geo.layer[geo.layer_name(b)],geo.column[geo.column_name(b)]
                    #     self.ys.append(geo.block_centre(lay,col)[2])
                    # self.xs = model.getData(Model.BLOCK,self.variable,indices)
                except Exception as ex:
                    logging.warning('Failed to get model data: %s for Well: %s' % (self.variable, self.well))
                    logging.debug('  Exception: %s' % str(ex))
        # these can be empty,
        return self.xs, self.ys

    def _setWell(self,w):
        w = str(w)
        if w <> self.well:
            self.well = str(w)
            self.reset()

    def _setVariable(self,v):
        v = str(v)
        if v <> self.variable:
            self.variable = v
            self.reset()

    def createEditor(self):
        well_select = QComboBox()
        well_select.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        label_well = QLabel('Select a Well:')
        label_well.setBuddy(well_select)
        variable_combobox = QComboBox()
        variable_combobox.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        label_variable = QLabel('Select Variable:')
        label_variable.setBuddy(variable_combobox)

        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(label_well)
        layout.addWidget(well_select)
        layout.addStretch()
        layout.addWidget(label_variable)
        layout.addWidget(variable_combobox)

        widget = QWidget()
        widget.setLayout(layout)
        # put these widgets as properties of the object, so easier to access
        widget.well_select = well_select
        widget.variable_combobox = variable_combobox
        return widget

    def updateEditor(self,editor):
        # well name
        editor.well_select.clear()
        if self._model:
            geo = self._model.geo()
            if geo:
                cur_i = -1
                for i,w in enumerate(geo.welllist):
                    editor.well_select.addItem(w.name)
                    if w.name == self.well:
                        cur_i = i
                editor.well_select.setCurrentIndex(cur_i)

        # variable name
        editor.variable_combobox.clear()
        if self._model:
            cur_i = -1
            for i,v in enumerate(self._model.variableList(self._model.BLOCK)):
                editor.variable_combobox.addItem(v)
                if v == self.variable:
                    cur_i = i
            editor.variable_combobox.setCurrentIndex(cur_i)

    def connectEditor(self,editor):
        QObject.connect(editor.well_select,
            SIGNAL('currentIndexChanged (const QString&)'), self._setWell)
        QObject.connect(editor.variable_combobox,
            SIGNAL('currentIndexChanged (const QString&)'), self._setVariable)

    def disconnectEditor(self,editor):
        QObject.disconnect(editor.well_select,
            SIGNAL('currentIndexChanged (const QString&)'), self._setWell)
        QObject.disconnect(editor.variable_combobox,
            SIGNAL('currentIndexChanged (const QString&)'), self._setVariable)

    def __repr__(self):
        return '<Well: %s, %s>' % (str(self.well),str(self.variable))

class DownholeBlocksSeries(BaseDataSeries):
    """Data series for downhole plot, use a user selected column."""
    KEYS = ['column','time','variable']
    def getXYData(self):
        if (len(self.xs) + len(self.ys)) == 0 and self._model:
            if all([self.column, self.variable]):
                self.xlabel, self.ylabel = self.variable, 'Elevation'
                try:
                    geo = self._model.geo()
                    indices = geo.column_blocks_indices(self.column)

                    for i in indices:
                        bname = geo.block_name_list[i]
                        lay = geo.layer[geo.layer_name(bname)]
                        col = geo.column[geo.column_name(bname)]
                        self.ys.append(geo.block_centre(lay,col)[2])

                    from t2model import Model
                    self.xs = self._model.getData(Model.BLOCK,self.variable,indices)
                    self.xunit = self._model.getDataUnit(Model.BLOCK,self.variable)
                    self.yunit = 'meter'
                except Exception as ex:
                    logging.warning('Failed to get model data: %s for Column %s' % (self.variable,self.column))
                    logging.debug('  Exception: %s' % str(ex))
        return self.xs, self.ys

    def createEditor(self, scene_manager, model):
        """ The model is only used if no series is connect to it.  Otherwise it
        will be series' model that is used to convert block name to column name.
        """
        from ui_control_mode import Ui_SceneModeControl
        mode_control = Ui_SceneModeControl()
        mbs = mode_control.button()
        from graphics_scenes import MyScene
        single_button = mbs[MyScene.SINGLE_SELECTION_MODE]

        edit_label = QLabel("&Pick Block:")
        column_edit = QLineEdit()
        edit_label.setBuddy(column_edit)

        variable_combobox = QComboBox()
        variable_combobox.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        vairable_label = QLabel("Select &Variable:")
        vairable_label.setBuddy(variable_combobox)

        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(single_button)
        layout.addWidget(edit_label)
        layout.addWidget(column_edit)
        layout.addStretch(1)
        layout.addWidget(vairable_label)
        layout.addWidget(variable_combobox)

        # collect things for the widget
        widget = QWidget()
        widget.setLayout(layout)
        widget._mode_control = mode_control
        widget.column_edit = column_edit
        widget.variable_combobox = variable_combobox
        widget._model = model

        # connect to scene manager, to receive single selection
        def _blockToColumnOriginal(self, name):
            """ this is using the original model object when the editor is
            created.  It may not be the same model that is being used by the
            series. """
            c = ''
            if self._model:
                geo = self._model.geo()
                if geo:
                    c = geo.column_name(name)
            return c
        def _singleSelect(self, name):
            """This needs series._model objects, so only active when it's
            connected to a series."""
            # if is connected, editor._blockToColumn will be working, using
            # series' model to convert block name ot column name.
            if hasattr(self,'_blockToColumn'):
                col_name = getattr(self,'_blockToColumn')(name)
            else:
                col_name = self._blockToColumnOriginal(name)
            self.column_edit.setText(col_name)
        from types import MethodType
        widget._blockToColumnOriginal = MethodType(_blockToColumnOriginal,widget)
        widget._singleSelect = MethodType(_singleSelect,widget)
        scene_manager.connectSignalFromScene("scene_single_selected",widget._singleSelect)

        return widget

    def _setColumn(self, c):
        c = str(c)
        if c <> self.column:
            self.column = c
            self.reset()

    def _setVariable(self, v):
        v = str(v)
        if v <> self.variable:
            self.variable = v
            self.reset()

    def updateEditor(self,editor):
        # column name
        if str(editor.column_edit.text()) and (self.column is None):
            self._setColumn(str(editor.column_edit.text()))
        else:
            editor.column_edit.clear()
            if self.column:
                editor.column_edit.setText(self.column)

        # variable name
        editor.variable_combobox.clear()
        if self._model:
            cur_i = -1
            for i,v in enumerate(self._model.variableList(self._model.BLOCK)):
                editor.variable_combobox.addItem(v)
                if v == self.variable:
                    cur_i = i
            editor.variable_combobox.setCurrentIndex(cur_i)

    def connectEditor(self, editor):
        """ This is a series object's method, connects to an editor widget. """
        QObject.connect(editor.column_edit,
            SIGNAL("textChanged (const QString&)"), self._setColumn)
        QObject.connect(editor.variable_combobox,
            SIGNAL("currentIndexChanged (const QString&)"), self._setVariable)

        if hasattr(editor,'_blockToColumn'):
            delattr(editor,'_blockToColumn')
        if self._model:
            geo = self._model.geo()
            if geo:
                # overwrite
                def _blockToColumn(self, name):
                    return geo.column_name(name)
                from types import MethodType
                editor._blockToColumn = MethodType(_blockToColumn,editor)

    def disconnectEditor(self, editor):
        QObject.disconnect(editor.column_edit,
            SIGNAL("textChanged (const QString&)"), self._setColumn)
        QObject.disconnect(editor.variable_combobox,
            SIGNAL("currentIndexChanged (const QString&)"), self._setVariable)

        if hasattr(editor,'_blockToColumn'):
            delattr(editor,'_blockToColumn')

    def __repr__(self):
        return '<Column: %s, %s>' % (str(self.column),str(self.variable))

class DownholeFieldDataSeries(BaseDataSeries):
    """ X, Y data from external files that contain more than two columns of
    data.  Note this is downhole series, so first column from file is y
    (elevation), and second column is actually x (temperature or pressure etc.)
    """
    KEYS = ['filename','transposexy']
    def getXYData(self):#issue: field data has no units
        if (len(self.xs) + len(self.ys)) == 0:
            if all([self.filename]):
                try:
                    f = open(self.filename,'r')
                    self.xs, self.ys = [], []
                    for line in f.readlines():
                        try:
                            vs = line.strip().split()
                            self.xs.append(float(vs[0]))
                            self.ys.append(float(vs[1]))
                        except:
                            continue
                    f.close()
                    if len(self.xs) <> len(self.ys):
                        raise Exception("Read data from file error.")
                    if self.transposexy:
                        self.xs, self.ys = self.ys, self.xs
                except Exception as ex:
                    logging.error('Failed to load x,y data from file: %s' % self.filename)
                    logging.debug('  Exception: %s' % str(ex))
        return self.xs, self.ys

    def createEditor(self):
        filename_label = QLabel("Drag a file here or open file by Browse...")
        browse_button = QPushButton('Browse...')

        check_transposed = QCheckBox("Transpose X-Y")

        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(filename_label)
        layout.addWidget(browse_button)
        layout.addWidget(check_transposed)

        widget = QWidget()
        widget.setLayout(layout)
        # put these widgets as properties of the object, so easier to access
        widget.filename_label = filename_label
        widget.browse_button = browse_button
        widget.check_transposed = check_transposed
        return widget

    def updateEditor(self,editor):
        if self.filename:
            editor.filename_label.setText(self.filename)
        else:
            editor.filename_label.setText("Drag a file here or open file by Browse...")
        if bool(self.transposexy):
            editor.check_transposed.setChecked(True)
        else:
            editor.check_transposed.setChecked(False)

    def connectEditor(self,editor):
        # needs to create a method (for browsing filename) by using partial so
        # that the method can be triggered with proper series and editor this
        # special temporary method will be removed once disconnected
        from functools import partial
        def browseFile(series,editor):
            fname = QFileDialog.getOpenFileName(editor,
                    "Open Field Data File", '.',
                    "Any text file (*.*)")
            if not fname.isEmpty():
                self.filename = str(fname)
                self.reset()
            editor.filename_label.setText(fname)
        # this (partial) is given to editor as method and knows which series and
        # editor widget is is linked to
        editor.browseFile = partial(browseFile,series=self,editor=editor)
        QObject.connect(editor.browse_button, SIGNAL('clicked()'), editor.browseFile)
        QObject.connect(editor.check_transposed, SIGNAL('stateChanged (int)'), self._setTransposed)

    def _setTransposed(self,transposed):
        if bool(transposed) <> self.transposexy:
            self.transposexy = bool(transposed)
            self.reset()

    def disconnectEditor(self,editor):
        QObject.disconnect(editor.browse_button, SIGNAL('clicked()'), editor.browseFile)
        QObject.disconnect(editor.check_transposed, SIGNAL('stateChanged (int)'), self._setTransposed)
        delattr(editor,'browseFile')

    def _modelUpdated(self):
        """ does not depend on model, overwrite base class implementation """
        pass

    def __repr__(self):
        from os.path import basename
        return '<File: %s>' % basename(str(self.filename))

class HistoryFieldDataSeries(DownholeFieldDataSeries):
    def getXYData(self):
        super(HistoryFieldDataSeries,self).getXYData()
        return self.xs, self.ys

class HistoryBlockSeries(BaseDataSeries):
    """ Data series for history plot of a single block's variable.  At the
    moment history plot only plots results from listing files, which uses
    PyTOUGH's .history() routine. """
    KEYS = ['block', 'variable']
    def getXYData(self):
        """ extract data from model. """
        if (len(self.xs) + len(self.ys)) == 0 and self._model:
            if all([self.block, self.variable]):
                self.xlabel, self.ylabel = 'Time', self.variable
                try:
                    lst = self._model.lst()
                    self.xs, self.ys = lst.history(('e', self.block, self.variable))
                    self.xunit = 't2sec'
                    self.yunit = guessUnits(self.variable)
                except Exception as ex:
                    logging.warning("Failed to get history data: ('e', %s, %s)" % (self.block, self.variable))
                    logging.debug('  Exception: %s' % str(ex))
        # these can be empty,
        return self.xs, self.ys

    def createEditor(self, scene_manager):
        from ui_control_mode import Ui_SceneModeControl
        mode_control = Ui_SceneModeControl()
        mbs = mode_control.button()
        from graphics_scenes import MyScene
        single_button = mbs[MyScene.SINGLE_SELECTION_MODE]

        edit_label = QLabel("&Pick Block:")
        block_edit = QLineEdit()
        edit_label.setBuddy(block_edit)

        variable_combobox = QComboBox()
        variable_combobox.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        vairable_label = QLabel("Select &Variable:")
        vairable_label.setBuddy(variable_combobox)

        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(single_button)
        layout.addWidget(edit_label)
        layout.addWidget(block_edit)
        layout.addStretch(1)
        layout.addWidget(vairable_label)
        layout.addWidget(variable_combobox)

        # collect things for the widget
        widget = QWidget()
        widget.setLayout(layout)
        widget._mode_control = mode_control
        widget.block_edit = block_edit
        widget.variable_combobox = variable_combobox

        # connect to scene manager, to receive single selection
        def _singleSelect(self, name):
            self.block_edit.setText(name)
        from types import MethodType
        widget._singleSelect = MethodType(_singleSelect,widget)
        scene_manager.connectSignalFromScene("scene_single_selected",widget._singleSelect)

        return widget

    def _setBlock(self, b):
        b = str(b)
        if b <> self.block:
            self.block = b
            self.reset()

    def _setVariable(self, v):
        v = str(v)
        if v <> self.variable:
            self.variable = v
            self.reset()

    def updateEditor(self,editor):
        # block name
        if str(editor.block_edit.text()) and (self.block is None):
            self._setBlock(str(editor.block_edit.text()))
        else:
            editor.block_edit.clear()
            if self.block:
                editor.block_edit.setText(self.block)

        # variable name
        editor.variable_combobox.clear()
        if self._model:
            lst = self._model.lst()
            if lst:
                cur_i = -1
                for i,v in enumerate(lst.element.column_name):
                    editor.variable_combobox.addItem(v)
                    if v == self.variable:
                        cur_i = i
                editor.variable_combobox.setCurrentIndex(cur_i)

    def connectEditor(self, editor):
        """ This is a series object's method, connects to an editor widget. """
        QObject.connect(editor.block_edit,
            SIGNAL("textChanged (const QString&)"), self._setBlock)
        QObject.connect(editor.variable_combobox,
            SIGNAL("currentIndexChanged (const QString&)"), self._setVariable)

    def disconnectEditor(self, editor):
        QObject.disconnect(editor.block_edit,
            SIGNAL("textChanged (const QString&)"), self._setBlock)
        QObject.disconnect(editor.variable_combobox,
            SIGNAL("currentIndexChanged (const QString&)"), self._setVariable)

    def __repr__(self):
        return '<Block: %s, %s>' % (str(self.block),str(self.variable))

    def connectModel(self,model):
        """ overwrite base class as lst.history() should always be called again
        when listing file is reloaded """
        if self._model:
            self.disconnect(self._model,SIGNAL("model_lst_reloaded"),self._modelUpdated)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_lst_reloaded"),self._modelUpdated)

    def _modelUpdated(self):
        """ overwrite base class as lst.history() should always be called again
        when listing file is reloaded, this method clears cached x y data if
        model objects modified.  If series x,y is cleared, a singal of
        "series_updated" will be emitted."""
        self.reset()

class HistoryGenerSeries(BaseDataSeries):
    """ Data series for history plot of a single gener's variable.  At the
    moment history plot only plots results from listing files, which uses
    PyTOUGH's .history() routine. """
    KEYS = ['gener', 'variable']
    def getXYData(self):
        """ extract data from model. """
        if (len(self.xs) + len(self.ys)) == 0 and self._model:
            if all([self.gener, self.variable]):
                self.xunit = 't2sec'
                self.yunit = guessUnits(self.variable)
                self.xlabel, self.ylabel = 'Time', self.variable
                try:
                    lst = self._model.lst()
                    for b,g in lst.generation.row_name:
                        if g == self.gener:
                            bg = (b,g)
                            break
                    self.xs, self.ys = lst.history(('g', bg, self.variable))
                    self.xs = self.xs
                except Exception as ex:
                    logging.warning("Failed to get history data: ('e', %s, %s)" % (self.gener, self.variable))
                    logging.debug('  Exception: %s' % str(ex))
        # these can be empty,
        return self.xs, self.ys

    def createEditor(self, scene_manager):

        gener_combobox = QComboBox()
        gener_combobox.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        gener_label = QLabel("Select &Gener:")
        gener_label.setBuddy(gener_combobox)

        variable_combobox = QComboBox()
        variable_combobox.setSizeAdjustPolicy(QComboBox.AdjustToContents)
        vairable_label = QLabel("Select &Variable:")
        vairable_label.setBuddy(variable_combobox)

        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(gener_label)
        layout.addWidget(gener_combobox)
        layout.addStretch(1)
        layout.addWidget(vairable_label)
        layout.addWidget(variable_combobox)

        # collect things for the widget
        widget = QWidget()
        widget.setLayout(layout)
        widget.gener_combobox = gener_combobox
        widget.variable_combobox = variable_combobox

        return widget

    def _setGener(self, g):
        g = str(g)
        if g <> self.gener:
            self.gener = g
            self.reset()

    def _setVariable(self, v):
        v = str(v)
        if v <> self.variable:
            self.variable = v
            self.reset()

    def updateEditor(self,editor):
        editor.gener_combobox.clear()
        editor.variable_combobox.clear()
        if self._model:
            lst = self._model.lst()
            if lst:
                if hasattr(lst, 'generation'):
                    # gener name
                    cur_i = -1
                    for i,v in enumerate(lst.generation.row_name):
                        editor.gener_combobox.addItem(v[1])
                        if v[1] == self.gener:
                            cur_i = i
                    editor.gener_combobox.setCurrentIndex(cur_i)

                    # variable name
                    cur_i = -1
                    for i,v in enumerate(lst.generation.column_name):
                        editor.variable_combobox.addItem(v)
                        if v == self.variable:
                            cur_i = i
                    editor.variable_combobox.setCurrentIndex(cur_i)

    def connectEditor(self, editor):
        """ This is a series object's method, connects to an editor widget. """
        QObject.connect(editor.gener_combobox,
            SIGNAL("currentIndexChanged (const QString&)"), self._setGener)
        QObject.connect(editor.variable_combobox,
            SIGNAL("currentIndexChanged (const QString&)"), self._setVariable)

    def disconnectEditor(self, editor):
        QObject.disconnect(editor.gener_combobox,
            SIGNAL("currentIndexChanged (const QString&)"), self._setGener)
        QObject.disconnect(editor.variable_combobox,
            SIGNAL("currentIndexChanged (const QString&)"), self._setVariable)

    def __repr__(self):
        return '<Gener: %s, %s>' % (str(self.gener),str(self.variable))

    def connectModel(self,model):
        """ overwrite base class as lst.history() should always be called again
        when listing file is reloaded """
        if self._model:
            self.disconnect(self._model,SIGNAL("model_lst_reloaded"),self._modelUpdated)
        self._model = model
        if self._model:
            self.connect(self._model,SIGNAL("model_lst_reloaded"),self._modelUpdated)

    def _modelUpdated(self):
        """ overwrite base class as lst.history() should always be called again
        when listing file is reloaded, this method clears cached x y data if
        model objects modified.  If series x,y is cleared, a singal of
        "series_updated" will be emitted."""
        self.reset()

class FrozenDataSeries(BaseDataSeries):
    """ Storing static X,Y data from other data series, usually used as a saved
    model result for later comparison.  Actual data stored within the object
    itself, hence capable of being serialised just like other properties. """
    KEYS = ['name', 'original_series', 'frozen_x', 'frozen_y', 'other_info',
        'xunit', 'yunit']
    def getXYData(self):
        """ xy from storage """
        if self.frozen_x is None or self.frozen_y is None:
            logging.error("Unable to load saved data series, not initialised properly with storage.")
            return [], []
        else:
            return self.frozen_x, self.frozen_y

    def saveSeries(self, other_series):
        """ This is unique to FrozenDataSeries class, it must be called, so xy
        values can be captured and saved.  This is not done in __init__ to keep
        the properties uniform among different DataSeries.  There is also more
        room here to deal with proccessing and property passing from other
        DataSeries. """
        xs, ys = other_series.getXYData()
        self.frozen_x, self.frozen_y = list(xs), list(ys)
        self.xunit, self.yunit = other_series.xunit, other_series.yunit

        def detailedInfo(s):
            info = {}
            from datetime import datetime
            info['accessed'] = datetime.now().strftime("%X %B %d, %Y")
            info['type'] = s.__class__.__name__
            for k in s.KEYS:
                info[k] = getattr(s,k)
            # model
            m = s._model
            if m:
                if m.geo(): info['mulgrid'] = m.geo().filename
                if m.dat(): 
                    try:
                        info['t2data'] = m.dat().filename
                    except:
                        info['t2data'] = m.dat()['_metadata']['filename']
                if m.lst():
                    info['t2listing'] = m.lst().filename
                    info['t2listing.time (seconds)'] = m.lst().time
            return info
        self.other_info = detailedInfo(other_series)

    def createEditor(self):
        name_label = QLabel("self.original_series")

        layout = QBoxLayout(QBoxLayout.LeftToRight)
        layout.setContentsMargins(0,0,0,0)
        layout.addWidget(name_label)

        widget = QWidget()
        widget.setLayout(layout)
        # put these widgets as properties of the object, so easier to access
        widget.name_label = name_label
        return widget

    def connectEditor(self,editor):
        # series is static, no dynamic interactions
        self.updateEditor(editor)

    def updateEditor(self,editor):
        if self.original_series is None:
            self.original_series = ''
        if self.name is None:
            self.name = ''
        editor.name_label.setText("'%s' from series %s" % (self.name, self.original_series))

    def disconnectEditor(self,editor):
        editor.name_label.setText('')

    def _modelUpdated(self):
        """ does not depend on model, overwrite base class implementation """
        pass

    def __repr__(self):
        return '<Saved: %s>' % self.name

if __name__ == '__main__':
    import unittest
    class TestDataSeries(unittest.TestCase):
        """docstring for TestDataSeries"""
        def setUp(self):
            from t2model import Model
            self.model = Model()
            self.model.loadFileMulgrid('GWAI1509_02.DAT')

        def test_downhole_well(self):
            s = DownholeWellSeries(well=self.model.geo().welllist[0].name, variable='Depth')
            s.getXYData(self.model)
            self.assertEqual(list(s.xs), list([157.5,270.,420.,570.,720.,870.,1020.,1267.5,1690.,2190.,2690.,]),
                msg='DownholeWellSeries should get x data as expected.')
            self.assertEqual(list(s.ys), list([282.5,170.,20.,-130.,-280.,-430.,-580.,-827.5,-1250.,-1750.,-2250.,]),
                msg='DownholeWellSeries should get y data as expected.')

        def test_downhole_fielddata(self):
            s = DownholeFieldDataSeries(filename='Temp_WK007_1958_00_Interp2012.dat')
            s.getXYData()
            self.assertEqual(list(s.xs), list([45.0,106.0,133.0,183.0,205.0,214.0]),
                msg='DownholeFieldDataSeries should get x data as expected.')
            self.assertEqual(list(s.ys), list([390.38,380.38,370.38,310.38,250.38,210.38]),
                msg='DownholeFieldDataSeries should get y data as expected.')


    unittest.main(verbosity=2)

