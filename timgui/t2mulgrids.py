"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

"""@package docstring
Extend PyTOUGH mulgrids's mulgrid class.

"""

from mulgrids import mulgrid
class TimMulgrid(mulgrid):
    """Extends PyTOUGH's original mulgrid class.

    This is a class extending PyTOUGH's original mulgrid class.  It should
    behave identical apart from the addition of several properties that eases
    some searching and indexing.  This is mainly designed to be used in TIM.
    Note, at the moment the grid is assumed to be static, changes in grid will
    cause many of the extensions failing to be correct.
    """
    def __init__(self, *args, **kwargs):
        super(TimMulgrid, self).__init__(*args, **kwargs)
        self._cached_welltrack_blocks = {}
        self._cached_welltrack_block_indices = {}
        self.quadtree = None

    def use_quadtree(self,use_qtree=True):
        """Enable the use of cached quadtree to speedup searching, recommended
        if performing search more than once.

        All the extended/replaced methods will automatically make use of
        quadtree if .use_quadtree(True).  Note that other original PyTOUGH
        methods can still make use of the generated quadtree by passing the
        stored quadtree (property .quadtree) as an argument where appropriate.
        Note, if the grid has been changed, user needs to call the method
        .refresh_quadtree() to recalculate the all-column quadtree.
        """
        if use_qtree:
            if self.quadtree is None:
                self.quadtree = self.column_quadtree()
        else:
            self.quadtree = None

    def refresh_quadtree(self):
        """Refresh the cached all-column quadtree.

        """
        self.use_quadtree(False)
        self.use_quadtree(True)

    def _get_welltrack_blocks(self, well_name, extend_to_bottom=True,
            regular_expression=False):
        """ Get a list of block names that a well track goes through.

        By default, the list extends to bottom of model using the last known
        column.  If regular_expression is True, layer name will be replaced
        by the use of '.'s.
        """
        blks = []
        col = None
        cols = set()
        for layer in self.layerlist[1:]:
            p = self.well[well_name].elevation_pos(layer.centre)
            if p <> None:
                col = self.column_containing_point(p[:2],qtree=self.quadtree)
                if col.surface >= p[2]:
                    cols.add(col.name)
                    blks.append(self.block_name(layer.name, col.name))
            else:
                if col is not None:
                    if extend_to_bottom:
                        blks.append(self.block_name(layer.name, col.name))
        if (len(cols) == 1) and extend_to_bottom and regular_expression:
            return [self.block_name('...', col.name)]
        return blks

    def welltrack_blocks(self, well_name):
        """Returns a list of block names that the well goes through and extends
        to the bottom of the model.

        This is actually just a variation of mulgrid.well_values() in PyTOUGH.
        Instead of returning a list of values, a list of block names is
        returned. Useful for repeated plotting downhole plots of the same well,
        since the welltrack often does not change between model value
        extractions.
        """
        if well_name not in self._cached_welltrack_blocks:
            self._cached_welltrack_blocks[well_name] = self._get_welltrack_blocks(well_name)
        return self._cached_welltrack_blocks[well_name]

    def welltrack_block_indices(self, well_name):
        """Returns a list of block names that the well goes through and extends
        to the bottom of the model.

        See welltrack_blocks() for details.
        """
        if well_name not in self._cached_welltrack_block_indices:
            self._cached_welltrack_block_indices[well_name] = [self.block_name_index[b] for b in self.welltrack_blocks(well_name)]
        return self._cached_welltrack_block_indices[well_name]

if __name__ == '__main__':
    import unittest
    class TestTimMulgrid(unittest.TestCase):
        def setUp(self):
            self.geo = TimMulgrid('../gwai41458_12.dat')
            self.geo.use_quadtree(True)
        def test_well_track_blocks(self):
            tests = [
                ('WK  1',['naq20', 'naq21', 'naq22', 'naq23', 'naq24', 'naq25', 'naq26', 'naq27', 'naq28', 'naq29', 'naq30', 'naq31', 'naq32', 'naq33', 'naq34', 'naq35', 'naq36', 'naq37', 'naq38', 'naq39', 'naq40', 'naq41', 'naq42', 'naq43', 'naq44', 'naq45', 'naq46', 'naq47', 'naq48', 'naq49', 'naq50', 'naq51', 'naq52', 'naq53', 'naq54', 'naq55']),
                ('TH 17',['tbm18', 'tbm19', 'tbm20', 'tbm21', 'tbm22', 'tbm23', 'tbm24', 'tbm25', 'tbm26', 'tbm27', 'tbm28', 'tbm29', 'tbm30', 'tbm31', 'tbm32', 'tbm33', 'tbm34', 'tbm35', 'tbm36', 'tbm37', 'tbm38', 'tbm39', 'tbm40', 'tbm41', 'tbm42', 'tbm43', 'tbm44', 'uqm45', 'udg46', 'udg47', 'udg48', 'udg49', 'udg50', 'udg51', 'udg52', 'udg53', 'udg54', 'udg55']),
                ]
            for t,expected in tests:
                o = self.geo.welltrack_blocks(t)
                self.assertEqual(o,expected)
        def test_well_track_block_indices(self):
            tests = [
                ('WK  1',[5993, 6936, 7904, 8889, 9884, 10884, 11886, 12888, 13890, 14892, 15894, 16896,17898, 18900, 19902, 20904, 21906, 22908, 23910, 24912, 25914, 26916, 27918, 28920, 29922, 30924, 31926, 32928, 33930, 34932, 35934, 36936, 37938, 38940, 39942, 40944]),
                ('TH 17',[4404, 5278, 6193, 7143, 8113, 9099, 10097, 11097, 12099, 13101, 14103, 15105, 16107, 17109, 18111, 19113, 20115, 21117, 22119, 23121, 24123, 25125, 26127, 27129, 28131, 29133, 30135, 31252, 32168, 33170, 34172, 35174, 36176, 37178, 38180,39182, 40184, 41186]),
                ]
            for t,expected in tests:
                o = self.geo.welltrack_block_indices(t)
                self.assertEqual(o,expected)
    unittest.main()
