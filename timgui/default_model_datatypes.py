import t2model
from units import find_rate_enth_names

import numpy as np
import logging
from waiwera_input import WAIWERA_ROCK_DFALT, rock_cells

class Block_Name(t2model.Data):
    """ The name of this class will be used, underscores will be translated into
    space .  Users must provide the essential properties and methods.  """
    """ Names of all blocks. """
    depends_on = (True,False,False)
    data_kind = t2model.Model.BLOCK
    unit = None
    simulators = ['autough2','tough2','waiwera']
    def calculate(self, geo, dat, lst):
        """ This must be implemeted to return an np.array (or list) of values,
        be it strings or numbers.  It is crucial the size matches the data type
        specified (data_kind).  If data_kind = t2model.BLOCK, the size should be
        number of blocks.  If data_kind = t2model.FLOW, the size should be
        number of connections. """
        return np.array([b for b in geo.block_name_list])

class Surf(t2model.Data):
    """ Surface elevation of the column. """
    depends_on = (True,False,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm'
    simulators = ['autough2','tough2','waiwera']
    def calculate(self, geo, dat, lst):
        surf = np.zeros(geo.num_blocks)
        for i,b in enumerate(geo.block_name_list):
            c = geo.column_name(b)
            try:
                surf[i] = geo.column[c].surface
            except KeyError:
                surf[i] = geo.layerlist[0].bottom
        return surf

class Depth(t2model.Data):
    """ Distance from the surface of the column to the block centre. """
    depends_on = (True,False,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm'
    simulators = ['autough2','tough2','waiwera']
    def calculate(self, geo, dat, lst):
        depth = np.zeros(geo.num_blocks)
        for i,b in enumerate(geo.block_name_list):
            c = geo.column_name(b)
            lay = geo.layer[geo.layer_name(b)]
            try:
                depth[i] = geo.column[c].surface - geo.block_centre(lay,geo.column[c])[2]
            except KeyError:
                # happends when column does not exist (special atm)
                depth[i] = geo.layerlist[0].bottom - lay.centre
        return depth

"""
Custom Model Data Type Template:

class XXXXX(t2model.Data):
    depends_on = (True,False,True)
    data_kind = t2model.Model.BLOCK
    def calculate(self, geo, dat, lst):

"""
################### Block from data (input) file  ####################


#-----------------------------  waiwera -----------------------------

def rock_data(geo,dat,valName,dattype='float64'):
    """Extracts rock data from a waiwera model. Uses waiwera default unit (specified at top of script) for cells with no rocktype"""
    data,offset = base_ww_array(geo,valName,dattype)

    for rock in dat['rock']['types']:
        for i in rock_cells(rock,dat):
            try:
                data[i+offset] = rock[valName]
            except KeyError:
                pass
    return data

def base_ww_array(geo,valName,dattype):
    """Creates an array of length equal to geo.num_blocks in the specified datatype. Populates non atmosphere blocks with the default value.
     Used by all waiwera datatypes"""
    offset = geo.num_atmosphere_blocks
    try:
        default = WAIWERA_ROCK_DFALT[valName]
        off_dat = np.zeros(offset,dtype=dattype)
        block_dat = np.full(geo.num_underground_blocks,default,dtype=dattype)
        data = np.concatenate((off_dat,block_dat))
    except KeyError:
        data = np.zeros(geo.num_blocks,dtype=dattype)
        default = data[0]
    return data,offset

class Rock_Type_Name(t2model.Data):
    """Rock type name for waiwera data """
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = None
    simulators = ['waiwera']
    def calculate(self, geo, dat, lst):
        data,offset = base_ww_array(geo,'name','S32')

        for rock in dat['rock']['types']:
            for i in rock_cells(rock,dat):
                try:
                    name = rock['name']
                    if len(name) > 32:
                        name = name[:29] + '...'
                    data[i+offset] = name
                except KeyError:
                    pass
        return data

class Rock_Type_Index(t2model.Data):
    """ Rock type index for waiwera data, as rock type name is not compulsory
    for waiwera.  Cells that have not been assigned a rock_type will be given
    rock index of -1.

    NOTE this can be dangerous, make sure rock editing deals with it properly.
    """
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = ''
    simulators = ['waiwera']
    def calculate(self, geo, dat, lst):
        # set to -1 if no rock type assigned
        data = np.ones(geo.num_blocks,dtype='int32') * -1
        offset = geo.num_atmosphere_blocks
        for idx, rock in enumerate(dat['rock']['types']):
            for i in rock_cells(rock,dat):
                data[i + offset] = idx
        return data

class Zone(t2model.Data):
    """Zone name. Currently only supports cell type zones"""
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = None
    simulators = ['waiwera']
    def calculate(self,geo,dat,lst):
        #calculates cell zone types only - need further support for box type zones and combination zones
        data,offset = base_ww_array(geo,'zone','S32')

        #can assume isValid returns True and therefore this will not raise any exceptions
        for name,zone in dat['mesh']['zones'].iteritems():
            if isinstance(zone,list):
                cells = zone
            elif 'cells' in zone:
                cells = zone['cells']
            else:
                #zone is not of cell type and not currently supported - need to add support for other zone types
                continue

            for i in cells:
                data[i + offset] = str(name)
        return data

    def isValid(self, geo, dat, lst):
        try:
            numCells = sum(('cells' in zone or isinstance(zone,list)) for _,zone in dat['mesh']['zones'].iteritems())
            numOtherZones = len(dat['mesh']['zones']) - numCells
            if numOtherZones > 0:
                logging.warning('Unable to load %i zone(s) specified in data file as tim currently supports only cell type zones' % numOtherZones)
            if numCells > 0:
                return True

            return False
        except (KeyError,TypeError):
            return False

def permeability(geo,dat,idx):
    """Calculates permeability (K1,K2,K3 or Kmax) for waiwera data"""
    data,offset = base_ww_array(geo,'permeability','float64')

    for rock in dat['rock']['types']:
        for i in rock_cells(rock,dat):
            try:
                if idx == 'max':
                    data[i + offset] = max(rock['permeability'])
                else:
                    data[i + offset] = rock['permeability'][idx]
            except KeyError:
                #no permeability field for that rocktype, use default
                #don't need to set default as array is preallocated with default value
                pass
            except TypeError:
                #permeability is a single value for all K1,K2,K3
                data[i + offset] = rock['permeability']
    return data

class K_1_ww(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm**2'
    simulators = ['waiwera']
    overwrite_name = 'K 1'
    def calculate(self, geo, dat, lst):
        return permeability(geo,dat,idx=0)

class K_2_ww(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm**2'
    simulators = ['waiwera']
    overwrite_name = 'K 2'
    def calculate(self, geo, dat, lst):
        return permeability(geo,dat,idx=1)

class K_3_ww(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm**2'
    simulators = ['waiwera']
    overwrite_name = 'K 3'
    def calculate(self, geo, dat, lst):
        return permeability(geo,dat,idx=2)
    def isValid(self, geo, dat, lst):
        # model is 2d and so a third permeability value does not make sense
        if geo.num_layers <= 2:
            return False
        return True

class K_Max_ww(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm**2'
    simulators = ['waiwera']
    overwrite_name = 'K Max'
    def calculate(self, geo, dat, lst):
        return permeability(geo,dat,idx='max')

class Porosity_ww(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = ''
    simulators = ['waiwera']
    overwrite_name = 'Porosity'
    def calculate(self, geo, dat, lst):
        return rock_data(geo,dat,'porosity')

#-----------------------------  AU/TOUGH2 -----------------------------

class Rock_Type(t2model.Data):
    """ Rock type name for t2 data """
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = None
    simulators = ['autough2','tough2']
    def calculate(self, geo, dat, lst):
        return np.array([b.rocktype.name for b in dat.grid.blocklist])

def k(geo,dat,lst,i):
    return np.array([b.rocktype.permeability[i] for b in dat.grid.blocklist])

class K_1_t2(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm**2'
    simulators = ['autough2','tough2']
    overwrite_name = 'K 1'
    def calculate(self, geo, dat, lst):
        return k(geo, dat, lst, i=0)

class K_2_t2(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm**2'
    simulators = ['autough2','tough2']
    overwrite_name = 'K 2'
    def calculate(self, geo, dat, lst):
        return k(geo, dat, lst, i=1)

class K_3_t2(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm**2'
    simulators = ['autough2','tough2']
    overwrite_name = 'K 3'
    def calculate(self, geo, dat, lst):
        return k(geo, dat, lst, i=2)

class K_Max_t2(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'm**2'
    simulators = ['autough2','tough2']
    overwrite_name = 'K Max'
    def calculate(self, geo, dat, lst):
        return np.array([max(b.rocktype.permeability) for b in dat.grid.blocklist])

class Porosity_t2(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = ''
    simulators = ['autough2','tough2']
    overwrite_name = 'Porosity'
    def calculate(self, geo, dat, lst):
        return np.array([b.rocktype.porosity for b in dat.grid.blocklist])


class Input_Constant_Mass_Production(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'kg/s'
    simulators = ['autough2','tough2']
    def isValid(self, geo, dat, lst):
        if all([geo, dat]):
            return True
        return False
    def calculate(self, geo, dat, lst):
        try:
            print "calculating production of constant MASS geners in blocks... ",
            prd = np.zeros(geo.num_blocks)
            for g in dat.generatorlist:
                if g.type in ['MASS', 'COM1', 'WATE'] and g.ltab in [None, 0, 1]:
                    i = geo.block_name_list.index(g.block)
                    if g.gx:
                        prd[i] = prd[i] + g.gx
            print "done."
            return np.array(prd)
        except Exception as e:
            print e
            return np.zeros(geo.num_blocks)

class Input_Constant_Heat(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.BLOCK
    unit = 'J/s/m**2'
    simulators = ['autough2','tough2']
    def isValid(self, geo, dat, lst):
        if all([geo, dat]):
            return True
        return False
    def calculate(self, geo, dat, lst):
        try:
            print "calculating production of constant HEAT geners in blocks... ",
            prd = np.zeros(geo.num_blocks)
            for g in dat.generatorlist:
                if g.type in ['HEAT'] and g.ltab in [None, 0, 1]:
                    i = geo.block_name_list.index(g.block)
                    if g.gx:
                        prd[i] = prd[i] + g.gx
            print "done."
            return np.array(prd)
        except Exception as e:
            print e
            return np.zeros(geo.num_blocks)

#################### Flow/Connection from data (input) file ####################

def con_d(geo,dat,lst,i):
    return np.array([c.distance[i] for c in dat.grid.connectionlist])

class Conne_Dist_1(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.FLOW
    unit = 'm'
    simulators = ['autough2','tough2']
    def calculate(self, geo, dat, lst):
        return con_d(geo,dat,lst, i=0)

class Conne_Dist_2(t2model.Data):
    depends_on = (True,True,False)
    data_kind = t2model.Model.FLOW
    unit = 'm'
    simulators = ['autough2','tough2']
    def calculate(self, geo, dat, lst):
        return con_d(geo,dat,lst, i=1) * -1.0


#################### from listing file ####################

class Total_Mass_Production(t2model.Data):
    depends_on = (True,False,True)
    data_kind = t2model.Model.BLOCK
    unit = 'kg/s'
    simulators = ['autough2','tough2']
    def isValid(self, geo, dat, lst):
        if 'generation' in lst.table_names:
            if find_rate_enth_names(lst.generation.column_name) is not None:
                return True
        return False
    def calculate(self, geo, dat, lst):
        try:
            print "calculating production in blocks... ",
            r_name, e_name = find_rate_enth_names(lst.generation.column_name)
            prd = np.zeros(geo.num_blocks)
            for (b,g) in lst.generation.row_name:
                bi = geo.block_name_index[b]
                rate = lst.generation[(b,g)][r_name]
                enth = lst.generation[(b,g)][e_name]
                if enth <> 0.0:
                    prd[bi] += rate
            print "done."
            return prd
        except:
            return np.zeros(geo.num_blocks)

class Total_Mass_Production_per_Column_Area(t2model.Data):
    depends_on = (True,False,True)
    data_kind = t2model.Model.BLOCK
    unit = 'kg/s/m**2'
    simulators = ['autough2','tough2']
    def isValid(self, geo, dat, lst):
        if 'generation' in lst.table_names:
            if find_rate_enth_names(lst.generation.column_name) is not None:
                return True
        return False
    def calculate(self, geo, dat, lst):
        try:
            print "calculating production in blocks... ",
            r_name, e_name = find_rate_enth_names(lst.generation.column_name)
            prd = np.zeros(geo.num_blocks)
            #area = [geo.column[geo.column_name(b)].area for b in geo.block_name_list]
            area = np.zeros(geo.num_blocks)
            for i,b in enumerate(geo.block_name_list):
                try:
                    area[i] = geo.column[geo.column_name(b)].area
                except:
                    area[i] = 1.0
                    print "Warning, block %s's column area set to 1.0" % b
            for (b,g) in lst.generation.row_name:
                bi = geo.block_name_index[b]
                rate = lst.generation[(b,g)][r_name]
                enth = lst.generation[(b,g)][e_name]
                if enth <> 0.0:
                    prd[bi] += rate
            print "done."
            return prd / area
        except:
            return np.zeros(geo.num_blocks)

class Total_Heat_per_Column_Area(t2model.Data):
    depends_on = (True,False,True)
    data_kind = t2model.Model.BLOCK
    unit = 'J/s/m**2'
    simulators = ['autough2','tough2']
    def isValid(self, geo, dat, lst):
        if 'generation' in lst.table_names:
            if find_rate_enth_names(lst.generation.column_name) is not None:
                return True
        return False
    def calculate(self, geo, dat, lst):
        try:
            print "calculating heat production in blocks... ",
            r_name, e_name = find_rate_enth_names(lst.generation.column_name)
            prd = np.zeros(geo.num_blocks)
            area = np.zeros(geo.num_blocks)
            for i,b in enumerate(geo.block_name_list):
                try:
                    area[i] = geo.column[geo.column_name(b)].area
                except:
                    area[i] = 1.0
                    print "Warning, block %s's column area set to 1.0" % b
            for (b,g) in lst.generation.row_name:
                bi = geo.block_name_index[b]
                rate = lst.generation[(b,g)][r_name]
                enth = lst.generation[(b,g)][e_name]
                if enth == 0.0:
                    prd[bi] += rate
            print "done."
            return prd / area
        except Exception as e:
            logging.warning('Failed to calculate Total Heat per Column Area: %s' % str(e))
            return np.zeros(geo.num_blocks)

#################### Originally from user_model_datatypes ####################

def find_co2_names(col_names):
    """ Given lst.generation.column_name, this returns tuple of column names
    matching 'Generation rate', 'Enthalpy' and 'CO2 frac.'.  This covers the
    known cases of TOUGH2's and AUTOUGH2's cases.  If not matched, None will be
    returned. """
    col_names_lower = [h.lower() for h in col_names]
    r, e, c = None, None, None
    for n, nlower in zip(col_names, col_names_lower):
        if 'generation rate'.startswith(nlower):
            r = n
        if 'enthalpy'.startswith(nlower):
            e = n
        if 'co2 frac.'.startswith(nlower) or 'xco2' == nlower:
            c = n
    if any([r is None, e is None, c is None]):
        return None
    else:
        return (r,e,c)

class Total_CO2_Production(t2model.Data):
    depends_on = (True,False,True)
    data_kind = t2model.Model.BLOCK
    unit = 'kg/s'
    simulators = ['autough2','tough2']
    def isValid(self, geo, dat, lst):
        if 'generation' in lst.table_names:
            if find_co2_names(lst.generation.column_name) is not None:
                return True
        return False
    def calculate(self, geo, dat, lst):
        try:
            print "calculating co2 production in blocks... ",
            r_name, e_name, c_name = find_co2_names(lst.generation.column_name)
            prd = np.zeros(geo.num_blocks)
            for (b,g) in lst.generation.row_name:
                bi = geo.block_name_index[b]
                rate = lst.generation[(b,g)][r_name]
                enth = lst.generation[(b,g)][e_name]
                co2f = lst.generation[(b,g)][c_name]
                if enth <> 0.0:
                    prd[bi] += rate * co2f
            print "done."
            return np.array(prd)
        except:
            return np.zeros(geo.num_blocks)

class Total_CO2_per_Column_Area(t2model.Data):
    depends_on = (True,False,True)
    data_kind = t2model.Model.BLOCK
    unit = 'kg/s/m**2'
    simulators = ['autough2','tough2']
    def isValid(self, geo, dat, lst):
        if 'generation' in lst.table_names:
            if find_co2_names(lst.generation.column_name) is not None:
                return True
        return False
    def calculate(self, geo, dat, lst):
        try:
            print "calculating production in blocks... ",
            r_name, e_name, c_name = find_co2_names(lst.generation.column_name)
            prd = np.zeros(geo.num_blocks)
            #area = [geo.column[geo.column_name(b)].area for b in geo.block_name_list]
            area = np.zeros(geo.num_blocks)
            for i,b in enumerate(geo.block_name_list):
                try:
                    area[i] = geo.column[geo.column_name(b)].area
                except:
                    area[i] = 1.0
                    print "Warning, block %s's column area set to 1.0" % b
            for (b,g) in lst.generation.row_name:
                bi = geo.block_name_index[b]
                rate = lst.generation[(b,g)][r_name]
                enth = lst.generation[(b,g)][e_name]
                co2f = lst.generation[(b,g)][c_name]
                if enth <> 0.0:
                    prd[bi] += rate * co2f
            print "done."
            area = np.array(area)
            return prd/area
        except:
            return np.zeros(geo.num_blocks)


if __name__ == '__main__':
    from mulgrids import *
    from t2model import Model

    model = Model()
    #model._geo = mulgrid('D:\\waiwera_files\\waiwera_example\\gOH35446.dat')
    model._geo = mulgrid('D:\\waiwera_files\\waiwera_example\\g2medium.dat')
    model.loadFileDataInput('D:\\waiwera_files\\waiwera_example\\2DM002dodge.json')
    #model.loadFileDataInput('D:\\waiwera_files\\waiwera_example\\2DM002.json')
    #model.loadFileDataInput('D:\\waiwera_files\\waiwera_example\\OH35446_ns_MJO001.dat')
    data_inst = K_1()
    data_vals = data_inst.calculate(model._geo,model._dat,None)
    names = Rock_Type_Name().calculate(model._geo,model._dat,None)
