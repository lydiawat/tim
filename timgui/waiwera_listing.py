"""
Wrap Waiwera's hdf5 output as t2listing
"""

import h5py
import numpy as np

from t2listing import listingtable

from pprint import pprint as pp
import unittest

class wlisting(object):
    def __init__(self, filename=None, geo=None):
        """ if corresponding geo is supplied, wlisting can behavemore like
        t2listing, which includes atmosphere blocks """
        self._table = {}
        if isinstance(geo, str):
            self.geo = mulgrid(geo)
        else:
            self.geo = geo
        self._h5 = h5py.File(filename, 'r')
        self.filename = filename
        self.simulator = 'waiwera'
        self.setup()

    def setup(self):
        self.cell_idx = self._h5['cell_index'][:,0]
        self.fulltimes = self._h5['time'][:,0]
        self.num_fulltimes = len(self.fulltimes)
        self._index = 0
        ### checks:
        nb = self.geo.num_blocks - self.geo.num_atmosphere_blocks
        if len(self.cell_idx) != nb:
            msg = 'HDF5 result %s has %i cells different from geometry %s (%i excl. atmosphere blocks)' % (
                self.filename, len(self.cell_idx), self.geo.filename, nb)
            raise Exception(msg)
        ### element table
        if 'cell_fields' in self._h5:
            cols = sorted([c for c in self._h5['cell_fields'].keys() if c.startswith('fluid_')])
            blocks = self.geo.block_name_list
            table = listingtable(cols, blocks, num_keys=1)
            self._table['element'] = table
        # makes tables in self._table accessible as attributes
        for key,table in self._table.items():
            setattr(self, key, table)
        # have to be get first table ready
        self.index = 0

    def history(self, selection, short=False, start_datetime=None):
        """
        short is not used at the moment
        """
        dname = {
            'e': 'cell_fields',
        }
        if isinstance(selection, tuple):
            selection = [selection]
        results = []
        for tbl,b,cname in selection:
            if isinstance(b, str):
                bi = self.geo.block_name_index[b]
            elif isinstance(b, unicode):
                bi = self.geo.block_name_index[str(b)]
            elif isinstance(b, int):
                bi = b
            else:
                raise Exception('.history() block must be an int or str: %s (%s)' % (str(b),str(type(b))))
            if bi < 0:
                bi = self.geo.num_blocks + bi
            if bi < self.geo.num_atmosphere_blocks:
                raise Exception('.history() does not support extracting results for atmosphere blocks')
            ### important to convert cell index
            bbi = self.cell_idx[bi-self.geo.num_atmosphere_blocks]
            ys = self._h5[dname[tbl]][cname][:,bbi]
            results.append((self.fulltimes, ys))
        if len(results) == 1: results = results[0]
        return results

    def read_tables(self):
        """ copy values from h5 into listingtables, with slicing """
        if 'element' in self.table_names:
            for i,cname in enumerate(self.element.column_name):
                self.element._data[self.geo.num_atmosphere_blocks:,i] = self._h5['cell_fields'][cname][self._index][self.cell_idx]

    def get_index(self): return self._index
    def set_index(self, i):
        self._index = i
        if self._index < 0: self._index += self.num_fulltimes
        self.read_tables()
    index = property(get_index, set_index)

    def get_table_names(self):
        return sorted(self._table.keys())
    table_names = property(get_table_names)

    def get_time(self): return self.fulltimes[self.index]
    def set_time(self, t):
        if t < self.fulltimes[0]: self.index = 0
        elif t > self.fulltimes[-1]: self.index = -1
        else:
            dt = np.abs(self.fulltimes - t)
            self.index = np.argmin(dt)
    time = property(get_time, set_time)


class test_medium(unittest.TestCase):
    def setUp(self):
        from mulgrids import mulgrid
        self.geo = mulgrid('g2medium.dat')
        self.lst = wlisting('2DM002.h5', self.geo)

    def test_atm_blocks(self):
        self.assertEqual(len(self.lst.element.row_name), self.geo.num_blocks)
        # atmosphere blocks should be zero
        self.assertEqual(
            list(self.lst.element['fluid_temperature'][:self.geo.num_atmosphere_blocks]),
            [0.0] * self.geo.num_atmosphere_blocks)
        # even after change index
        self.index = 1
        self.assertEqual(
            list(self.lst.element['fluid_temperature'][:self.geo.num_atmosphere_blocks]),
            [0.0] * self.geo.num_atmosphere_blocks)

    def test_tables(self):
        self.assertEqual(self.lst.table_names, ['element'])
        cols = [
            'fluid_liquid_capillary_pressure',
            'fluid_liquid_density',
            'fluid_liquid_internal_energy',
            'fluid_liquid_relative_permeability',
            'fluid_liquid_saturation',
            'fluid_liquid_specific_enthalpy',
            'fluid_liquid_viscosity',
            'fluid_liquid_water_mass_fraction',
            'fluid_phases',
            'fluid_pressure',
            'fluid_region',
            'fluid_temperature',
            'fluid_vapour_capillary_pressure',
            'fluid_vapour_density',
            'fluid_vapour_internal_energy',
            'fluid_vapour_relative_permeability',
            'fluid_vapour_saturation',
            'fluid_vapour_specific_enthalpy',
            'fluid_vapour_viscosity',
            'fluid_vapour_water_mass_fraction',
            'fluid_water_partial_pressure']
        self.assertEqual(sorted(self.lst.element.column_name), sorted(cols))

    def test_basic_properties(self):
        self.assertEqual(self.lst.num_fulltimes, 2)
        np.testing.assert_almost_equal(
            self.lst.fulltimes,
            [0.0, 1.0E16],
            decimal=7)

    def test_index(self):
        self.assertEqual(self.lst.index, 0)
        self.assertAlmostEqual(self.lst.time, 0.0)
        np.testing.assert_almost_equal(
            self.lst.element['fluid_temperature'][-5:],
            [15.0, 15.0, 15.0, 15.0, 15.0]
            )

        self.lst.index = 1
        self.lst.index = 1
        self.assertEqual(self.lst.index, 1)
        self.assertAlmostEqual(self.lst.time, 1.0E16)
        np.testing.assert_almost_equal(
            self.lst.element['fluid_temperature'][-5:],
            [235.7365710, 234.266913, 233.142164, 232.376319, 231.98525],
            decimal=4
            )

        self.lst.index = 0
        self.assertEqual(self.lst.index, 0)
        self.assertAlmostEqual(self.lst.time, 0.0)
        np.testing.assert_almost_equal(
            self.lst.element['fluid_temperature'][-5:],
            [15.0, 15.0, 15.0, 15.0, 15.0]
            )

        self.lst.index = -1
        self.assertEqual(self.lst.index, 1)
        self.assertAlmostEqual(self.lst.time, 1.0E16)
        np.testing.assert_almost_equal(
            self.lst.element['fluid_temperature'][-5:],
            [235.7365710, 234.266913, 233.142164, 232.376319, 231.98525],
            decimal=4
            )

    def test_time(self):
        self.lst.time = self.lst.fulltimes[1] - 100.0
        self.assertEqual(self.lst.index, 1)
        self.lst.time = 1.0e19
        self.assertEqual(self.lst.index, 1)
        self.lst.time = 0.0
        self.assertEqual(self.lst.index, 0)
        self.lst.time = 100.0
        self.assertEqual(self.lst.index, 0)

    def test_history(self):
        xs, ys = self.lst.history(('e', -1, 'fluid_pressure'))
        np.testing.assert_almost_equal(xs, [0, 1.0e16] )
        np.testing.assert_almost_equal(ys, [101350.0, 1.3010923804323431E7 ] )
        xs, ys = self.lst.history(('e', 339, 'fluid_pressure'))
        np.testing.assert_almost_equal(xs, [0, 1.0e16] )
        np.testing.assert_almost_equal(ys, [101350.0, 1.3010923804323431E7 ] )
        xs, ys = self.lst.history(('e', '  t16', 'fluid_pressure'))
        np.testing.assert_almost_equal(xs, [0, 1.0e16] )
        np.testing.assert_almost_equal(ys, [101350.0, 1.3010923804323431E7 ] )
        xs, ys = self.lst.history(('e', 338, 'fluid_temperature'))
        np.testing.assert_almost_equal(xs, [0, 1.0e16] )
        np.testing.assert_almost_equal(ys, [15.0, 232.3763193900396] )
        tbl = self.lst.history([
            ('e', -1, 'fluid_pressure'),
            ('e', 339, 'fluid_pressure'),
            ('e', '  t16', 'fluid_pressure'),
            ('e', 338, 'fluid_temperature'),
            ])
        np.testing.assert_almost_equal(tbl[0][0], [0, 1.0e16] )
        np.testing.assert_almost_equal(tbl[0][1], [101350.0, 1.3010923804323431E7 ] )
        np.testing.assert_almost_equal(tbl[1][1], [101350.0, 1.3010923804323431E7 ] )
        np.testing.assert_almost_equal(tbl[2][1], [101350.0, 1.3010923804323431E7 ] )
        np.testing.assert_almost_equal(tbl[3][1], [15.0, 232.3763193900396] )

        # should spit out an exception about not supporting atmosphere blocks
        self.assertRaises(Exception, self.lst.history, ('e', 0, 'fluid_temperature'))
        self.assertRaisesRegexp(Exception, 'atmosphere', self.lst.history, ('e', 0, 'fluid_temperature'))

class test_medium_multiple_cpu(test_medium):
    def setUp(self):
        from mulgrids import mulgrid
        self.geo = mulgrid('g2medium.dat')
        self.lst = wlisting('2DM002a.h5', self.geo)

class test_compare(unittest.TestCase):
    def setUp(self):
        from mulgrids import mulgrid
        self.geo = mulgrid('g2medium.dat')
        self.lst1 = wlisting('2DM002.h5', self.geo)
        self.lst2 = wlisting('2DM002a.h5', self.geo)

    def test_table(self):
        self.lst1.index = 1
        self.lst2.index = 1
        self.assertAlmostEqual(self.lst1.time, 1.0E16)
        self.assertAlmostEqual(self.lst2.time, 1.0E16)
        np.testing.assert_allclose(
            self.lst1.element['fluid_temperature'],
            self.lst2.element['fluid_temperature'],
            rtol=1e-10,
            equal_nan=True
            )
        np.testing.assert_allclose(
            self.lst1.element['fluid_pressure'],
            self.lst2.element['fluid_pressure'],
            rtol=1e-10,
            equal_nan=True
            )

    def test_history(self):
        np.testing.assert_allclose(
            self.lst1.history(('e', -1, 'fluid_pressure'))[1],
            self.lst2.history(('e', -1, 'fluid_pressure'))[1],
            rtol=1e-10,
            )
        np.testing.assert_allclose(
            self.lst1.history(('e', 128, 'fluid_temperature'))[1],
            self.lst2.history(('e', 128, 'fluid_temperature'))[1],
            rtol=1e-10,
            )

        # check for false positive, this should be different
        self.assertRaises(
            AssertionError,
            np.testing.assert_allclose,
            self.lst1.history(('e', 123, 'fluid_pressure'))[1],
            self.lst2.history(('e', 78, 'fluid_pressure'))[1],
            rtol=1e-10,
            )
        self.assertRaises(
            AssertionError,
            np.testing.assert_allclose,
            self.lst1.history(('e', 127, 'fluid_temperature'))[1],
            self.lst2.history(('e', 128, 'fluid_temperature'))[1],
            rtol=1e-10,
            )



if __name__ == '__main__':
    unittest.main(verbosity=2)

    # import time
    # from mulgrids import mulgrid
    # geo = mulgrid('gLihir_v7_NS.dat')
    # init_time = time.time()
    # lst = wlisting('Lihir_v7_SP_NS_060_wai.h5', geo)
    # print '%.2f sec' % (time.time() - init_time)
    # init_time = time.time()
    # lst.index = 1
    # print '%.2f sec' % (time.time() - init_time)



