"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from PyQt4.QtCore import *
from PyQt4.QtGui import *

class LinePlot(QObject):
    """ a plot basically has a list of series, along with additional information
    kept like titles etc. """
    def __init__(self):
        super(LinePlot, self).__init__()

        # the series list is special list that able to callback so LinePlot
        # objects can emit signals when the list is updated.
        from misc_types import NotifyList
        self.series = NotifyList()
        self.series.register_callback(self.notify)

        self.title = None
        self.xlabel, self.ylabel = None, None

        # non-essential properties: a dict for storing misc things that users
        # may want to keep during the exporting and importing cycle.  Contents
        # of this dict is optional and should be kept simple, ie. json can
        # serialise/de-serialise easily without extra coding.
        self.properties = {}

        # It is possible to give more attributes to the LinePlot objects, but
        # when these objects are serialised/exported, only those above will
        # be exported ('title', 'xlabel', 'ylabel', 'properties')

    def notify(self):
        self.emit(SIGNAL("updated"))

    def __repr__(self):
        if self.title is None:
            return "Plot with %i lines: [" % len(self.series) + "],[".join([str(s) for s in self.series[:3]]) + "]"
        else:
            return "Plot with %i lines: %s " % (len(self.series),self.title)

    ##### the following are methods that mak this class behaves like a list
    def __len__(self):
        return self.series.__len__()

    def __getitem__(self, key):
        return self.series.__getitem__(key)

    def __setitem__(self, key, value):
        self.series.__setitem__(key,value)

    def __delitem__(self, key):
        del self.series[key]

    def __iter__(self):
        return self.series.__iter__()

    def __reversed__(self):
        return self.series.__reversed__()

    def __contains__(self, item):
        return self.series.__contains__(item)

    def append(self, item):
        self.series.append(item)

    def remove(self, item):
        self.series.remove(item)

    def pop(self, key):
        self.series.pop(key)

