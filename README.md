# **TIM** (**T**im **I**sn't **M**ulgraph)

[![Online Documentation](https://readthedocs.org/projects/tim/badge/?version=latest)](https://tim.readthedocs.org/)

## What is TIM?

TIM is a graphical tool for TOUGH2 simulators.  The aim of this application is to ease some common model development tasks.  It is designed to display quantitative information clearly and to allow users interactively browse and modify models.

## What is Mulgraph?

Tim isn't Mulgraph, but TIM is inspired by **Mulgraph**, a long standing graphical tool for TOUGH2 simulators created at the University of Auckland in 1995. TIM aims to duplicate Mulgraph's features while making them more powerful and easier to use.  You do not need Mulgraph to use TIM.  However, the model geometry file used by TIM is the same as that used by Mulgraph (see below).

## Geometry

Because TOUGH2 simulations do not need geometry information to run, it is essential to have auxiliary geometric information to display models graphically. The choice here is the geometry file format used and specified by Mulgraph.  For more details please see Appendix A in the  ['*PyTOUGH user's guide*'](https://github.com/acroucher/PyTOUGH/blob/master/doc/PyTOUGH- guide.pdf?raw=true).  Other geometry formats *may* be supported in the future.

## More information

A paper introducing TIM can be found [here](http://www.geothermal-energy.org/pdf/IGAstandard/NZGW/2013/Yeh_Final.pdf?) or [here](https://pangea.stanford.edu/ERE/db/IGAstandard/record_detail.php?id=19732).

For more detailed and up-to-date information regarding TIM, please see TIM's [online documentation](https://tim.readthedocs.org/).

## Install

**The easy way (only on Windows at the moment):**

The easy way for many Windows users is to install the *self-contained* version, which includes its own Python and all the dependencies.  To install, simply download and unzip to a directory, then run the included file `tim.exe`.  Administrator privileges are not needed.  The latest version can be found [here](https://bitbucket.org/angusyeh/tim/downloads)

**The Pythonic way (Windows, Linux and MacOS):**

The other way is to install it like many other Python packages (TIM is written in the Python language).  This method is better suited for users who are interested in using PyTOUGH (or parts of TIM) on other scripting in general.  Please download TIM's source code and execute the command:

```python setup.py install```

This will create a script file called `tim.py` which the user can run.  Note that Python and several Python libraries are required:

* Python 2.7
* PyQt 4.8+ (if you have the latest PyTOUGH working, this is the only additional library required)
* NumPy
* Matplotlib 1.0+ (and its dependencies)
* Scipy
* Pint
* VTK (optional)

More details of how to install these can be found [here](https://tim.readthedocs.org/en/latest/install/).

## What's new in TIM

(0.7.1)

* Add simple rectangular mesh creation
* Add simple model input file creation
* Add radial plot
* Add matplotlib toolbar for line plots
* Allow rename rocktype name
* Fix unable to use pyplot in interactive mode with binary distribution
* Improve model running command

(0.7.0)

* Improve error handling of files/settings loading
* Add image exporting ability for line plots
* Add model running, batch plotting and semilog/loglog line plots
* Add unit system, unit changable in scenes, display units in line plots
* Add online documentation (mkdocs/readthedocs) and Help menu links
* Fix listing history update and a few datatype value bugs
* Fix a few rendering and UI bugs

(0.6.7)

* Fix issue of not refreshing after reloading listing file
* Improve auto rock grouping, now possible via scene and model limit
* Improve colorbar editing with color picker dialog
* Improve colorbar internal logic to prevent matplotlib errors with odd cases
* Improve handling of invalid listing files (requires latest PyTOUGH testing after commit 3a9201a)
* Fix a few flickering issues

(0.6.6)

* Improve zoom in/out with mouse, now mouse coordinate (google map style)
* Add shortcut keys (user configurable)
* Add find box with autocomplete for goto block or well in scene
* Add (hidden) advanced rocktype editing
* Add improved algorithm for automatic rocktype grouping/coloring
* Fix layout issues when using screen with limited resolution (height)
* Fix a couple of colorbar issues

(0.6.5)

* Add save/freeze line plot series for comparing results
* Change default of loading plot list on start, on by default
* Fix several small cosmetic issues with line plotting
* Fix issue from changes in 0.6.4

(0.6.4)

* Add feature of selecting blocks along a polyline
* Improve the visual of selection, now use cross pattern overlay
* Fix issue of not selecting all blocks inside polygon

(0.6.2)

* Fix issue startup when user settings directory not exist
* Add open user settings folder
* Add open log file
* User commands now loaded from user settings folder
* Fix some colorbar issues
* Fix 'K Max' not refreshing after editing rock types
* Fix default axis with offset values
* Fix incorrect direction of connection distance 2
* Fix issue with transpose x-y of plot with elevation field data

