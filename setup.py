"""
Standard Python distutils setup script.

Use:
python setup.py install

Setting up TIM this way will have all the code being installed into system's
python environment.  On Windows, this will usually be a directory like:
C:\python27\Lib\site-packages\.  A script tim.py will also be installed into
Python's script directory, eg. C:\python27\Scripts\.  If this directory is on
the system's path.  It is possible for users to launch TIM easily by typing
'tim.py' in the commandline.  Windows user will need to make their system
associate .py files with python.exe.
"""

"""
Copyright 2013, 2014 University of Auckland.

This file is part of TIM (Tim Isn't Mulgraph).

    TIM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TIM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TIM.  If not, see <http://www.gnu.org/licenses/>.
"""

from distutils.core import setup
from timgui import info

setup(
    name = info.name,
    version = info.version,
    description = info.description,
    author = info.author,
    author_email = info.author_email,
    url = info.url,
    license = info.license,
    packages = ['timgui',],
    package_data={'timgui': ['*.json','*.qrc']},
    scripts = ['tim.py'],
)
