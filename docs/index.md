
**TIM** -- **T**im **I**sn't **M**ulgraph.

---

### Welcome to TIM

TIM is a graphical tool for TOUGH2 simulators.  The aim of this application is to ease some common model development tasks.  It is designed to display quantitative information clearly and to allow users interactively browse and modify models.

### What is Mulgraph?

Tim isn't Mulgraph, but TIM is inspired by **Mulgraph**, a long standing graphical tool for TOUGH2 simulators created at the University of Auckland in 1995. TIM aims to duplicate Mulgraph's features while making them more powerful and easier to use.  You do not need Mulgraph to use TIM.  However, the model geometry file used by TIM is the same as that used by Mulgraph (see below).

### How about PyTOUGH?

TIM is built on top of [PyTOUGH](https://github.com/acroucher/PyTOUGH), a powerul Python scripting library for pre- and post-processing TOUGH2 related simulations.  Knowing PyTOUGH and general Python scripting will allow you to customise many aspects of TIM, but TIM is designed to be used effectively as a GUI without needing to know any programming.

### Geometry

Because TOUGH2 simulations do not need geometry information to run, it is essential to have auxiliary geometric information to display models graphically. The choice here is the geometry file format used and specified by Mulgraph.  For more details please see Appendix A in the  ['*PyTOUGH user's guide*'](https://github.com/acroucher/PyTOUGH/blob/master/doc/PyTOUGH- guide.pdf?raw=true).  Other geometry formats *may* be supported in the future.

### Support

TIM has online documentation at <http://tim.readthedocs.org> (you are probably reading it).

If you found any bug, or have some feature requests, feel free to open issues at <https://bitbucket.org/angusyeh/tim/issues> or simply email me at <a.yeh@auckland.ac.nz>.

### Reference paper

A paper introducing TIM can be found [here](http://www.geothermal-energy.org/pdf/IGAstandard/NZGW/2013/Yeh_Final.pdf?) or [here](https://pangea.stanford.edu/ERE/db/IGAstandard/record_detail.php?id=19732).

