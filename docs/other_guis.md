There are some other GUIs built for TOUGH2 simulators over the years.  

---

* MulGRAPH
* G*BASE
* (Tanaka)
* mView
* Leapfrog
* TOUGH2Viewer
* GEOCAD
* TOUGHVISUAL
* Petrasim
* Tougher
* Steinar (http://www.vatnaskil.is/2-uncategorised/21-steinar-pre-and-post-processor-for-tough2-model-meshes.html)
* TOUGH2GIS (http://software.dicam.unibo.it/tough2gis)
* iMatTOUGH (2015 ToughSymposium)


